package fr.inra.mia.ssb.rmesplot.views;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;

import javax.swing.*;

import fr.inra.mia.ssb.rmesplot.controls.RMESPlotController;

/**
 * This class regroups the used components in the pyramid tab.
 * 
 * @author $Author : jguerin $
 * @version $Revision : 1.1 $
 */
public class PyramidPanel extends JPanel {

    private static final int TITLE_TEXTFIELD_SIZE = 20;

    private RMESPlotController _controller;

    PyramidPanel(RMESPlotController controller) {
    	super();
    	
        _controller = controller;
        setLayout(new BorderLayout());

        JScrollPane scrollPane = new JScrollPane();
       
        scrollPane
                .setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane
                .setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(scrollPane, BorderLayout.CENTER);

        final PyramidArea pyramidArea = new PyramidArea(controller);
        scrollPane.setViewportView(pyramidArea);
        controller.setPyramidArea(pyramidArea);

        JPanel bottomPanel=new JPanel();
        add(bottomPanel, BorderLayout.SOUTH);
        bottomPanel.setLayout(new BorderLayout());
        JPanel keyPanel=new PyramidKeyPanel(controller);
        bottomPanel.add(keyPanel,BorderLayout.NORTH);
        
        JPanel buttonPanel = new JPanel();
        bottomPanel.add(buttonPanel,BorderLayout.CENTER);
        
        JLabel title = new JLabel(_controller.getLabel("TITLE_LABEL"));
        buttonPanel.add(title);

        final JTextField textField = new JTextField();
        textField.setColumns(TITLE_TEXTFIELD_SIZE);
        textField.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                pyramidArea.setTitle(textField.getText());
            }});
        buttonPanel.add(textField);
        
        JButton button=new JButton(_controller.getLabel("CLEAR_PYRAMIDS_BUTTON"));
        button.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				pyramidArea.clearPyramids();
			}
        });
        buttonPanel.add(button);
        button=new JButton(_controller.getLabel("PRINT_BUTTON"));
        button.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent e) {
               PrinterJob pJob=PrinterJob.getPrinterJob();
               if (pJob.printDialog()==true) {
                   try {
                       pJob.setPrintable(pyramidArea);
                       pJob.print();
                } catch (PrinterException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
               }
           }
        });
        buttonPanel.add(button);
        
    }
}