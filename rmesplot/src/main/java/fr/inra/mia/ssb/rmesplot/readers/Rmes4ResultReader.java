package fr.inra.mia.ssb.rmesplot.readers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashSet;

import fr.inra.mia.ssb.rmesplot.controls.RMESPlotController;
import fr.inra.mia.ssb.rmesplot.models.ElementStats;
import fr.inra.mia.ssb.rmesplot.models.ElementStatsFactory;
import fr.inra.mia.ssb.rmesplot.models.FamilyResults;
import fr.inra.mia.ssb.rmesplot.models.GaussStats;
import fr.inra.mia.ssb.rmesplot.models.Model;
import fr.inra.mia.ssb.rmesplot.models.Results;
import fr.inra.mia.ssb.rmesplot.models.UnknownAlgorithmException;
import fr.inra.mia.ssb.rmesplot.models.Util;
import fr.inra.mia.ssb.rmesplot.models.WordResults;

public class Rmes4ResultReader extends ResultReader {

	
	
	public Rmes4ResultReader() {
		
	}
	
	public int readSingleLengthWordResults(BufferedReader resultReader, String algorithmString, int order, WordResults results) throws IOException
	{
		Model model=new Model();
		model.setOrder(order);
		order=-1;
		
		/*
		 * Skip line with column headers.
		 */
		String buffer=resultReader.readLine();
		
		/*
		 * Start reading word data.
		 */
		HashSet<ElementStats> elements=new HashSet<ElementStats>();
		ElementStatsFactory esf=new ElementStatsFactory();
		
		boolean done=false;
		while (resultReader.ready() && !done) {
			buffer=resultReader.readLine().trim();
			if (buffer.startsWith("Markov order")) {
				/*
				 * Extract next Markov order.
				 */
		        buffer=buffer.substring(buffer.indexOf(':')+2);
		        order=Integer.parseInt(buffer);
				
				/*
				 * Skip line with word lengths.
				 */
				buffer=resultReader.readLine();
				done=true;
				
			} else {

				String[] fields=buffer.split("\\s+");
				Float[] attributes=new Float[fields.length-1];
				for (int index=1;index<fields.length;index++)
					attributes[index-1]=Float.parseFloat(fields[index]);
				try {
					ElementStats  estats=esf.getInstance(algorithmString);
					estats.setName(fields[0]);
					estats.setAttributes(attributes);
					elements.add(estats);
				} catch (UnknownAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}		
		}
		
		model.setElements(elements);
		results.getModels().add(model);
		model.setResults(results);
		return order;
	}
	
	public Results readWordResults(BufferedReader resultReader, String algorithmString, int order) throws IOException
	{
		WordResults results=new WordResults();
		
		while (order>=0) {
			order=readSingleLengthWordResults(resultReader, algorithmString, order,results);
		}
		
		return results;
	}
	
	
	public Results readFamilyResults(BufferedReader resultReader, String line, String algorithmString, int order) throws IOException, UnknownAlgorithmException
	{
		FamilyResults results=new FamilyResults();
		
		HashSet<Model> models=new HashSet<Model>();
		Model model = new Model();
        model.setOrder(order);
        models.add(model);
        
		/*
		 * Extract name of family set.
		 */
		String name=line.substring(line.indexOf(':')+2);
		results.setName(name);
		
		/*
		 * Ignore line with name of family file.
		 */
		line=resultReader.readLine();
		
		/*
		 * Ignore line with column headers.
		 */
		line=resultReader.readLine();
		
		/**
		 * Loop on data lines.
		 */
		HashSet<ElementStats> elements=new HashSet<ElementStats>();
		ElementStatsFactory esf=new ElementStatsFactory();
		while (resultReader.ready()) {
			ElementStats eStats=esf.getInstance(algorithmString);
			int nattributes=eStats.getNAttributes();
			Float[] attributes=new Float[nattributes];
			
			line=resultReader.readLine();
			String[] fields=line.split("\\s+");
			eStats.setName(fields[0]);
			int offset=1;
			for (int index=0; index < attributes.length; index++) {
				if ("Compound Poisson".equals(algorithmString) && index == attributes.length-2) {
					attributes[index]=new Float(0.0);
					index++;
					offset=0;
				}
				attributes[index]=Float.parseFloat(fields[index+offset]);
			}
			eStats.setAttributes(attributes);
			elements.add(eStats);
		}
		
		
		model.setElements(elements);
		model.setResults(results);
		results.setModels(models);
		return results;
	}
	
	@Override
	public Results readResults(RMESPlotController controller, URL resultURL)
			throws IOException, UnknownAlgorithmException {
		
		Results results = null;
		
		InputStream resultStream = resultURL.openStream();
        BufferedReader resultReader = new BufferedReader(new InputStreamReader(resultStream));
		
        String line=resultReader.readLine(); // Skip first line of file with R'MES description.
        
        /*
         * Read sequence name 
         */
        line=resultReader.readLine();
        String title=line.substring(line.indexOf(':')+2);
        
        /*
         * Read Alphabet
         */
        line=resultReader.readLine();
        String alphabet=line.substring(line.indexOf(':')+2);
		String[] letters=alphabet.split("\\s");
		Util.setAlphabet(letters);

		/*
		 * Read algorithm
		 */
		line=resultReader.readLine();
		String algorithmString=line.substring(line.indexOf(':')+2);
		
		/*
		 * Read Markov Order
		 */
		line=resultReader.readLine();
        line=line.substring(line.indexOf(':')+2);
        int markovOrder=-1;
        markovOrder=Integer.parseInt(line);

        /*
         * Read either word length or family name.
         */
        String datatype=controller.getLabel("WORDS_LABEL");
        line=resultReader.readLine();
        if (line.startsWith("Word"))
        	results=readWordResults(resultReader,algorithmString,markovOrder);
        if (line.startsWith("Families")) {
        	results=readFamilyResults(resultReader,line,algorithmString,markovOrder);
        	datatype=controller.getLabel("FAMILIES_LABEL");
        }
        
        results.setTitle(title);
        results.setAlgorithm((String) _algorithms.get(algorithmString) + " "
                + datatype);
        results.setPhase(0);
        
        resultStream.close();
        
		return results;
	}

}
