package fr.inra.mia.ssb.rmesplot.models;

/**
 * This class creates an object which lists all data of a little box in a
 * pyramid, this element is called PyramidElement.
 * 
 * @author $Author: mhoebeke $
 * @version $Revision: 123 $
 */
public class PyramidElement {
    
    String _word;
    float _value;
    float _minValue;
    float _maxValue;

    PyramidElement(String word, float value, float minValue, float maxValue) {
        _word = word;
        _value = value;
        _maxValue = maxValue;
        _minValue = minValue;
    }

    /**
     * Method getting the word representing the PyramidElement.
     * 
     * @return String giving the word.
     */
    public String getWord() {
        return _word;
    }

    /**
     * Method getting the stat of the word.
     * 
     * @return float the stat of the word
     */
    public float getValue() {
        return _value;
    }

    /**
     * Method getting the stat max. for a giving word length.
     * 
     * @return float the stat max.
     */
    public float getMaxValue() {
        return _maxValue;
    }

    /**
     * Method getting the stat min. for a giving word length.
     * 
     * @return float the stat min.
     */
    public float getMinValue() {
        return _minValue;
    }

}