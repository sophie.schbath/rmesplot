package fr.inra.mia.ssb.rmesplot.models;

public class UnknownResultFormatException extends Exception {

	public UnknownResultFormatException(String header) {
		super("Result header ("+header+") not compliant with any known version of R'MES");
	}
}
