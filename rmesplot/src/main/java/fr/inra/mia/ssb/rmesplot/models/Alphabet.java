/*
 * Created on 28 f�vr. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package fr.inra.mia.ssb.rmesplot.models;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

/**
 * @author mhoebeke
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Alphabet {
    
    static Alphabet _currentAlphabet;
    
    HashSet _tokens;
    char _joker;
    
    protected Alphabet(String tokenString, char joker) {
        _tokens=new HashSet();
        _joker=joker;
        
        for (int c=0;c<tokenString.length();c++) {
            _tokens.add(""+tokenString.charAt(c));
        }
            
    }
    
    public String[] expand(String word) {
        Vector resultVector=new Vector();
        resultVector.add(word);
        boolean  moreJokers=false;
        do {
            moreJokers=false;
            int jokerIndex=-1;
            int i=0;
            for (i=0;i<resultVector.size() && jokerIndex<0;i++)
                jokerIndex=((String)resultVector.get(i)).indexOf(_joker);                    
            
            if (jokerIndex>=0) {
                moreJokers=true;
                String tmpWord=((String)resultVector.get(i-1));
                resultVector.remove(i-1);
                Iterator it=_tokens.iterator();
                while (it.hasNext()) {
                    String token=(String)it.next();
                    String replaceWord=tmpWord.replaceFirst(""+_joker,token);
                   resultVector.add(replaceWord);
                }
               
            }
            
        } while (moreJokers==true);
        int jokerIndex=word.indexOf(_joker);
        
        String[] resultArray=new String[resultVector.size()];
        resultArray=(String[])resultVector.toArray(resultArray);
        return resultArray;
    }
    
    public static void setCurrentAlphabet(Alphabet alphabet) {
        _currentAlphabet=alphabet;
    }
    
    public static Alphabet getCurrentAlphabet() {
        return _currentAlphabet;
    }

}
