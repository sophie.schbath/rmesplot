/*
 * Created on 14 f�vr. 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fr.inra.mia.ssb.rmesplot.views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import fr.inra.mia.ssb.rmesplot.controls.RMESPlotController;

/**
 * @author mark
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class RMESPlotMenuBar extends JMenuBar {

    RMESPlotController _controller;

    AboutDialog _aboutDialog;

    public RMESPlotMenuBar(RMESPlotController controller) {
        super();

        _controller = controller;
        JMenu menu = new JMenu(_controller.getLabel("FILE_MENU"));
        add(menu);

        AbstractAction action = new AbstractAction("Load Files") {
            public void actionPerformed(ActionEvent evt) {	
            	JFileChooser fileChooser=new JFileChooser();
            	fileChooser.setMultiSelectionEnabled(true);
            	fileChooser.setCurrentDirectory(_controller.getCurrentDir());
                int retval = fileChooser.showOpenDialog(null);
                if (retval == JFileChooser.APPROVE_OPTION) {
                    File[] files = fileChooser.getSelectedFiles();
                    _controller.setCurrentDir(fileChooser.getCurrentDirectory());
                    _controller.loadFiles(files);
                }
            }
        };

        JMenuItem item = new JMenuItem(action);
        item.setText(_controller.getLabel("LOAD_MENUITEM"));
        menu.add(item);


        action=new AbstractAction("Delete Result") {
        	public void actionPerformed(ActionEvent evt) {
        		_controller.deleteResults();
        	}
        };
        
        item=new JMenuItem(action);
        item.setText(_controller.getLabel("DEL_MENUITEM"));
        menu.add(item);
        
        menu.addSeparator();

        action = new AbstractAction("Quit") {
            public void actionPerformed(ActionEvent evt) {
                _controller.quit();
            }
        };

        item = new JMenuItem(action);
        item.setText(_controller.getLabel("QUIT_MENUITEM"));
        menu.add(item);

        menu=new JMenu(_controller.getLabel("OPTIONS_MENU"));
        add(menu);
        
        action = new AbstractAction("Filtering") {
          public void actionPerformed(ActionEvent evt) {
              _controller.showFilterDialog();
          }
        };
        item = new JMenuItem(action);
        item.setText(_controller.getLabel("FILTERS_MENUITEM"));
        menu.add(item);
        
        add(Box.createHorizontalGlue());
        
        menu=new JMenu(_controller.getLabel("HELP_MENU"));
        add(menu);
        
        _aboutDialog=new AboutDialog(controller);
        action=new AbstractAction("About") {
            public void actionPerformed(ActionEvent e) {
                _aboutDialog.setVisible(true);
            }
        };
        item=new JMenuItem(action);
        item.setText(_controller.getLabel("ABOUT_MENUITEM"));
        menu.add(item);
    }
}