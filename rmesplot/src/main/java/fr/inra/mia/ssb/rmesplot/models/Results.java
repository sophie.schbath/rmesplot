package fr.inra.mia.ssb.rmesplot.models;

import java.util.*;

/**
 * Class holding all the information stored in a result file generated by R'MES.
 * This class references all the data contained in the different kinds if result
 * files generated by R'MES. Its subclasses manage the two main categories of
 * results : WordResults for statistical data about single words and
 * FamilyResults for statistical data about words defined by patterns. The main
 * attributes of a Result instance are : - the title, - the algorithm used to
 * generate the results, - the phase of the Markov model. - a set of Model
 * instances relative to the results.
 * 
 * @author $Author: mhoebeke $
 * @version $Revision: 123 $
 */
public abstract class Results {
        
    protected String _title;

    protected String _algorithm;

    protected int _phase;

    protected HashSet<Model> _models;

    
    public Results() {
    	_models=new HashSet<Model>();
    }
    
    /**
     * Initializes the tite of this Result.
     * 
     * @param title
     *            the title of this Result.
     */
    public void setTitle(String title) {
        _title = title;
    }

    /**
     * Returns the title of this Result.
     * 
     * @return the title of this Result.
     */
    public String getTitle() {
        return _title;
    }

    /**
     * Initializes the name of the algortihm for this Result.
     * 
     * @param algorithm
     *            the name of the algorithm for this Result.
     */
    public void setAlgorithm(String algorithm) {
        _algorithm = algorithm;
    }

    /**
     * Returns the name of the algorithm for this Result.
     * 
     * @return the name of the algorithm for this Result.
     */
    public String getAlgorithm() {
        return _algorithm;
    }

    /**
     * Initializes the phase for this Result.
     * 
     * @param phase
     *            the pase for this Result.
     */
    public void setPhase(int phase) {
        _phase = phase;
    }

    /**
     * Returns the phase for this Result.
     * 
     * @return the phase for this Result.
     */
    public int getPhase() {
        return _phase;
    }

    /**
     * Initializes the set of Model instances of this Result.
     * 
     * @param models
     *            a HashSet of Model instances.
     */
    public void setModels(HashSet<Model> models) {
        _models = models;
        for (Model model : models) {
        	model.setResults(this);        	
        }
    }

    /**
     * Returns the set of Model instances of this Result.
     * 
     * @return a HashSet of Model instances.
     */
    public HashSet<Model> getModels() {
        return _models;
    }
    

}