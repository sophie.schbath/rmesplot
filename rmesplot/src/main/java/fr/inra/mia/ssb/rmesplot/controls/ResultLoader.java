/*
 * Created on 24 f�vr. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package fr.inra.mia.ssb.rmesplot.controls;

import java.awt.Cursor;
import java.awt.EventQueue;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import fr.inra.mia.ssb.rmesplot.models.Results;
import fr.inra.mia.ssb.rmesplot.models.UnknownAlgorithmException;
import fr.inra.mia.ssb.rmesplot.models.UnknownResultFormatException;
import fr.inra.mia.ssb.rmesplot.readers.ResultReader;
import fr.inra.mia.ssb.rmesplot.readers.ResultReaderFactory;
import fr.inra.mia.ssb.rmesplot.readers.Rmes3ResultReader;
import fr.inra.mia.ssb.rmesplot.views.WaitDialog;

/**
 * @author mhoebeke
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ResultLoader implements Runnable {

    RMESPlotController _controller = null;
    File[] _files = null;
    
    String _dialogString = null;
    Results _results = null;
    
    public ResultLoader(RMESPlotController controller, File[] files) {
        _controller=controller;
        _files=files;
    }
    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    public void run() {
        final WaitDialog waitDialog= _controller.getWaitDialog();
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                _controller.getDataFrame().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                _controller.getGraphicsFrame().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                waitDialog.setVisible(true);
            }
        });
        for (int i = 0; i < _files.length; i++) {
            final URL resultsURL;
            try {
                resultsURL = new URL("file", null, _files[i].getAbsolutePath());
                _dialogString="<html><h3>"+_controller.getLabel("WAITDIALOG_LOADING_MESSAGE")+" "+_files[i].getName()+"</h3></html>";
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        waitDialog.setMessage(_dialogString);
                        String errorDetail=null;
                        try {
                        	ResultReader reader=new ResultReaderFactory().buildResultReader(resultsURL);
                            _results = reader.readResults(_controller,resultsURL); 
                            _controller.getResultTree().addResults(_results);
                        } catch (IOException ioe) {
                            errorDetail=_controller.getLabel("IOEXCEPTION_LOADING_ERROR");
                        } catch (UnknownAlgorithmException uae) {
                            errorDetail=_controller.getLabel("UNKNOWN_ALGORITHM_LOADING_ERROR");
                        } catch (UnknownResultFormatException e) {
							errorDetail=_controller.getLabel("UNKNOWN_FORMAT_LOADING_ERROR");
						}
                        if (_results==null) {
                        	JOptionPane.showMessageDialog(null,_controller.getLabel("UNABLE_TO_LOAD_ERROR")+resultsURL.toString()+"\n"+errorDetail,_controller.getLabel("LOAD_ERROR_TITLE"), JOptionPane.ERROR_MESSAGE);
                        }
                    }}
                );
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                _controller.getDataFrame().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                _controller.getGraphicsFrame().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                waitDialog.setVisible(false);
            }
        });    }

}
