/*
 * Created on 24 f�vr. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package fr.inra.mia.ssb.rmesplot.views.filters;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import fr.inra.mia.ssb.rmesplot.controls.RMESPlotController;
import fr.inra.mia.ssb.rmesplot.models.filters.ElementFilter;
import fr.inra.mia.ssb.rmesplot.models.filters.ExpectationFilter;
import fr.inra.mia.ssb.rmesplot.models.filters.ScoreFilter;

/**
 * @author mhoebeke
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ExpectationFilterPanel extends FilterPanel {
    
    private static final int TEXTFIELD_MAXCOLUMNS = 8;
    
    private JTextField _lowerTextField;
    private JTextField _upperTextField;
    
    public ExpectationFilterPanel(RMESPlotController controller) {
        super(controller,controller.getLabel("EXPECTATIONFILTERBORDER_TITLE"));
        
        JPanel panel=new JPanel();
        add(panel,BorderLayout.CENTER);
        
        JPanel innerPanel=new JPanel();
        innerPanel.setLayout(new GridLayout(2,1));
        panel.add(innerPanel);
        
        JPanel lowerPanel=new JPanel();
        innerPanel.add(lowerPanel);
        
        JLabel label=new JLabel(_controller.getLabel("EXPECTATIONFILTER_LOWERTEXTFIELD"));
        lowerPanel.add(label);
        
        _lowerTextField=new JTextField(TEXTFIELD_MAXCOLUMNS);
        _lowerTextField.setText("0.0");
        lowerPanel.add(_lowerTextField);JPanel upperPanel=new JPanel();
        innerPanel.add(upperPanel);
        
        label=new JLabel(_controller.getLabel("EXPECTATIONFILTER_UPPERTEXTFIELD"));
        upperPanel.add(label);
        
        _upperTextField=new JTextField(TEXTFIELD_MAXCOLUMNS);
        _upperTextField.setText("0.0");
        upperPanel.add(_upperTextField);
        
        
    }

    /* (non-Javadoc)
     * @see rmes.views.filters.FilterPanel#getFilter()
     */
    public ElementFilter getFilter() {
        float upperThreshold=Float.parseFloat(_upperTextField.getText());
        float lowerThreshold=Float.parseFloat(_lowerTextField.getText());
        return new ExpectationFilter(lowerThreshold,upperThreshold);
    }

}
