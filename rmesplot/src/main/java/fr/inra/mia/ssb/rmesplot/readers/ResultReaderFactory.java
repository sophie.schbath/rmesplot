package fr.inra.mia.ssb.rmesplot.readers;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.util.Hashtable;

import fr.inra.mia.ssb.rmesplot.controls.RMESPlotController;
import fr.inra.mia.ssb.rmesplot.models.UnknownResultFormatException;

public class ResultReaderFactory {
	
	private Hashtable<String,Class> readers;
	
	public ResultReaderFactory() {
		readers=new Hashtable<String, Class>();
		readers.put("# rmes-3",Rmes3ResultReader.class);
		readers.put("Analysis performed with: rmes-4.",Rmes4ResultReader.class);
	}
	
	public ResultReader buildResultReader(URL resultURL) throws UnknownResultFormatException {
		ResultReader reader=null;
		
		try {
			BufferedReader br=new BufferedReader(new InputStreamReader(resultURL.openStream()));
			String headerline=br.readLine();
			for (String resultId : readers.keySet()) {
				if (headerline.startsWith(resultId)) {
					Class readerClass=readers.get(resultId);
					reader=(ResultReader)readerClass.newInstance();
				}
			}
			if (reader==null) {
				throw new UnknownResultFormatException(headerline);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return reader; 
	}

}
