/*
 * Created on 14 f�vr. 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fr.inra.mia.ssb.rmesplot.models.filters;

import java.util.HashSet;
import java.util.Iterator;

import fr.inra.mia.ssb.rmesplot.models.ElementStats;

/**
 * @author mark
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ObservedFilter implements ElementFilter {

    private int _threshold;
    private boolean _above;

    public ObservedFilter(int threshold, boolean above) {
        _threshold = threshold;
        _above=above;
    }

    /* (non-Javadoc)
     * @see rmes.models.filters.ElementFilter#apply(java.util.HashSet)
     */
    public HashSet apply(HashSet elements) {
       HashSet newElements=new HashSet();
        Iterator it=elements.iterator();
        while (it.hasNext()) {
            ElementStats es=(ElementStats)it.next();
            if ((_above ==true && es.getCount().intValue() >= _threshold) ||
            		(_above == false && es.getCount().intValue() <= _threshold)) {
                newElements.add(es);
            }
        }
        return newElements;
    }

}