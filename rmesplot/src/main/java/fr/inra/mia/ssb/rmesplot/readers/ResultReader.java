package fr.inra.mia.ssb.rmesplot.readers;

import java.io.IOException;
import java.net.URL;
import java.util.Hashtable;

import fr.inra.mia.ssb.rmesplot.controls.RMESPlotController;
import fr.inra.mia.ssb.rmesplot.models.Results;
import fr.inra.mia.ssb.rmesplot.models.UnknownAlgorithmException;

public class ResultReader {

	protected static Hashtable<String,String> _algorithms = null;

	static {
		/**
		 * R'MES 3 identifiers.
		 */
		_algorithms = new Hashtable<String,String>();
		_algorithms.put("Cond_as","Gauss");
		_algorithms.put("Mart","Gauss (opt)");
		_algorithms.put("Mart_r","Reversed Martingale");
		_algorithms.put("Cond_as_p","Poisson");
		_algorithms.put("Cond_as_pc","Compound Poisson");
		_algorithms.put("Exact","Exact");
		
		/**
		 * R'MES 4 identifiers.
		 */
		_algorithms.put("Gauss","Gauss");
		_algorithms.put("Compound Poisson","Compound Poisson");
		_algorithms.put("Poisson","Poisson");
	}


	public Results readResults(RMESPlotController controller, URL resultURL) throws IOException, UnknownAlgorithmException
	{
		return null;
	}

}
