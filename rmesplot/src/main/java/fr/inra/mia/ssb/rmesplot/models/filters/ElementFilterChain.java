/*
 * Created on 14 f�vr. 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fr.inra.mia.ssb.rmesplot.models.filters;

import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;


/**
 * @author mark
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ElementFilterChain {
    private Hashtable _filters;

    public ElementFilterChain() {
        _filters = new Hashtable();
    }

    public void addFilter(String name, ElementFilter filter) {
        _filters.put(name, filter);
    }

    public void removeFilter(String name) {
        _filters.remove(name);
    }

    public void clear() {
        _filters.clear();
    }
    
    public HashSet applyFilters(HashSet elements) {
        HashSet newElements=elements;
        Collection filters=_filters.values();
        Iterator it=filters.iterator();
        while (it.hasNext()) {
            ElementFilter filter=(ElementFilter)it.next();
            newElements=filter.apply(newElements);
        }
        return newElements;
        
    }
 
}