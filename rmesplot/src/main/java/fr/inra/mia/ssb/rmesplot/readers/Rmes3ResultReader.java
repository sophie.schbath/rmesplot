package fr.inra.mia.ssb.rmesplot.readers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.zip.GZIPInputStream;

import fr.inra.mia.ssb.rmesplot.controls.RMESPlotController;
import fr.inra.mia.ssb.rmesplot.models.ElementStats;
import fr.inra.mia.ssb.rmesplot.models.ElementStatsFactory;
import fr.inra.mia.ssb.rmesplot.models.FamilyResults;
import fr.inra.mia.ssb.rmesplot.models.Model;
import fr.inra.mia.ssb.rmesplot.models.Results;
import fr.inra.mia.ssb.rmesplot.models.UnknownAlgorithmException;
import fr.inra.mia.ssb.rmesplot.models.Util;
import fr.inra.mia.ssb.rmesplot.models.WordResults;

/**
 * Class building an instance of Result by reading data from a URL. This
 * high-level class allows for easy reading and parsing of R'MES result files by
 * needing only a URL to build a representation of the Result instance it
 * contains.
 * 
 * @author $Author: mhoebeke $
 * @version $Revision: 123 $
 */
public class Rmes3ResultReader extends ResultReader {
 
    public Rmes3ResultReader() {
 
    }

    /**
     * DOCUMENT ME!
     * 
     * @param resultReader
     *            DOCUMENT ME!
     * 
     * @return DOCUMENT ME!
     * 
     * @throws IOException
     *             DOCUMENT ME!
     */
    private Results readFamilyResults(BufferedReader resultReader, String algorithmdesc)
            throws IOException, UnknownAlgorithmException {
    	
        FamilyResults results = new FamilyResults();
        ElementStatsFactory esFactory = new ElementStatsFactory();
        ElementStats es = esFactory.getInstance(algorithmdesc);
        int nattributes = es.getNAttributes();
        Float[] attributes=new Float[nattributes];
 
        String line;
        StringTokenizer tokenizer;

        //Parse word length.
        line = resultReader.readLine();
        tokenizer = new StringTokenizer(line, " ");

        int wordlength = Integer.parseInt(tokenizer.nextToken());

        while (!tokenizer.hasMoreTokens()) {
            line = resultReader.readLine();
            tokenizer = new StringTokenizer(line, " ");
        }

        int wordlengthrepeat = Integer.parseInt(tokenizer.nextToken());

        //Parse family information.
        while (!tokenizer.hasMoreTokens()) {
            line = resultReader.readLine();
            tokenizer = new StringTokenizer(line, " ");
        }

        int nfamilies = Integer.parseInt(tokenizer.nextToken());

        while (!tokenizer.hasMoreTokens()) {
            line = resultReader.readLine();
            tokenizer = new StringTokenizer(line, " ");
        }

        int wordsperfamily = Integer.parseInt(tokenizer.nextToken());

        line=resultReader.readLine();
        String[] components=line.split("\\W");
        results.setName(components[components.length-1]);
        
        line=resultReader.readLine();
        tokenizer = new StringTokenizer(line, " ");
  
        int maxfamilylength = Integer.parseInt(tokenizer.nextToken());

        //Parse model order.
        while (!tokenizer.hasMoreTokens()) {
            line = resultReader.readLine();
            tokenizer = new StringTokenizer(line, " ");
        }

        int ordermin = Integer.parseInt(tokenizer.nextToken());

        if (!tokenizer.hasMoreTokens()) {
            line = resultReader.readLine();
            tokenizer = new StringTokenizer(line, " ");
        }

        int ordermax = Integer.parseInt(tokenizer.nextToken());

        HashSet models = new HashSet();

        Model model = new Model();
        model.setOrder(ordermax);
        models.add(model);

        // Read actual values for each family.
        HashSet elements = new HashSet();

        for (int i = 0; i < nfamilies; i++) {
            try {
                while (!tokenizer.hasMoreTokens()) {
                    line = resultReader.readLine();
                    tokenizer = new StringTokenizer(line, " ");
                }

                String name = tokenizer.nextToken();

                for (int j=0;j<nattributes;j++) {
                	/**
                	 * Le fichier de familles pour Poisson compos�e ne contient pas
                	 * l'attribut _A. Il faut le passer...
                	 */
                	if ("Cond_as_pc".equals(algorithmdesc) && j==nattributes-2) {
                			attributes[j]=new Float(0.0);
                			j++;
                	}
                	while (!tokenizer.hasMoreTokens()) {
                		line = resultReader.readLine();
                		tokenizer = new StringTokenizer(line, " ");
                	}

                	String stringToken=tokenizer.nextToken();
                	try {
                		attributes[j]= new Float(stringToken);
                	} catch (NumberFormatException nfe) {
                		if (name != null) {
                			System.err.println("Word "+name+" ignored because of invalid value: "+stringToken);
                			name=null;
                		}
                	}
                	
                }
                if (name != null) {
                	ElementStats element=esFactory.getInstance(algorithmdesc);
                	element.setName(name);
                	element.setAttributes(attributes);
                	elements.add(element);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        model.setElements(elements);

        results.setModels(models);
        return results;
    }

    /**
     * DOCUMENT ME!
     * 
     * @param resultReader
     *            DOCUMENT ME!
     * @param algorithmdesc
     *            DOCUMENT ME!
     * 
     * @return DOCUMENT ME!
     * 
     * @throws IOException
     *             DOCUMENT ME!
     * @throws UnknownAlgorithmException
     *             DOCUMENT ME!
     */
    private Results readWordResults(BufferedReader resultReader,
            String algorithmdesc) throws IOException, UnknownAlgorithmException {
        WordResults results = new WordResults();

        ElementStatsFactory esFactory = new ElementStatsFactory();
        ElementStats es = esFactory.getInstance(algorithmdesc);
        int nattributes = es.getNAttributes();
        HashSet models = new HashSet();

        String line;
        StringTokenizer tokenizer;

        line = resultReader.readLine();
        tokenizer = new StringTokenizer(line, " ");

        //Parse minlength/maxlength.
        while (!tokenizer.hasMoreTokens()) {
            line = resultReader.readLine();
            tokenizer = new StringTokenizer(line, " ");
        }

        int minlength = Integer.parseInt(tokenizer.nextToken());

        while (!tokenizer.hasMoreTokens()) {
            line = resultReader.readLine();
            tokenizer = new StringTokenizer(line, " ");
        }

        int maxlength = Integer.parseInt(tokenizer.nextToken());

        //Loop on word lengths.
        for (int wlength = minlength; wlength <= maxlength; wlength++) {
            int nwords = Util.nWords(wlength);
            
            //Parse ordermin/ordermax.
            while (!tokenizer.hasMoreTokens()) {
                line = resultReader.readLine();
                tokenizer = new StringTokenizer(line, " ");
            }

            int ordermin = Integer.parseInt(tokenizer.nextToken());

            while (!tokenizer.hasMoreTokens()) {
                line = resultReader.readLine();
                tokenizer = new StringTokenizer(line, " ");
            }

            int ordermax = Integer.parseInt(tokenizer.nextToken());

            //Loop on orders.
            for (int order = ordermin; order <= ordermax; order++) {
                Model model = new Model();
                model.setOrder(order);
                models.add(model);

                HashSet elements = new HashSet();
                String[] wordstring = new String[nwords];
                Float[][] attributes = new Float[nwords][nattributes];

                //Loop on attributes.
                for (int attribute = 0; attribute < nattributes; attribute++) {
                    //Loop on words.
                    for (int word = 0; word < nwords; word++) {
                        if (attribute == 0) {
                            wordstring[word] = Util.intToWord(word, wlength);
                        }

                        while (!tokenizer.hasMoreTokens()) {
                            line = resultReader.readLine();
                            tokenizer = new StringTokenizer(line, " ");
                        }
                        
                        String stringToken=tokenizer.nextToken();
                        try {
                        attributes[word][attribute] = new Float(stringToken);
                        } catch  (NumberFormatException nfe){
                        	if (wordstring[word] != null) {
                        		attributes[word][attribute]=null;
                        		System.err.println("Word "+wordstring[word]+" ignored because of invalid value: "+stringToken);
                        		wordstring[word]=null;
                        	}
                        }
                    }
                }

                for (int word = 0; word < nwords; word++) {
                    ElementStats element = esFactory.getInstance(algorithmdesc);
                    if (wordstring[word] != null) {
                    	element.setName(wordstring[word]);
                    	element.setAttributes(attributes[word]);
                    	elements.add(element);
                    }
                }

                model.setElements(elements);
            }
        }

        results.setModels(models);
        return results;
    }

    /**
     * Static method carrying out result loading and parsing and returning an
     * instance of the resulting Result.
     * 
     * @param resultURL
     *            the URL pointing to the R'MES result file.
     * 
     * @return a Result instance containing the data read from the URL.
     * 
     * @throws IOException
     *             raised when it is not possible to read from the URL.
     * @throws UnknownAlgorithmException
     *             raised when the results do not conform to one of the known
     *             algorithms.
     */
    public Results readResults(RMESPlotController controller, URL resultURL) throws IOException,
            UnknownAlgorithmException {
    	InputStream resultStream = null;
        Results results = null;
        BufferedReader resultReader =null;
        /*
         * Try to read compressed format first.
         */
        try {
        	resultStream = resultURL.openStream();
        	GZIPInputStream gzStream=new GZIPInputStream(resultStream);
        	resultReader = new BufferedReader(new InputStreamReader(gzStream));
        } catch (IOException ze) {
        	String message=ze.getMessage();
        	if (message.indexOf(("Not in GZIP format")) >= 0) {
        		resultStream = resultURL.openStream();
        		resultReader = new BufferedReader(new InputStreamReader(resultStream));
        	} else {
        		ze.printStackTrace();
        	}
        }
        
        String line;
        StringTokenizer tokenizer;


        //Read header line OR comment lines introduced with RMES2
        line=resultReader.readLine();
        while (line.startsWith("#")) {
        	String alphabetString=line.replaceFirst("# Alphabet = ","");
        	String [] alphabet=alphabetString.split("\\s");
        	Util.setAlphabet(alphabet);
        	line = resultReader.readLine();
        }
        //Read header line.
        tokenizer = new StringTokenizer(line, "#");

        String title = tokenizer.nextToken();
        String algorithmstring = tokenizer.nextToken();

        //Parse algorithm type.
        tokenizer = new StringTokenizer(algorithmstring, " ");
        String algorithmdesc = tokenizer.nextToken();
        
        //Parse phase (last integer token of line)
        String[] tokens=line.split("\\s");
        int phase = Integer.parseInt(tokens[tokens.length-1]);

        //Parse for WordResult or FamilyResult.
        boolean familles = algorithmdesc.endsWith("_familles");
        String datatype = controller.getLabel("WORDS_LABEL");

        if (familles) {
            algorithmdesc = algorithmdesc.replaceFirst("_familles","");
            results = readFamilyResults(resultReader,algorithmdesc);
            datatype = controller.getLabel("FAMILIES_LABEL");
        } else {
            results = readWordResults(resultReader, algorithmdesc);
        }

        results.setTitle(title);
        results.setAlgorithm((String) _algorithms.get(algorithmdesc) + " "
                + datatype);
        results.setPhase(phase);

        resultStream.close();

        return results;
    }
}