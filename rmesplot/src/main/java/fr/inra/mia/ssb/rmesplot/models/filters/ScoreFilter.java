/*
 * Created on 14 f�vr. 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fr.inra.mia.ssb.rmesplot.models.filters;

import java.util.HashSet;
import java.util.Iterator;

import fr.inra.mia.ssb.rmesplot.models.ElementStats;

/**
 * @author mark
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ScoreFilter implements ElementFilter {

    private float _threshold;

    public ScoreFilter(float threshold) {
        _threshold = threshold;
    }

    /* (non-Javadoc)
     * @see rmes.models.filters.ElementFilter#apply(java.util.HashSet)
     */
    public HashSet apply(HashSet elements) {
       HashSet newElements=new HashSet();
        Iterator it=elements.iterator();
        while (it.hasNext()) {
            ElementStats es=(ElementStats)it.next();
            float absValue=Math.abs(es.getScore().floatValue());
            if (absValue >= _threshold) {
                newElements.add(es);
            }
        }
        return newElements;
    }

}