/*
 * Created on 25 f�vr. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package fr.inra.mia.ssb.rmesplot.models;

import java.util.HashSet;
import java.util.Hashtable;

/**
 * @author mhoebeke
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Pyramid {
    
    private Hashtable _elements;
    private int _phase;
    private String _topWord;
    
    public Pyramid(int phase) {
        _phase=phase;
        _elements=new Hashtable();
    }
    
    public int getLayers() {
        return _elements.size();
    }
    
    public PyramidElement[] getElementsAtLayer(int i) {
        Integer layerInt=new Integer(i);
        return (PyramidElement[])_elements.get(layerInt);
    }
    
    public void setElementsAtLayer(int i, PyramidElement[] elements) {
        Integer layerInt=new Integer(i);
        _elements.put(layerInt,elements);
        if (i==0)
        	_topWord=elements[0].getWord();
    }
    
    public String getTopWord() {
    	return _topWord;
    }
    
    

}
