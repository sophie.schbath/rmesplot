/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.controls;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.swing.table.TableModel;

import fr.ssbgroup.jrmes.rmesplot.models.Alphabet;
import fr.ssbgroup.jrmes.rmesplot.models.DNAAlphabet;
import fr.ssbgroup.jrmes.rmesplot.models.ElementStats;
import fr.ssbgroup.jrmes.rmesplot.models.Model;
import fr.ssbgroup.jrmes.rmesplot.models.Pyramid;
import fr.ssbgroup.jrmes.rmesplot.models.Results;
import fr.ssbgroup.jrmes.rmesplot.models.WordResults;
import fr.ssbgroup.jrmes.rmesplot.models.filters.ElementFilterChain;
import fr.ssbgroup.jrmes.rmesplot.views.DataFrame;
import fr.ssbgroup.jrmes.rmesplot.views.DataTable;
import fr.ssbgroup.jrmes.rmesplot.views.DataTableTabbedPane;
import fr.ssbgroup.jrmes.rmesplot.views.GraphicsFrame;
import fr.ssbgroup.jrmes.rmesplot.views.PlotArea;
import fr.ssbgroup.jrmes.rmesplot.views.PyramidArea;
import fr.ssbgroup.jrmes.rmesplot.views.ResultTree;
import fr.ssbgroup.jrmes.rmesplot.views.WaitDialog;
import fr.ssbgroup.jrmes.rmesplot.views.filters.FilterDialog;

/**
 * @author mark
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class RMESPlotController {

	private boolean _exitOnQuit=true;
	
	private Properties _properties=null;
	
    private ResourceBundle _bundle = null;

    private ResultTree _tree = null;

    private DataTableTabbedPane _dataTableTabbedPane = null;

    private PlotArea _plotArea = null;
    
    private PyramidArea _pyramidArea = null;
    
    private ElementFilterChain _filterChain = null;

    private Model _modelOnX = null;

    private int _indexOnX = 0;

    private Model _modelOnY = null;

    private int _indexOnY = 0;

    private FilterDialog _filterDialog = null;
    private WaitDialog _waitDialog=null;

    protected GraphicsFrame _graphicsFrame;

    protected DataFrame _dataFrame;

    boolean _autoAddPyramids;
    
    File _currentDir;
    
    public RMESPlotController(boolean exitOnQuit) {

    	_exitOnQuit=exitOnQuit;
    	
        _bundle = ResourceBundle.getBundle("fr.ssbgroup.jrmes.rmesplot.RMESPlotL10N");
        _properties = new Properties();
        try {
			_properties.load(getClass().getResourceAsStream("/fr/ssbgroup/jrmes/rmesplot/RMESPlot.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		_filterChain=new ElementFilterChain();
		Boolean tmpBoolean=Boolean.valueOf((String)_properties.getProperty("datatablepanel.autoaddpyramids"));
		_autoAddPyramids=tmpBoolean.booleanValue();
	
		_currentDir=new File(System.getProperty("user.dir"));
		
		Alphabet.setCurrentAlphabet(new DNAAlphabet());
		
		buildGUI();
    }

    private void buildGUI() {
    	_dataFrame = new DataFrame(this);
        _graphicsFrame = new GraphicsFrame(this);
        _filterDialog=new FilterDialog(this);
        _waitDialog=new WaitDialog(this);
    }

    public String getLabel(String key) {
        String label=null;
        try {
            label = _bundle.getString(key);
        } catch (MissingResourceException mre) {
            label = "???";
        }
        return label;
    }

    public Object getProperty(Object key) {
    	return _properties.get(key);
    }
    
    public void loadFiles(File[] files) {
    	_tree.clearSelection();
        if (files.length>0) {
            ResultLoader resultLoader=new ResultLoader(this,files);
            Thread loaderThread=new Thread(resultLoader);
            loaderThread.start();
            try {
                loaderThread.join();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public void modelSelected(Model model,String description) {
        _dataTableTabbedPane.addModel(model, description);
    }

    public void deleteResults() {
    	_tree.deleteResults();
    }
    
    public String buildTitleString(Model model)
    {
    	
    	Results results=model.getResults();
    	String title="";
    	
    	if (results instanceof WordResults)
    		title="l"+model.getWordSize();
    	else
    		title="fam";
    	
    	title=title+"-m"+model.getOrder()+"-p"+results.getPhase();
    	
    	return title;
    }
    
    public void plotOnX(Model model, int index) {
        _modelOnX = model;
        _indexOnX = index;
        _plotArea.plotOnX(_modelOnX,""+index+"-"+buildTitleString(model));
    }

    public void clearPlotOnX() {
        _plotArea.clearXPlot();
        _modelOnX = null;
        _indexOnX = 0;
    }

    public void plotOnY(Model model, int index) {
        _modelOnY = model;
        _indexOnY = index;
        _plotArea.plotOnY(_modelOnY,""+index+"-"+buildTitleString(model));
    }

    public void clearPlotOnY() {
        _plotArea.clearYPlot();
        _modelOnY = null;
        _indexOnY = 0;
    }

    public void exportModel(DataTable table, File file) {
        try {
			PrintWriter pw=new PrintWriter(file);
			StringBuffer line=new StringBuffer();
			TableModel model=table.getModel();
			String separator="";
			for (int col=0;col<model.getColumnCount();col++) {
					line.append(separator+model.getColumnName(col));
					separator="\t";
			}
			pw.println(line);
			for (int row=0;row<model.getRowCount();row++) {
				separator="";
				line.setLength(0);
				for (int col=0;col<model.getColumnCount();col++) {
					line.append(separator+table.getValueAt(row,col));
					separator="\t";
				}
				pw.println(line);
			}
			pw.close();
        } catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void removeModel(Model model, int index) {
        if (model == _modelOnX && index == _indexOnX) {
            clearPlotOnX();

        }
        if (model == _modelOnY && index == _indexOnY) {
            clearPlotOnY();
        }
    }

    public void showFilterDialog() {
        _filterDialog.setVisible(true);
    }
    
    public void quit() {
    	_graphicsFrame.dispose();
    	_dataFrame.dispose();
    	_filterDialog.dispose();
    	_waitDialog.dispose();
    	if (_exitOnQuit)
    		System.exit(0);
    }

    public ResultTree getResultTree() {
        return _tree;
    }
    
    public void setResultTree(ResultTree tree) {
        _tree = tree;
    }

    public void setDataTableTabbedPane(DataTableTabbedPane dataTableTabbedPane) {
        _dataTableTabbedPane = dataTableTabbedPane;
    }

    public PlotArea getPlotArea() {
        return _plotArea;
    }
    
    public void setPlotArea(PlotArea plotArea) {
        _plotArea = plotArea;
    }
    
    public PyramidArea getPyramidArea() {
    	return _pyramidArea;
    }
    
    public void setPyramidArea(PyramidArea pyramidArea) {
    	_pyramidArea=pyramidArea;
    }
    
    public DataFrame getDataFrame() {
        return _dataFrame;
    }
    
    public GraphicsFrame getGraphicsFrame() {
        return _graphicsFrame;
    }

    public WaitDialog getWaitDialog() {
        return _waitDialog;
    }
    
    public ElementFilterChain getElementFilterChain() {
        return _filterChain;
    }
    /**
     * Method called by inner components to filter out elements from datasets
     * based on the filters activated in _filterChain.
     * @param elements elements before filtering.
     * @return filtered elements.
     */
    public HashSet applyFilters(HashSet elements) {
        return _filterChain.applyFilters(elements);
        
    }
    
    /**
     * Method called when the _filterDialog is closed and leading
     * to the refiltering of all models displayed in tables, plots and pyramids.
     *
     */
    public void filtersChanged() {
    	_dataTableTabbedPane.applyFilters();
        if (_modelOnX != null)
            _plotArea.refreshPlotOnX();
        if (_modelOnY != null)
            _plotArea.refreshPlotOnY();
    }
    
    public void ElementSelected(ElementStats es, Model model) {
        if (model==_modelOnX || model==_modelOnY)
            _plotArea.selectWord(es.getName());
 
        if (_autoAddPyramids == true) {
            Results results=model.getResults();
            if (results instanceof WordResults) {
                Pyramid pyramid=((WordResults)results).getPyramidWithTop(es,model);
                _pyramidArea.addPyramid(pyramid);
            }       
        }
    }
    
    public boolean getAutoAddPyramids() {
        return _autoAddPyramids;
    }
    
    public void setAutoAddPyramids(boolean aap) {
        _autoAddPyramids=aap;
    }

	public File getCurrentDir() {
		return _currentDir;
	}

	public void setCurrentDir(File dir) {
		_currentDir = dir;
	}


}