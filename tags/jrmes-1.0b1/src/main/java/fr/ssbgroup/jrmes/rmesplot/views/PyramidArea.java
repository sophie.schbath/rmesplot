/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.JPanel;

import fr.ssbgroup.jrmes.rmesplot.controls.RMESPlotController;
import fr.ssbgroup.jrmes.rmesplot.models.Pyramid;
import fr.ssbgroup.jrmes.rmesplot.models.PyramidElement;

/**
 * @author mark
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class PyramidArea extends JPanel implements Printable {
	
	private static final int BOX_SIZE = 24;
	
	private static final int HORIZONTAL_GAP = 2*BOX_SIZE;
	
	private static final int VERTICAL_GAP = 2*BOX_SIZE;
	
	private static final int LEFT_BORDER = BOX_SIZE;
	
	private static final int TOP_BORDER = BOX_SIZE;
	
	private static final int TEXT_GAP = BOX_SIZE/6;
	
	private static final int TITLE_SEP = BOX_SIZE/2;
	
	private static final Font WORD_FONT = new Font("Monospaced",Font.BOLD,24);
	
	
	RMESPlotController _controller;
	
	Vector _pyramids;
	Vector _pyramidBounds;
	String _title;
	boolean _inPrintJob;
	
	public PyramidArea(RMESPlotController controller) {
		super();
		_controller=controller;
		_pyramids=new Vector();
		_pyramidBounds=new Vector();
		_title=null;
		_inPrintJob=false;
		
        setLayout(new BorderLayout());
        setBackground(Color.white);
        setForeground(Color.black);
  
          
        addMouseListener(new MouseListener() {

			public void mouseClicked(MouseEvent e) {
				Point position=e.getPoint();
				int removeIndex=-1;
				for (int i=0;i<_pyramidBounds.size();i++) {
					Rectangle rect=(Rectangle)_pyramidBounds.get(i);
					if (rect.contains(position)) {
						removeIndex=i;
					}
				}
				if (removeIndex >=0 ) {
					_pyramids.remove(removeIndex);
					repaint();
				}
				// TODO Auto-generated method stub
				
			}

			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}});
	}
	
	public void addPyramid(Pyramid pe) {
		_pyramids.add(pe);
		repaint();
	}
	
	public void clearPyramids() {
		_pyramids.clear();
		repaint();
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
	
		int titleSpace=0;
		if (_title != null) {
		    FontMetrics fm=g.getFontMetrics();
		    Rectangle2D titleBounds=fm.getStringBounds(_title,g);
		    titleSpace=(int)titleBounds.getHeight()+TITLE_SEP;
		    g.drawString(_title,(getWidth()-(int)titleBounds.getWidth())/2,TOP_BORDER+(int)titleBounds.getHeight()-fm.getAscent());
		}
		
		Font oldFont=g.getFont();
		g.setFont(WORD_FONT);
		
		_pyramidBounds.clear();
		Dimension maxPyramidSize=new Dimension(0,0);
		Iterator it=_pyramids.iterator();
		while (it.hasNext()) {
			Pyramid pyramid=(Pyramid)it.next();
			Dimension dim=getPyramidSize(g,pyramid);
			if (dim.width>maxPyramidSize.width)
				maxPyramidSize.width=dim.width;
			if (dim.height>maxPyramidSize.height)
				maxPyramidSize.height=dim.height;
		}

		Point position=new Point(LEFT_BORDER,TOP_BORDER+titleSpace);
		it=_pyramids.iterator();
		while (it.hasNext()) {
			Pyramid pyramid=(Pyramid)it.next();
			_pyramidBounds.add(new Rectangle(position,maxPyramidSize));
			drawPyramid(g,pyramid,position,maxPyramidSize);
			position.x=position.x+maxPyramidSize.width+HORIZONTAL_GAP;
			if (position.x+maxPyramidSize.width>getWidth()) {
				position.x=LEFT_BORDER;
				position.y=position.y+maxPyramidSize.height+VERTICAL_GAP;
			}
			
		}
		
		if (_inPrintJob==false) {
		    setPreferredSize(new Dimension(getWidth(),position.y));
		    revalidate();
		}
		g.setFont(oldFont);
	}
	
	public Dimension getPyramidSize(Graphics g,Pyramid pyramid) {
		int pyramidWidth=pyramid.getLayers()*BOX_SIZE;
		FontMetrics metrics = g.getFontMetrics();
        Rectangle2D bounds = metrics.getStringBounds(pyramid.getTopWord(), g);
        int stringWidth=(int)bounds.getWidth();
        int stringHeight=(int)bounds.getHeight();
        if (stringWidth>pyramidWidth)
        	pyramidWidth=stringWidth;
        int pyramidHeight=pyramid.getLayers()*BOX_SIZE+TEXT_GAP+stringHeight;
		return new Dimension(pyramidWidth,pyramidHeight);
		
	}
	
	public void drawPyramid(Graphics g, Pyramid py, Point pos, Dimension maxPySize) {
		int layers=py.getLayers();
		FontMetrics metrics = g.getFontMetrics();
		int ascent=metrics.getAscent();
        Rectangle2D wordBounds = metrics.getStringBounds(py.getTopWord(), g);
 		for (int layer=0;layer<layers;layer++) {
			PyramidElement[] pyElements=py.getElementsAtLayer(layer);
			int nboxes=pyElements.length;
			int x=pos.x+maxPySize.width/2-BOX_SIZE*nboxes/2;
			int y=pos.y+maxPySize.height-(int)wordBounds.getHeight()-TEXT_GAP-BOX_SIZE*(layers-layer);
			float maxValue=pyElements[0].getMaxValue();
			float minValue=pyElements[0].getMinValue();
			for (int box=0;box<nboxes;box++) {
				PyramidElement pyElement=pyElements[box];
				float value=pyElement.getValue();
				float red=1.0F;
				float green=1.0F;
				float blue=1.0F;
				if (value*maxValue >= 0) {
					blue=1.0F-value/maxValue;
					red=1.0F-value/maxValue;
				} else {
					blue=1.0F-(-value/-minValue);
					green=1.0F-(-value/-minValue);
				}
				g.setColor(new Color(red,green,blue));
				g.fillRect(x,y,BOX_SIZE,BOX_SIZE);
				g.setColor(Color.black);
				g.drawRect(x,y,BOX_SIZE,BOX_SIZE);
				x=x+BOX_SIZE;
			}
		}
		g.drawString(py.getTopWord(),pos.x+maxPySize.width/2-(int)Math.round(wordBounds.getWidth())/2,pos.y+maxPySize.height-(int)wordBounds.getHeight()+ascent);
	}

	public void setTitle(String title) {
	    if (title.length() > 0)
	        _title=title;
	    else
	        _title=null;
	    repaint();
	}
	   /* (non-Javadoc)
     * @see java.awt.print.Printable#print(java.awt.Graphics, java.awt.print.PageFormat, int)
     */
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        int ret=Printable.NO_SUCH_PAGE;
        if (pageIndex == 0) {
            _inPrintJob=true;
            Graphics2D g2d=(Graphics2D)graphics;
            double scaleX=pageFormat.getImageableWidth()/(double)getWidth();
            double scaleY=pageFormat.getImageableHeight()/(double)getHeight();
            double scale=scaleX;
            if (scaleX>scaleY)
                scale=scaleY;
            g2d.translate(pageFormat.getImageableX(),pageFormat.getImageableY());
            g2d.scale(scale,scale);
            paintComponent(g2d);
            _inPrintJob=false;
            ret=Printable.PAGE_EXISTS;
        }
        return ret;
    }
}

