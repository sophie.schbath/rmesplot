/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.gfam.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Vector;

import org.apache.log4j.Logger;

import fr.ssbgroup.jrmes.common.core.ProcessRunner;
import fr.ssbgroup.jrmes.common.core.ProcessWatcher;
import fr.ssbgroup.jrmes.gfam.core.Messages;

public class GfamRunner implements ProcessRunner {

	private static final Logger logger=Logger.getLogger("fr.ssbgroup.jrmes"); //$NON-NLS-1$

	private GfamRunParameters params;
	private String gfamPath;
	private String gfamVersion;

	Process gfamProcess;
	InputStream es;
	InputStream is;
	OutputStream os;

	ProcessWatcher watcher;
	
	public ProcessWatcher getWatcher() {
		return watcher;
	}

	public void setWatcher(ProcessWatcher watcher) {
		this.watcher = watcher;
	}

	public GfamRunner() {
		this.gfamPath=null;
		this.gfamVersion=null;
		this.params=null;
		this.gfamProcess=null;
		
	}

	public void configureGfamPath()  throws GfamNotFoundException {

		String pathString=System.getenv("PATH"); //$NON-NLS-1$
		String pathSeparator=System.getProperty("path.separator"); //$NON-NLS-1$
		String fileSeparator=System.getProperty("file.separator"); //$NON-NLS-1$
		String[] pathDirectories=pathString.split(pathSeparator);
		for (String directory : pathDirectories ){
			String gfamPathCandidate=directory+fileSeparator+"rmes.gfam"; //$NON-NLS-1$
			File gfamFile=new File(gfamPathCandidate);
			if (gfamFile.canRead()) {
				gfamPath=gfamFile.getAbsolutePath();
			} else {
				gfamPathCandidate=gfamPathCandidate+".exe"; //$NON-NLS-1$
				gfamFile=new File(gfamPathCandidate);
				if (gfamFile.canRead()) {
					gfamPath=gfamFile.getAbsolutePath();
				}
			}
		}
		if (gfamPath != null) {
			logger.info(Messages.getString("GfamRunner.gfamfound")+gfamPath); //$NON-NLS-1$
			findGfamVersion();			
		} else
			throw new GfamNotFoundException();

	}

	private void findGfamVersion()  throws GfamNotFoundException {
		String[] cmdArray={gfamPath,"--version"}; //$NON-NLS-1$
		gfamVersion=null;
		try {
			gfamProcess=Runtime.getRuntime().exec(cmdArray);
			InputStream is=gfamProcess.getInputStream();
			if (gfamProcess.waitFor()==0) {
				BufferedReader br=new BufferedReader(new InputStreamReader(is));
				while(br.ready() && gfamVersion==null) {
					String line=br.readLine();
					if (line.contains("version:")) { //$NON-NLS-1$
						String[] fields=line.split(":");
						gfamVersion=fields[fields.length-1].trim();
						logger.info(Messages.getString("GfamRunner.gfamversionfound")+gfamVersion); //$NON-NLS-1$
					}
				}

			} else 
				gfamPath=null;
		} catch (IOException e) {
			gfamPath=null;
		} catch (InterruptedException e) {
			gfamPath=null;
		}
		if (gfamPath==null || gfamVersion==null) {
			logger.error(Messages.getString("GfamRunner.gfamversionnotfound")); //$NON-NLS-1$
			throw new GfamNotFoundException();
		}
	}

	public String getGfamPath() {
		return gfamPath;
	}

	public String getGfamVersion() {
		return gfamVersion;
	}

	public void setGfamPath(String gfamPath)  throws GfamNotFoundException {
		this.gfamPath = gfamPath;
		/* Yuck ! Handle special case where gfamPath is set in controller after
		 * completing the detection of rmes. As the controller suffixes 'gfam' to
		 * the path found for rmes, care has to be taken on Windows (TM) platforms to
		 * switch gfam and exe in the final path.
		 */
		if (gfamPath.endsWith(".exe.gfam")) //$NON-NLS-1$
			this.gfamPath=gfamPath.replace(".exe.gfam",".gfam.exe"); //$NON-NLS-1$ //$NON-NLS-2$
		findGfamVersion();
	}

	public void setRunParameters(GfamRunParameters params) {
		this.params=params;
	}

	
	public void run() {


		/**
		 *  On Windows platforms, «long» filenames seem to confuse the command interpreter
		 *  when launching the R'MES process. Hence they have to be quoted. Unfortunately, 
		 *  the quotes do not work on Linux platforms. Not tested on Mac OS X yet.
		 */
		String filenamequoter=""; //$NON-NLS-1$
		if (System.getProperty("os.name").startsWith("Windows")) //$NON-NLS-1$ //$NON-NLS-2$
			filenamequoter="\""; //$NON-NLS-1$
		
		Vector<String> cmdarray=new Vector<String>();
		cmdarray.add(filenamequoter+gfamPath+filenamequoter);

		try {

			
			cmdarray.add("-t"); //$NON-NLS-1$
			cmdarray.add("\""+params.getFamilyName()+"\""); //$NON-NLS-1$ //$NON-NLS-2$
			
			cmdarray.add("-p"); //$NON-NLS-1$
			cmdarray.add(params.getPattern());

			String[] cmds=new String[cmdarray.size()];
			cmds=cmdarray.toArray(cmds);

			String cmdLine=""; //$NON-NLS-1$
			for (String arg : cmdarray)
				cmdLine=cmdLine+" "+arg; //$NON-NLS-1$
			logger.info(Messages.getString("GfamRunner.gfamcmdline")+cmdLine); //$NON-NLS-1$

			gfamProcess=Runtime.getRuntime().exec(cmds);
			is=gfamProcess.getInputStream();
			os=gfamProcess.getOutputStream();
			es=gfamProcess.getErrorStream();	
			if (watcher != null)
				watcher.processStarted(this);
	
			boolean completed=false;
			StringBuffer errorMessage=new StringBuffer();
			StringBuffer outMessage=new StringBuffer();
			
			try {
				PrintWriter pw=new PrintWriter(params.getResultFile());
			
			int exitValue=0;
			do {
				if (is.available()>0) {
					BufferedReader br=new BufferedReader(new InputStreamReader(is));
					while (br.ready()) {
						outMessage.append(br.readLine()+"\n"); //$NON-NLS-1$
					}
					pw.print(outMessage);
					outMessage.setLength(0);
				}
				if (es.available()>0) {
					BufferedReader br=new BufferedReader(new InputStreamReader(es));
					while (br.ready()) {
						errorMessage.append(br.readLine()+"\n"); //$NON-NLS-1$
					}
					logger.warn(errorMessage.toString());
					errorMessage.setLength(0);
				}
				try {
					if (completed==false) {
						exitValue=gfamProcess.exitValue();
						if (exitValue==0)
							logger.info(Messages.getString("GfamRunner.gfamdone")); //$NON-NLS-1$
						
						completed=true;
						if (is.available()>0) {
							BufferedReader br=new BufferedReader(new InputStreamReader(is));
							while (br.ready()) {
								outMessage.append(br.readLine()+"\n"); //$NON-NLS-1$
							}
							pw.print(outMessage);
						}
						if (es.available()>0) {
							BufferedReader br=new BufferedReader(new InputStreamReader(es));
							while (br.ready()) {
								errorMessage.append(br.readLine()+"\n"); //$NON-NLS-1$
							}
							logger.warn(errorMessage.toString());
						}
					}

				} catch (IllegalThreadStateException ex) {
					;//ex.printStackTrace();
				}
			} while (completed==false);
			pw.close();
			if (watcher != null) {
				if (exitValue==0)
					watcher.processFinished(this);
				else 
					watcher.processFailed(this,Messages.getString("GFamRunner.exitvalue")+exitValue); //$NON-NLS-1$
			}
			} catch (FileNotFoundException e) {
				if (watcher != null)
					watcher.processFailed(this,Messages.getString("GfamRunner.cantwrite"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
