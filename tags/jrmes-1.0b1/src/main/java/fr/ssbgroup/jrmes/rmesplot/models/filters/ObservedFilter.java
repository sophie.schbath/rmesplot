/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.models.filters;

import java.util.HashSet;
import java.util.Iterator;

import fr.ssbgroup.jrmes.rmesplot.models.ElementStats;

/**
 * @author mark
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ObservedFilter implements ElementFilter {

    private int _threshold;
    private boolean _above;

    public ObservedFilter(int threshold, boolean above) {
        _threshold = threshold;
        _above=above;
    }

    /* (non-Javadoc)
     * @see rmes.models.filters.ElementFilter#apply(java.util.HashSet)
     */
    public HashSet apply(HashSet elements) {
       HashSet newElements=new HashSet();
        Iterator it=elements.iterator();
        while (it.hasNext()) {
            ElementStats es=(ElementStats)it.next();
            if ((_above ==true && es.getCount().intValue() >= _threshold) ||
            		(_above == false && es.getCount().intValue() <= _threshold)) {
                newElements.add(es);
            }
        }
        return newElements;
    }

}