/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;

import javax.swing.JPanel;
import javax.swing.JViewport;
import javax.swing.SpinnerModel;

import fr.ssbgroup.jrmes.rmesplot.controls.RMESPlotController;
import fr.ssbgroup.jrmes.rmesplot.models.ElementStats;
import fr.ssbgroup.jrmes.rmesplot.models.Model;

/**
 * This class draws a graphical representation of on or two R'MES result sets.
 * The PlotArea is the GUI component appearing in the right half of RMESPlot.
 * Apart from drawing the actual data, it also allows the user to
 * select/deselect data points with the mouse.
 * 
 * @author $Author: mhoebeke $
 * @version $Revision: 123 $
 */
public class PlotArea extends JPanel implements Printable {

    private static final int LEFTBORDER = 20;

    public static final int STATE_SELECT_WORD = 0;
    
    public static final int STATE_ZOOM_IN = 1;
    
    public static final int STATE_ZOOM_OUT = 2;

    private static final int RIGHTBORDER = 20;

    private static final int TOPBORDER = 20;

    private static final int BOTTOMBORDER = 20;

    private static int HEADERSPACE = 0;

    private static final int CROSS_SIZE = 3;

    private static final int TICK_SIZE = 2;

    private static final int MIN_TICKS = 5;

    private static final Dimension INIT_DIMENSION=new Dimension(800,600);
    
    private static final Color COLOR_HIGHLIGHT = Color.orange;

    private static final Color COLOR_UNMATCHED = Color.red;

    private static final Color COLOR_MATCHED = Color.blue;
    
    private static final Cursor[] CURSORS={ Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR), 
            								Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR), 
            								Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR)};

    RMESPlotController _controller;
    
    SpinnerModel _spinnerModel;

    float _xMin;

    float _xMax;

    float _yMin;

    float _yMax;
    
    float _xMinInit;
    
    float _xMaxInit;
    
    float _yMinInit;
    
    float _yMaxInit;

    Hashtable _xData;

    Hashtable _yData;

    String _title;

    Hashtable _wordsByPositions;

    Hashtable _positionsByWords;

    HashSet _hWords;

    int _width;

    int _height;
     
    float _zoomFactor;
    
    boolean _filtersEnabled;

    boolean _showAllLabels;
    
    Model _modelOnX;
    
    Model _modelOnY;
    
    String _titleOnX;
    
    String _titleOnY;
    
    int _state;

    /**
     * Creates a new PlotArea object.
     */
    PlotArea(RMESPlotController controller, SpinnerModel spinnerModel) {
        super();

        _controller = controller;
        _spinnerModel= spinnerModel;
        
        Boolean tmpBoolean=Boolean.valueOf((String)controller.getProperty("plotpanel.plotsfiltered"));
        _filtersEnabled=tmpBoolean.booleanValue();
        
        _showAllLabels = false;

        setLayout(new BorderLayout());
        setBackground(Color.white);
        setForeground(Color.black);

        _xMin = -20;
        _xMax = 20;
        _yMin = -20;
        _yMax = 20;
        _xMinInit=_xMin;
        _xMaxInit=_xMax;
        _yMinInit=_yMin;
        _yMaxInit=_yMax;

        _xData = new Hashtable();
        _yData = new Hashtable();
        _title = null;

        _wordsByPositions = new Hashtable();
        _positionsByWords = new Hashtable();
        _hWords = new HashSet();
        
        _width=-1;
        _height=-1;
        _zoomFactor=1.0F;
        
        _state=STATE_SELECT_WORD;
        
        _modelOnX=null;
        _modelOnY=null;
        
        addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (_state == STATE_SELECT_WORD)
                    selectWord(e.getX(), e.getY());
                if (_state == STATE_ZOOM_IN || _state == STATE_ZOOM_OUT)
                    zoomAndFocus(e.getX(),e.getY());
            }
        });
        _controller.setPlotArea(this);
        setPreferredSize(INIT_DIMENSION);
        setCursor(CURSORS[_state]);
    }

    public void plotOnX(Model model, String title) {
        _modelOnX=model;
        _titleOnX=title;
        refreshPlotOnX();
        String newTitle=_titleOnX;
        if (_titleOnY != null)
        	newTitle=newTitle+" / "+_titleOnY;
        setTitle(newTitle);
    }
    
    public void refreshPlotOnX() {
        if (_modelOnX != null) {
            _xData = new Hashtable();
            _xMin = (float) 1e38;
            _xMax = (float) -1e38;
            
            HashSet elements=_modelOnX.getElements();
            if (_filtersEnabled) {
                HashSet newElements=_controller.applyFilters(elements);
                elements=newElements;
            }
            if (elements.size()>0) {
            	Iterator it = elements.iterator();
            	while (it.hasNext()) {
            		ElementStats es = (ElementStats) it.next();
            		Float valueFloat = es.getScore();
            		_xData.put(es.getName(), valueFloat);
            		float value = valueFloat.floatValue();
            		if (value < _xMin) {
            			_xMin = value;
            		}
            		if (value > _xMax) {
            			_xMax = value;
            		}
            	}
            	float xAmp = (_xMax - _xMin) * (float) 1.10;
            	_xMin = _xMin - (xAmp * (float) 0.05);
            	_xMax = _xMax + (xAmp * (float) 0.05);
            } else {
            	_xMin=-20;
            	_xMax= 20;
            }
        	_xMinInit=_xMin;
        	_xMaxInit=_xMax;
            repaint();
        }
    }

   public void plotOnY(Model model, String title) {
       _modelOnY=model;
       	_titleOnY=title;
       refreshPlotOnY();
       String newTitle="/ "+_titleOnY;
       if (_titleOnX != null)
       	newTitle=_titleOnX+" "+newTitle;
       setTitle(newTitle);
   }
   
   public void refreshPlotOnY() {
       if (_modelOnY != null) {
           _yData = new Hashtable();
           _yMin = (float) 1e38;
           _yMax = (float) -1e38;
           
           HashSet elements=_modelOnY.getElements();
           if (_filtersEnabled) {
               HashSet newElements=_controller.applyFilters(elements);
               elements=newElements;
           }
           if (elements.size()>0) {
        	   Iterator it = elements.iterator();
        	   while (it.hasNext()) {
        		   ElementStats es = (ElementStats) it.next();
        		   Float valueFloat = es.getScore();
        		   _yData.put(es.getName(), valueFloat);
        		   float value = valueFloat.floatValue();
        		   if (value < _yMin) {
        			   _yMin = value;
        		   }
        		   if (value > _yMax) {
        			   _yMax = value;
        		   }
        	   }
        	   float yAmp = (_yMax - _yMin) * (float) 1.10;
        	   _yMin = _yMin - (yAmp * (float) 0.05);
        	   _yMax = _yMax + (yAmp * (float) 0.05);
           } else {
        	   _yMin = -20;
        	   _yMax = 20;
           }
           _yMinInit=_yMin;
           _yMaxInit=_yMax;
           repaint();
       }
    }

    public void setTitle(String title) {
        _title = title;
        repaint();
    }

    public void setFiltersEnabled(boolean filtersEnabled) {
        if (filtersEnabled != _filtersEnabled) {
            _filtersEnabled=filtersEnabled;
            refreshPlotOnX();
            refreshPlotOnY();
        }
    }
    /**
     * Method allowing the drawing of all the name of the words.
     * 
     * @param val
     *            a boolean which decides to draw or not the all the labels of
     *            the words.
     */
    public void setShowAllLabels(boolean val) {
        _showAllLabels = val;
        repaint();
    }

    /**
     * Method performing the actual drawing of the data points.
     * 
     * @param g
     *            the Graphics context on which the drawing has to be performed.
     */
    public void drawHWords(Graphics g) {
        Iterator it;
        if (_showAllLabels == true) {
            it = _wordsByPositions.values().iterator();
        } else {
            it = _hWords.iterator();
        }
        while (it.hasNext()) {
            String word = (String) it.next();
            if (_positionsByWords.get(word) != null) {
                String coordString = (String) _positionsByWords.get(word);
                String[] coords = coordString.split("V");
                int xpos = Integer.parseInt(coords[0]);
                int ypos = Integer.parseInt(coords[1]);
                Color oc = g.getColor();
                g.setColor(COLOR_HIGHLIGHT);
                drawCross(g, xpos, ypos);
                g.setColor(oc);
                g.drawString(word, xpos + LEFTBORDER + CROSS_SIZE, ypos
                        - CROSS_SIZE + HEADERSPACE);
            }
        }
    }

    /**
     * Method called to toggle highlighting for a given word.
     * 
     * @param hword
     *            the word whose highlighting must be toggled.
     */
    public void selectWord(String hword) {
        String coordString = (String) _positionsByWords.get(hword);
        if (coordString != null) {
            String[] coords = coordString.split("V");
            int xpos = Integer.parseInt(coords[0]);
            int ypos = Integer.parseInt(coords[1]);
            selectWord(xpos + LEFTBORDER, ypos + HEADERSPACE);
        }
    }

    /**
     * Method called to toggle highlighting of a word at a given position.
     * 
     * @param x
     *            the X coordinate of the word to toggle.
     * @param y
     *            the Y coordinate of the word to toggle.
     */
    public void selectWord(int x, int y) {
        x -= LEFTBORDER;
        y -= HEADERSPACE;
        //Lookup exact word position.
        String lookup = new String(x + "V" + y);
        String word = (String) _wordsByPositions.get(lookup);
        if (word == null) {
            //Lookup neighboring positions up to CROSS_SIZE.
            for (int dist = 1; (dist <= CROSS_SIZE) && (word == null); dist++) {
                for (int dx = -dist; (dx <= dist) && (word == null); dx += dist) {
                    for (int dy = -dist; (dy <= dist) && (word == null); dy += dist) {
                        lookup = new String((x + dx) + "V" + (y + dy));
                        word = (String) _wordsByPositions.get(lookup);
                    }
                }
            }
        }
        // if word exists, it is hightlighting.
        if (word != null) {
           if (_hWords.contains(word)) {
                _hWords.remove(word);
            } else {
                _hWords.add(word);
            }
        }
        repaint();
    }

    /**
     * Method drawing the ticks on the horizontal axis.
     * 
     * @param g
     *            the Graphics context on which to do the drawing.
     * @param ypos
     *            the vertical position of the horizontal axis.
     */
    private void drawXTicks(Graphics g, int ypos) {
        FontMetrics metrics = g.getFontMetrics();
        float amp = _xMax - _xMin;
        float gap = amp / 10;
        float scalefactor=1;
        while (gap*scalefactor<1)
            scalefactor*=10;
        float factor = Math.round(_xMin*scalefactor)/scalefactor;
        while (factor < _xMax) {
            float xpos = (factor - _xMin) / (_xMax - _xMin) * _width;
            g.drawLine(LEFTBORDER + (int) xpos, (HEADERSPACE + ypos)
                    - TICK_SIZE, LEFTBORDER + (int) xpos, HEADERSPACE + ypos
                    + TICK_SIZE);
            String marker = new String("" + factor);
            if (factor != 0) {
                g.drawString(marker, LEFTBORDER + (int) xpos,
                        (HEADERSPACE + (int) ypos) - TICK_SIZE);
            }
            factor = Math.round((factor + gap)*scalefactor)/scalefactor;
        }
    }

    /**
     * Method drawing the ticks on the vertical axis.
     * 
     * @param g
     *            the Graphics context on which to do the drawing.
     * @param xpos
     *            the horizontal position of the vertival axis.
     */
    private void drawYTicks(Graphics g, int xpos) {
        FontMetrics metrics = g.getFontMetrics();
        float amp = _yMax - _yMin;
        float gap = amp / 10;
        float scalefactor=1;
        while (gap*scalefactor<1)
            scalefactor=scalefactor*10;
        float factor=Math.round((_yMin+gap)*scalefactor)/scalefactor;
        while (factor < _yMax) {
            float ypos = _height
                    - ((factor - _yMin) / (_yMax - _yMin) * _height);
            g.drawLine((LEFTBORDER + xpos) - TICK_SIZE, HEADERSPACE
                    + (int) ypos, LEFTBORDER + xpos + TICK_SIZE, HEADERSPACE
                    + (int) ypos);
            String marker = new String("" + factor);
            if (factor != 0 && factor > 0) {
                g.drawString(marker, LEFTBORDER + (int) xpos + TICK_SIZE,
                        HEADERSPACE + (int) ypos);
            }
            if (factor != 0 && factor < 0) {
                Rectangle2D bounds = metrics.getStringBounds(marker, g);
                g.drawString(marker, LEFTBORDER + (int) xpos + TICK_SIZE,
                        HEADERSPACE + (int) ypos + (int) bounds.getHeight());
            }
            factor=Math.round((factor+gap)*scalefactor)/scalefactor;
        }
    }

    /**
     * Method drawing both axes of the plot.
     * 
     * @param g
     *            the Graphics context on which to draw the axes.
     */
    private void drawAxes(Graphics g) {
        g.drawRect(LEFTBORDER, HEADERSPACE, _width, _height);
        if ((_xMin * _xMax) < 0) {
            int xpos = (int) (-_xMin / (_xMax - _xMin) * _width);
            g.drawLine(LEFTBORDER + xpos, HEADERSPACE, LEFTBORDER + xpos,
                    HEADERSPACE + _height);
            drawYTicks(g, xpos);
        }
        if ((_yMin * _yMax) < 0) {
            int ypos = _height - (int) (-_yMin / (_yMax - _yMin) * _height);
            g.drawLine(LEFTBORDER, HEADERSPACE + ypos, _width + LEFTBORDER,
                    HEADERSPACE + ypos);
            drawXTicks(g, ypos);
        }
    }

    /**
     * Method performing the actual drawing of the data sets.
     * 
     * @param g
     *            the Graphics context on which to perform the drawing.
     */
    public void plotData(Graphics g) {
        _wordsByPositions.clear();
        _positionsByWords.clear();
        Color oldColor = g.getColor();
        //Plot data in X dataset and data common to X and Y datasets.
        for (Enumeration e = _xData.keys(); e.hasMoreElements();) {
            g.setColor(COLOR_MATCHED);
            String word = (String) e.nextElement();
            Float statXFloat = (Float) _xData.get(word);
            float statX = statXFloat.floatValue();
            if ((statX >= _xMin) && (statX <= _xMax)) {
                int xpos = (int) ((statX - _xMin) / (_xMax - _xMin) * _width);
                Float statYFloat = (Float) _yData.get(word);
                float statY = 0;
                if (statYFloat != null) {
                    statY = statYFloat.floatValue();
                } else {
                    g.setColor(COLOR_UNMATCHED);
                }
                if ((statY >= _yMin) && (statY <= _yMax)) {
                    int ypos = _height
                            - (int) ((statY - _yMin) / (_yMax - _yMin) * _height);
                    String positionString = new String(xpos + "V" + ypos);
                    _wordsByPositions.put(positionString, word);
                    _positionsByWords.put(word, positionString);
                    drawCross(g, xpos, ypos);
                }
            }
        }
        g.setColor(COLOR_UNMATCHED);
        //Plot data exclusively in Y data set.
        for (Enumeration e = _yData.keys(); e.hasMoreElements();) {
            String word = (String) e.nextElement();
            Float statXFloat = (Float) _xData.get(word);
            if (statXFloat == null) {
                Float statYFloat = (Float) _yData.get(word);
                float statY = statYFloat.floatValue();
                float statX = 0;
                if ((statX >= _xMin) && (statX <= _xMax)) {
                    int xpos = (int) ((statX - _xMin) / (_xMax - _xMin) * _width);
                    if ((statY >= _yMin) && (statY <= _yMax)) {
                        int ypos = _height
                                - (int) ((statY - _yMin) / (_yMax - _yMin) * _height);
                        String positionString = new String(xpos + "V" + ypos);
                        _wordsByPositions.put(positionString, word);
                        _positionsByWords.put(word, positionString);
                        drawCross(g, xpos, ypos);
                    }
                }
            }
        }
        g.setColor(oldColor);
    }

    /**
     * Method drawing a diagonal line for easy spotting of outlying data.
     * 
     * @param g
     *            the Graphics context on which to perform the drawing.
     */
    private void drawDiagonal(Graphics g) {
        float urx = _xMax;
        float ury = _yMax;
        if (urx > ury) {
            urx = (_xMax * ury) / urx;
        } else if (ury > urx) {
            ury = (_yMax * urx) / ury;
        }
        float llx = _xMin;
        float lly = _yMin;
        if (llx < lly) {
            llx = (_xMin * lly) / llx;
        } else if (lly < llx) {
            lly = (_yMin * llx) / lly;
        }
        float xmax = (urx - _xMin) / (_xMax - _xMin) * _width;
        float xmin = (llx - _xMin) / (_xMax - _xMin) * _width;
        float ymax = _height - ((ury - _yMin) / (_yMax - _yMin) * _height);
        float ymin = _height - ((lly - _yMin) / (_yMax - _yMin) * _height);
        g.drawLine(LEFTBORDER + (int) xmin, HEADERSPACE + (int) ymin,
                LEFTBORDER + (int) xmax, HEADERSPACE + (int) ymax);
    }

    /**
     * Method composing the plot titles
     * 
     * @param g
     *            the Graphics context on which to draw the titles.
     */
    private void drawTitle(Graphics g) {
        if (_title != null) {
            FontMetrics metrics = g.getFontMetrics();
            Rectangle2D bounds = metrics.getStringBounds(_title, g);
            g.drawString(_title, LEFTBORDER
                    + (int) ((_width - bounds.getWidth()) / 2), HEADERSPACE
                    - (int) (2.1 * bounds.getHeight()));
        }
    }

    /**
     * Method drawing a cross for a given data point.
     * 
     * @param g
     *            the Graphics context on which to draw the cross.
     * @param x
     *            the horizontal position of the cross.
     * @param y
     *            the vertical position of the cross.
     */
    private void drawCross(Graphics g, int x, int y) {
        g.drawLine((LEFTBORDER + x) - CROSS_SIZE, HEADERSPACE + y, LEFTBORDER
                + x + CROSS_SIZE, HEADERSPACE + y);
        g.drawLine(LEFTBORDER + x, (HEADERSPACE + y) - CROSS_SIZE, LEFTBORDER
                + x, HEADERSPACE + y + CROSS_SIZE);
    }

    /**
     * The method called by the AWT to refresh the widget.
     * 
     * @param g
     *            the Graphics context on which this component should draw its
     *            contents.
     */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (_title != null) {
            FontMetrics f = g.getFontMetrics();
            Rectangle2D bounds = f.getStringBounds(_title, g);
            HEADERSPACE = (int) ((bounds.getHeight() * 2.1) + TOPBORDER);
        } else {
            HEADERSPACE = TOPBORDER;
        }
        _width = getWidth() - (LEFTBORDER + RIGHTBORDER);
        _height = getHeight() - (HEADERSPACE + BOTTOMBORDER);
        drawAxes(g);
        drawDiagonal(g);
        drawTitle(g);
        plotData(g);
        drawHWords(g);
    }

    /**
     * Method setting zoom factor and resizing the picture.
     * 
     * @param factZoom
     *            a float corresponding to the multiplication factor of the
     *            drawing.
     */
    public void setZoomFactor(float zoomFactor) {
    	_zoomFactor=zoomFactor;
    	setPreferredSize(new Dimension((int)(INIT_DIMENSION.getWidth()*_zoomFactor),
    	        (int)(INIT_DIMENSION.getHeight()*_zoomFactor)));
    	revalidate();
    }
    
    public void setState(int state) {
        _state=state;
        setCursor(CURSORS[_state]);
        
    }

    public void clearXPlot() {
        _xData.clear();
        _xMin = _xMinInit;
        _xMax = _xMaxInit;
        _modelOnX=null;
        if (_modelOnY==null)
            _hWords.clear();
        repaint();
    }

    public void clearYPlot() {
        _yData.clear();
        _yMin = _yMinInit;
        _yMax = _yMaxInit;
        _modelOnY=null;
        if (_modelOnX==null)
            _hWords.clear();
        repaint();
    }
    
    public void clearHWords() {
        _hWords.clear();
        repaint();
    }
    public void zoomAndFocus(int x, int y) {
        Object zoomObject=null;
        if (_state == STATE_ZOOM_IN) {
            zoomObject=_spinnerModel.getNextValue();
        } else {
            zoomObject=_spinnerModel.getPreviousValue();
        }
        if (zoomObject != null ) {
            JViewport viewport=(JViewport)getParent();
            Point viewOffset=viewport.getViewPosition();
            float deltaX=x-viewOffset.x;
            float deltaY=y-viewOffset.y;
            float widthBefore=(float)INIT_DIMENSION.getWidth()*_zoomFactor;
            float heightBefore=(float)INIT_DIMENSION.getHeight()*_zoomFactor;
            float fracX=((float)(x-LEFTBORDER))/(float)(widthBefore-LEFTBORDER-RIGHTBORDER);
            float dataX=_xMin+fracX*(_xMax-_xMin);
            float fracY=((float)(y-HEADERSPACE))/(float)(heightBefore-HEADERSPACE-BOTTOMBORDER);
            float dataY=_yMax-fracY*(_yMax-_yMin);
            _spinnerModel.setValue(zoomObject);
            float widthAfter=(float)INIT_DIMENSION.getWidth()*_zoomFactor;
            float xAfter=LEFTBORDER+(widthAfter-LEFTBORDER-RIGHTBORDER)*(dataX-_xMin)/(_xMax-_xMin);
            float heightAfter=(float)INIT_DIMENSION.getHeight()*_zoomFactor;
            float yAfter=HEADERSPACE+(heightAfter-HEADERSPACE-BOTTOMBORDER)*(_yMax-dataY)/(_yMax-_yMin);
            xAfter=xAfter-deltaX;
            yAfter=yAfter-deltaY;
            viewport.setViewPosition(new Point((int)xAfter,(int)yAfter));
            
        }
    }

    /* (non-Javadoc)
     * @see java.awt.print.Printable#print(java.awt.Graphics, java.awt.print.PageFormat, int)
     */
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        int ret=Printable.NO_SUCH_PAGE;
        if (pageIndex == 0) {
            Graphics2D g2d=(Graphics2D)graphics;
            double scaleX=pageFormat.getImageableWidth()/(double)getWidth();
            double scaleY=pageFormat.getImageableHeight()/(double)getHeight();
            double scale=scaleX;
            if (scaleX>scaleY)
                scale=scaleY;
            g2d.translate(pageFormat.getImageableX(),pageFormat.getImageableY());
            g2d.scale(scale,scale);
            paintComponent(g2d);
            
            ret=Printable.PAGE_EXISTS;
        }
        return ret;
    }
}