/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.views;

import java.awt.Component;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.event.ListSelectionEvent;

import fr.ssbgroup.jrmes.rmesplot.controls.RMESPlotController;
import fr.ssbgroup.jrmes.rmesplot.models.Model;

/**
 * Class defining the tabbed pane for the two table views. This class defines
 * the behavior of the tabbed pane where each of the two data sets are
 * displayed.
 * 
 * @author $Author: mhoebeke $
 * @version $Revision: 123 $
 */
public class DataTableTabbedPane extends JTabbedPane {

    RMESPlotController _controller;

    DataTablePanel _layeredTabbedPane;

    int _tabIndex;

    JPopupMenu _popup;

    JMenuItem _item;

    /**
     * Creates a new ResultTablePane object.
     * 
     * @param tree
     *            the ResultTree containing the currently available results.
     * @param plotPanel
     *            the PlotArea where the graphics are drawn.
     * @param pyramidPanel
     *            the PyramidArea where pyramids are drawn.
     */
    public DataTableTabbedPane(RMESPlotController controller) {
        super(JTabbedPane.TOP);

        _controller = controller;
        _tabIndex = 0;

        _controller.setDataTableTabbedPane(this);

    }

    public void addModel(Model model,String description) {
    	++_tabIndex;
    	String tabName=""+_tabIndex+"-"+_controller.buildTitleString(model);
        DataTablePanel dataTablePanel = new DataTablePanel(_controller, model,
                _tabIndex);
        addTab(tabName, null, dataTablePanel, description);
        setSelectedComponent(dataTablePanel);
    }
    
    public void applyFilters() {
        Component[] components=getComponents();
        for (int i=0;i<components.length;i++) {
            if (components[i] instanceof DataTablePanel) {
                ((DataTablePanel)components[i]).applyFilters();
            }
        }
    }

    /**
     * Method reacting to user selections on the tabular data. This method is
     * called when the user selects/deselects a value in the table, and causes
     * the corresponding value to be highlighted/unhighlighted in the plot area.
     * 
     * @param le
     *            the actual ListSelectionEvent.
     */
    public void valueChanged(ListSelectionEvent le) {

    }

    /**
     * Method displaying a window that gives a warning message. No pyramid can
     * be displaying with word family or degenerated words.
     */
    public void dialogWindow() {
        JDialog dialog;
        JFrame cadre = new JFrame("Warning");
        JPanel pan = new JPanel();
        JLabel lab = new JLabel(
                "you can't display pyramid of a word family or a degenerated word");
        pan.add(lab);
        Object[] options = { "OK" };
        JOptionPane pane = new JOptionPane(pan, JOptionPane.WARNING_MESSAGE,
                JOptionPane.DEFAULT_OPTION, null, options);
        dialog = pane.createDialog(cadre, "Warning");
        dialog.pack();
        dialog.setVisible(true);
    }

}