/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.main.ui;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import fr.ssbgroup.jrmes.JRmes;
import fr.ssbgroup.jrmes.main.ui.actions.ConfigureRmesAction;
import fr.ssbgroup.jrmes.main.ui.actions.QuitAction;

/**
 * {@link MenuBar} builds the menu bar for JR'MES.
 * @author mark
 *
 */
@SuppressWarnings("serial")
public class MenuBar extends JMenuBar {
	
	JRmes controller=null;
	AboutDialog aboutDialog=null;
	
	/**
	 * Class constructor using a {@link JRmes} instance.
	 * 
	 * @param controller the top-level {@link JRmes} controller.
	 */
	public MenuBar(JRmes controller) {
		super();
		this.controller=controller;
		initMenuBar();
	}
	
	protected void initMenuBar() {
		
		JMenu menu=new JMenu();
		menu.setText(Messages.getString("MenuBar.file")); //$NON-NLS-1$
		add(menu);
		
		JMenuItem item=new JMenuItem();
		item.setAction(new ConfigureRmesAction(controller));
		menu.add(item);
		
		item=new JMenuItem();
		item.setAction(new QuitAction(controller));
		menu.add(item);
		
		      
        add(Box.createHorizontalGlue());
        
        menu=new JMenu(Messages.getString("MenuBar.help"));
        add(menu);
        
        final AboutDialog aboutDialog=new AboutDialog();
        AbstractAction action=new AbstractAction(Messages.getString("HelpMenu.about")) {
            public void actionPerformed(ActionEvent e) {
                aboutDialog.setVisible(true);
            }
        };
        
        item=new JMenuItem(action);
        menu.add(item);
		
	}

}
