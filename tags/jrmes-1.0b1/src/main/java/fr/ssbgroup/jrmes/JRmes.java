/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

import fr.ssbgroup.jrmes.common.core.ProcessWatcher;
import fr.ssbgroup.jrmes.gfam.core.GfamNotFoundException;
import fr.ssbgroup.jrmes.gfam.core.GfamRunParameters;
import fr.ssbgroup.jrmes.gfam.core.GfamRunner;
import fr.ssbgroup.jrmes.main.ui.MainFrame;
import fr.ssbgroup.jrmes.rmes.core.RmesNotFoundException;
import fr.ssbgroup.jrmes.rmes.core.RmesRunParameters;
import fr.ssbgroup.jrmes.rmes.core.RmesRunner;

/**
 * JRmes is the main class of the JRmes application.
 * 
 * JRmes handles the building of the UI after trying to figure out what flavour of R'MES is
 * available in a set of default directories. In MVC parlance, it it implements all the toplevel C-related behaviour.
 */
public class JRmes  implements Runnable {
	
	static Logger logger=Logger.getLogger("fr.ssbgroup.jrmes"); //$NON-NLS-1$

	/**
	 * Constant representing the "statistics computation" main function. 
	 */
	public static final String FUNCTION_STATS=Messages.getString("JRmes.computescores"); //$NON-NLS-1$
	
	/**
	 * Constant representing the "skew computation" main function. 
	 */
	
	public static final String FUNCTION_SKEW=Messages.getString("JRmes.computeskew"); //$NON-NLS-1$
	
	/**
	 * Constant representing the "score comparison" main function. 
	 */
	
	public static final String FUNCTION_COMPARE=Messages.getString("JRmes.comparescores"); //$NON-NLS-1$
	
	/**
	 * A table of constants describing the "main functions" available for R'MES :
	 */
	static final String[] mainFunctions={FUNCTION_STATS, FUNCTION_SKEW, FUNCTION_COMPARE};
	
	/**
	 * Constant representing the Gaussian approximation method.
	 */
	public static final String METHOD_GAUSS=Messages.getString("JRmes.gaussian"); //$NON-NLS-1$
	
	
	/**
	 * Constant representing the Poisson approximation method.
	 */
	public static final String METHOD_POISSON=Messages.getString("JRmes.poisson"); //$NON-NLS-1$
	
	/**
	 * Constant representing the Compound Poisson approximation method.
	 */
	public static final String METHOD_COMPOUNDPOISSON=Messages.getString("JRmes.compoundpoisson"); //$NON-NLS-1$
	
	/**
	 * A table of constants describing the available approximation methods.
	 */
	static final String[] methods={METHOD_GAUSS, METHOD_COMPOUNDPOISSON, METHOD_POISSON};
	
	/**
	 * Constant representing the fact that computations will be performed on words and not on families.
	 */
	public static final String USE_WORDS=Messages.getString("JRmes.words"); //$NON-NLS-1$
	
	/**
	 * Constant representing the fact that computations will be performed on families and not on words.
	 */
	public static final String USE_FAMILIES=Messages.getString("JRmes.families"); //$NON-NLS-1$
	
	
	/**
	 * The frame with the toolbar and the logging text area.
	 */
	MainFrame mf=null;
	
	/**
	 * The object wrapping "rmes" run paramters.
	 */
	RmesRunParameters rmesrunparams=null;
	
	/**
	 * The command-like object encapsulating the excution of "rmes".
	 */
	RmesRunner rmesrunner=null;
	
	/**
	 * An observer-like object following the execution of "rmes".
	 */
	ProcessWatcher rmeswatcher=null;
	
	/**
	 * Indicates whether a supported version of "rmes" was found.
	 */
	boolean rmesConfigured=false;
	
	/**
	 * The object wrapping "rmes" run paramters.
	 */
	GfamRunParameters gfamrunparams=null;
	
	/**
	 * The command-like object encapsulating the excution of "rmes.gfam".
	 */
	GfamRunner gfamrunner=null;
	
	/**
	 * An observer-like object following the execution of "rmes.gfam".
	 */
	ProcessWatcher gfamwatcher=null;
	
	/**
	 * Indicates whether a supported version of "rmes.gfam" was found.
	 */
	boolean gfamConfigured=false;
	
	
	/**
	 * Method trying to set up the environment with some default configuration parameters :
	 * <ul>
	 * <li>a default directory for the generation of result files,
	 * <li>the path to the actual executables if they are located in one of the default directories.
	 * </ul>
	 *  On exit, the following instance variables are set :
	 *  <ul>
	 *  <li>runparams/gfamrunparams : with sensible values for most of the execution parameters (except the input sequence file for "rmes").
	 *  <li>runner/gfamrunner : with (possibly) the path to "rmes"/"rmes.gfam".
	 *  <li>rmesConfigured/gfamConfigured : whose boolean value indicates wether "rmes"/"rmes.gfam" is usable.
	 *  </ul>
	 */
	public void initialize() {
		SimpleDateFormat df=new SimpleDateFormat("yyyyMMddhhmmss");

		String defaultDir=System.getProperty("user.dir")+System.getProperty("file.separator");

		rmesrunparams=new RmesRunParameters();
		File resultprefix=new File(defaultDir+"rmes-"+df.format(Calendar.getInstance().getTime())); //$NON-NLS-1$ //$NON-NLS-2$
		rmesrunparams.setResultFile(resultprefix);
		rmesrunner=new RmesRunner();
		try {
			rmesrunner.configureRmesPath();
			rmesConfigured=true;
		}
		catch (RmesNotFoundException e) {
			rmesConfigured=false;
			logger.error(Messages.getString("JRmes.normesprogram")); //$NON-NLS-1$
		}
		
		gfamrunparams=new GfamRunParameters();
		resultprefix=new File(defaultDir+"rmes.gfam-"+df.format(Calendar.getInstance().getTime())); //$NON-NLS-1$ //$NON-NLS-2$
		gfamrunparams.setResultFile(resultprefix);
		
		gfamrunner=new GfamRunner();
		try {
			gfamrunner.configureGfamPath();
			gfamConfigured=true;
		} catch (GfamNotFoundException e) {
			gfamConfigured=false;
			logger.error(Messages.getString("JRmes.nogfamprogram")); //$NON-NLS-1$
		}
	
	}
	
	/**
	 * Builds the UI and starts handling user interaction.
	 */
	public void run() {
		mf=new MainFrame(this);	
		mf.setRmesConfigured(rmesConfigured);
		mf.setGfamConfigured(gfamConfigured);
	}
	
	/**
	 * Configure where the R'MES executables are located. 
	 * 
	 * Given the path to the "rmes" program, this method checks that it matches one of the supported versions of JRMES.
	 * It also configures "rmes.gfam". The two programs *must* be in the same directory.
	 * 
	 * @param rmesPath : the full path to the "rmes" program. 
	 */
	public void setRmesPath(String rmesPath) {
		rmesConfigured=false;
		gfamConfigured=false;
		try {
			rmesrunner.setRmesPath(rmesPath);
			rmesConfigured=true;
		}	catch (RmesNotFoundException e) {
			rmesConfigured=false;
		}
		mf.setRmesConfigured(rmesConfigured);
		if (rmesConfigured==true) {
			try {
				gfamrunner.setGfamPath(rmesPath+".gfam"); //$NON-NLS-1$
				gfamConfigured=true;
			} catch (GfamNotFoundException e) {
				gfamConfigured=false;
			}
			mf.setGfamConfigured(gfamConfigured);
		}
		
	}
	
	/**
	 * Starts the actual Thread in which "rmes" is run with the current
	 * set of parameters. 		
	 */
	public void runRmes() {
		rmesrunner.setRunParameters(rmesrunparams);
		if (rmeswatcher!=null)
			rmesrunner.setWatcher(rmeswatcher);
		Thread runThread=new Thread(rmesrunner);
		runThread.start();
	}
	
	/**
	 * Starts the actual Thread in which "rmes.gfam" is run with the current
	 * set of parameters. 		
	 */
	public void runGfam() {
		gfamrunner.setRunParameters(gfamrunparams);
		if (gfamwatcher!=null)
			gfamrunner.setWatcher(gfamwatcher);
		Thread runThread=new Thread(gfamrunner);
		runThread.start();
	}

	/**
	 * Quits JRmes when the user so decides.
	 */
	public void quit() {
		System.exit(0);
	}

	/**
	 * Returns whether rmes is correctly configured.
	 * 
	 * @return true if a supported version of the "rmes" program was found.
	 */
	public boolean isRmesConfigured() {
		return rmesConfigured;
	}
	
	/**
	 * Returns the current run parameters of "rmes".
	 * 
	 * @return the current set of run parameters as an RmesRunParameters instance.
	 */
	public RmesRunParameters getRmesRunParameters() {
		return rmesrunparams;
	}
	
	/**
	 * Defines the sequence file to use.
	 *
	 * @param seqFile : a File describing the sequence file (FASTA formatted) to be used as input. 
	 */
	public void setSequenceFile(File seqFile) {
		rmesrunparams.setSequenceFile(seqFile);
	}
	
	/**
	 * Defines the length of the smallest words for which statistics will be computed.
	 * 
	 * @param wordLength : the length (number of characters) of the smallest words to use.
	 */
	public void setMinWordLength(Integer wordLength) {
		rmesrunparams.setMinWordLength(wordLength);
	}
	
	/**
	 * Defines the length of the largest words for which statistics will be computed.
	 * 
	 * @param wordLength : the length (number of characters) of the largest words to use.
	 */
	public void setMaxWordLength(Integer wordLength) {
		rmesrunparams.setMaxWordLength(wordLength);
	}
	
	/**
	 * Returns a table of strings describing the "main functions" supported by R'MES.
	 * 
	 * @return an array of Strings with the function names.
	 */
	public String[] getMainFunctions() {
		return mainFunctions;
	}
	
	/**
	 * Returns a table of strings describing the approximation methods available in R'MES.
	 * 
	 * @return an array of Strings with the approximation method names.
	 */
	public String[] getApproximationMethods() {
		return methods;
	}
	
	/**
	 * Defines the "main function" to use.
	 * 
	 * @param mainFunction a String constant (one of the FUNCTION_ constants) defining the main function. 
	 */
	public void setMainFunction(String mainFunction) {
		rmesrunparams.setMainFunction(mainFunction);
	}
	
	/**
	 * Defines the approximation method to use.
	 * 
	 * @param method a String constant (one of the METHOD_ constants) defining the approximation method to use. 
	 */
	public void setApproximationMethod(String method) {
		rmesrunparams.setMethod(method);
	}
	
	/**
	 * Defines the family file to use.
	 *
	 * @param familyFile : a File describing the family file (generated with "rmes.gfam") to be used. 
	 */
	public void setFamilyFile(File familyFile) {
		rmesrunparams.setFamilyFile(familyFile);
	}

	/**
	 * Defines whether rmes will process words or families.
	 * 
	 * @param what a String constant (one of the USE_ constants) defining what to use.
	 */
	public void setWordOrFamily(String what) {
		if (USE_WORDS.equals(what)) {
			rmesrunparams.setUseWords(true);
		}
		if (USE_FAMILIES.equals(what)) {
			rmesrunparams.setUseFamilies(true);
		}
	}

	/**
	 * Defines the sequence file to use for comparing scores.
	 *
	 * @param seqFile : a File describing the family file (generated with "rmes.gfam") to be used. 
	 */
	public void setCompareSequenceFile(File seqFile) {
		rmesrunparams.setCompareSequenceFile(seqFile);
	}

	/**
	 * Defines the watcher object that will follow the execution of rmes.
	 * 
	 * @param watcher : an RmesWatcher instance that will be informed on the progress of subsequent rmes runs.
	 */
	public void setRmesWatcher(ProcessWatcher watcher) {
		this.rmeswatcher=watcher;
		
	}

	/**
	 * Defines the order of the Markov model.
	 * 
	 * @param m : the order of the Markov model.
	 */
	public void setMarkovOrder(int m) {
		rmesrunparams.setMarkovOrder(m);
	}

	/**
	 * Defines whether the highest possible order will be used
	 * 
	 * @param useMaxOrder : when true, rmes will use the highest possible Markov order applicable to the given word length(s).
	 */
	public void setUseMaxOrder(boolean useMaxOrder) {
		rmesrunparams.setUseMaxOrder(useMaxOrder);
	}
	
	/**
	 * Returns the size of the smallest words used by rmes.
	 * 
	 * @return an integer with the number of characters in the smallest words.
	 */
	public int getMinWordLength() {
		return rmesrunparams.getMinWordLength();
	}

	/**
	 * Returns the size of the largest words used by rmes.
	 * 
	 * @return an integer with the number of characters in the largest words.
	 */
	public int getMaxWordLength() {
		return rmesrunparams.getMaxWordLength();
	}

	/**
	 * Returns the order of the Markov model.
	 * 
	 * @return an integer with the Markov model order.
	 */
	public int getMarkovOrder() {
		return rmesrunparams.getMarkovOrder();
	}

	
	/**
	 * Defines the prefix of the output file that will be generated by "rmes".
	 *
	 * @param outFile a File instance representing the output file, to which a suffix will be appended by R'MES.
	 */
	public void setOutputFilePrefix(File outFile) {
		rmesrunparams.setResultFile(outFile);
	}

	/**
	 * Returns the output prefix.
	 * 
	 * @return a String representing the prefix of the output file.
	 */
	public String getOutputFilePrefix() {
		return rmesrunparams.getResultFile().getAbsolutePath();
	}

	/**
	 * Returns a runner (command-like object) for "rmes".
	 * 
	 * @return an instance of RmesRunner wrapping the execution details of "rmes" in an objet.
	 */
	public RmesRunner getRmesRunner() {
		return rmesrunner;
	}

	/**
	 * Defines the name of the families that will be generated by "rmes.fam".
	 * 
	 * @param fName a String with the family name (or label) that describes the families in a family file.
	 */
	public void setGfamFamilyName(String fName) {
		gfamrunparams.setFamilyName(fName);
	}
	
	/**
	 * Returns the name of the families that will be generated by "rmes.gfam".
	 * 
	 * @return a String with the family name.
	 */
	public String getGfamFamilyName() {
		return gfamrunparams.getFamilyName();
	}
	
	/**
	 * Defines the pattern that will be used by "rmes.gfam" to generate the families.
	 * 
	 * @param pattern a String with characters of the pattern.
	 */
	public void setGfamPattern(String pattern) {
		gfamrunparams.setPattern(pattern);
	}
	
	/**
	 * Returns the pattern that will be used by "rmes.gfam" to generate the families.
	 * @return a String with characters of the pattern.
	 */
	public String getGfamPattern() {
		return gfamrunparams.getPattern();
	}
	
	/**
	 * Returns the name of the output file that will be generated by "rmes.gfam".
	 * 
	 * @return a String with the output file name.
	 */
	public String getGfamOutputFile() {
		return gfamrunparams.getResultFile().getAbsolutePath();
	}
	
	/**
	 * Defines the outputfile that will be used by "rmes.gfam".
	 * 
	 * @param outfile a File object representing the output file in which families will be generated.
	 */
	public void setGfamOutputFile(File outfile) {
		gfamrunparams.setResultFile(outfile);
	}

	
	/**
	 * Defines the watcher for "rmes.gfam". 
	 * @param watcher
	 */
	public void setGfamWatcher(ProcessWatcher watcher) {
		this.gfamwatcher=watcher;
	}


	/**
	 * Comparison is only possible for R'MES 4 (and higher ?)
	 * 
	 * @return true is the currently configured version of R'MES supports comparing scores.
	 */
	public boolean isCompareEnabled() {
		boolean canCompare=false;
		if (rmesrunner.getRmesVersion().startsWith("4"))
			canCompare=true;
		return canCompare;
	}

	/**
	 * The entry point for JRmes.
	 * 
	 * The args are currently not used.
	 */
	public static void main(String[] args) {
	
		JRmes jrmes=new JRmes();
		jrmes.initialize();
		SwingUtilities.invokeLater(jrmes);
		
	}

}
