package fr.ssbgroup.jrmes.core;
/**
 * This file is part of JRmes.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) : Mark Hoebeke
 *
 * JRmes is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * JRmes is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JRmes; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 **/
import junit.framework.TestCase;
import fr.ssbgroup.jrmes.common.core.ProcessRunner;
import fr.ssbgroup.jrmes.common.core.ProcessWatcher;
import fr.ssbgroup.jrmes.rmes.core.RmesNotFoundException;
import fr.ssbgroup.jrmes.rmes.core.RmesRunner;

public class RmesRunnerTester extends TestCase {

	class BasicRmesWatcher implements ProcessWatcher {

		public void processFinished(RmesRunner launcher) {
			System.out.println("Finished");
			
		}

		public void processPercentProgressed(RmesRunner launcher,
				double percentage) {
			System.out.println("Completed :"+percentage);
			
		}

		public void processStarted(RmesRunner launcher) {
			System.out.println("Started.");
			
		}
		
		public void processFailed(RmesRunner runner, String message) {
			System.out.println("Failed: "+message);
		}

		public void processFailed(ProcessRunner launcher, String message) {
			// TODO Auto-generated method stub
			
		}

		public void processFinished(ProcessRunner launcher) {
			// TODO Auto-generated method stub
			
		}

		public void processPercentProgressed(ProcessRunner launcher,
				double percentage) {
			// TODO Auto-generated method stub
			
		}

		public void processStarted(ProcessRunner launcher) {
			// TODO Auto-generated method stub
			
		}
		
	};
	
	public void testRun() throws RmesNotFoundException {
	
		RmesRunner runner=new RmesRunner();
		
		runner.setWatcher(new BasicRmesWatcher());
		
		Thread thread=new Thread(runner);
		
		thread.start();

		while(thread.isAlive()) {
			;
		}
		
		assert(true);
		
	}

}
