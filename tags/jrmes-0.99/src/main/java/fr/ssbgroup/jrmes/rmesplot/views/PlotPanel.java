/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.views;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.SpinnerListModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fr.ssbgroup.jrmes.rmesplot.controls.RMESPlotController;

/**
 * Class implementing a JPanel which regroups the PlotArea where plot is drawing
 * and ZoomToolBar which acts on the plot (zoom function).
 * 
 * @author $Author: mhoebeke $
 * @version $Revision: 123 $
 */

public class PlotPanel extends JPanel implements ChangeListener {
    private static final int TITLE_TEXTFIELD_SIZE = 20;

    private RMESPlotController _controller;
    private PlotArea _plotArea;
    private JToggleButton _zoomInToggleButton;
    private JToggleButton _zoomOutToggleButton;
    private JTextField _titleField;
    
    PlotPanel(RMESPlotController controller)  {
        _controller = controller;
        setLayout(new BorderLayout());

     
        JPanel mainButtonPanel=new JPanel();
        add(mainButtonPanel, BorderLayout.SOUTH);
        mainButtonPanel.setLayout(new GridLayout(2,1));
        
        JPanel buttonPanel = new JPanel();
        mainButtonPanel.add(buttonPanel);
        
        ItemListener zoomButtonListener=new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                int state=PlotArea.STATE_SELECT_WORD;
                Object source=e.getSource();
                if (source==_zoomInToggleButton) {
                    if (_zoomInToggleButton.isSelected()== true) {
                      state=PlotArea.STATE_ZOOM_IN;
                      _zoomOutToggleButton.setSelected(false);
                    } else if (_zoomOutToggleButton.isSelected() == true) 
                        state=PlotArea.STATE_ZOOM_OUT;
                        
                }
                if (e.getSource() == _zoomOutToggleButton) {
                    if (_zoomOutToggleButton.isSelected() == true) {
                        state=PlotArea.STATE_ZOOM_OUT;
                        _zoomInToggleButton.setSelected(false);
                    } else if (_zoomInToggleButton.isSelected()==true)
                            state=PlotArea.STATE_ZOOM_IN;
                }
                _plotArea.setState(state);
            };
        };
        
        
        _zoomInToggleButton=new JToggleButton("+");
        _zoomInToggleButton.addItemListener(zoomButtonListener);
        buttonPanel.add(_zoomInToggleButton);
        
        String zoomValueString=(String)_controller.getProperty("plotarea.zoomvalues");
        String[] zoomValueStrings=zoomValueString.split(",");
        for (int i=0;i<zoomValueStrings.length;i++) {
            zoomValueStrings[i]+="%";
        }
        
        SpinnerListModel spinnerListModel=new SpinnerListModel(zoomValueStrings);
        spinnerListModel.setValue("100%");
        spinnerListModel.addChangeListener(this);
        JSpinner zoomSpinner=new JSpinner(spinnerListModel);
        buttonPanel.add(zoomSpinner);   
        
        _zoomOutToggleButton=new JToggleButton("-");
        _zoomOutToggleButton.addItemListener(zoomButtonListener);
        buttonPanel.add(_zoomOutToggleButton);
        
        JLabel label = new JLabel(_controller.getLabel("TITLE_LABEL"));
        buttonPanel.add(label);

        _titleField = new JTextField();
        _titleField.setColumns(TITLE_TEXTFIELD_SIZE);
        _titleField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String text = _titleField.getText();
                if (text.length() == 0)
                    text = null;
                _plotArea.setTitle(text);
            }
        });
        buttonPanel.add(_titleField);

        JButton button = new JButton(_controller.getLabel("CLEAR_XPLOT_BUTTON"));
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                _controller.clearPlotOnX();
            }

        });
        buttonPanel.add(button);

        button = new JButton(_controller.getLabel("CLEAR_YPLOT_BUTTON"));
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                _controller.clearPlotOnY();
            }

        });
        buttonPanel.add(button);
        
        
        
        button=new JButton(_controller.getLabel("PRINT_BUTTON"));
        button.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent e) {
               PrinterJob pJob=PrinterJob.getPrinterJob();
               if (pJob.printDialog()==true) {
                   try {
                       pJob.setPrintable(_plotArea);
                       pJob.print();
                } catch (PrinterException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
               }
           }
        });
        buttonPanel.add(button);
        
        buttonPanel=new JPanel();
        mainButtonPanel.add(buttonPanel);
        
        Boolean tmpBoolean=Boolean.valueOf((String)_controller.getProperty("plotpanel.plotsfiltered"));
        boolean filterPlots=tmpBoolean.booleanValue();
        JCheckBox checkBox=new JCheckBox(_controller.getLabel("FILTER_PLOTS_CHECKBOX"),filterPlots);
        checkBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
              _plotArea.setFiltersEnabled(((JCheckBox)e.getSource()).isSelected());  
            }});
        buttonPanel.add(checkBox);
        
        checkBox=new JCheckBox(_controller.getLabel("SHOW_ALL_LABELS_CHECKBOX"),false);
        checkBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
              _plotArea.setShowAllLabels(((JCheckBox)e.getSource()).isSelected());  
            }});
        buttonPanel.add(checkBox);
        button = new JButton(_controller.getLabel("CLEAR_LABELS_BUTTON"));
        button.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                _plotArea.clearHWords();
            }
        });
        buttonPanel.add(button);

        _plotArea = new PlotArea(_controller,spinnerListModel);
        
        JScrollPane scrollPane = new JScrollPane(_plotArea);
        add(scrollPane, BorderLayout.CENTER);

    }

	/* (non-Javadoc)
	 * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
	 */
	public void stateChanged(ChangeEvent e) {
		Object source=e.getSource();
		if (source instanceof SpinnerListModel) {
			String zoomValueString=(String)((SpinnerListModel)source).getValue();
			zoomValueString=zoomValueString.replaceFirst("%","");
			float zoomValue=Float.parseFloat(zoomValueString);
			zoomValue*=0.01;
			_plotArea.setZoomFactor(zoomValue);
			
		}
		// TODO Auto-generated method stub
		
	}

}