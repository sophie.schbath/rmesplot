/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.views;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import fr.ssbgroup.jrmes.rmesplot.controls.RMESPlotController;

/**
 * This class regroups the used components in the pyramid tab.
 * 
 * @author $Author : jguerin $
 * @version $Revision : 1.1 $
 */
public class PyramidPanel extends JPanel {

    private static final int TITLE_TEXTFIELD_SIZE = 20;

    private RMESPlotController _controller;

    PyramidPanel(RMESPlotController controller) {
    	super();
    	
        _controller = controller;
        setLayout(new BorderLayout());

        JScrollPane scrollPane = new JScrollPane();
       
        scrollPane
                .setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane
                .setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(scrollPane, BorderLayout.CENTER);

        final PyramidArea pyramidArea = new PyramidArea(controller);
        scrollPane.setViewportView(pyramidArea);
        controller.setPyramidArea(pyramidArea);

        JPanel bottomPanel=new JPanel();
        add(bottomPanel, BorderLayout.SOUTH);
        bottomPanel.setLayout(new BorderLayout());
        JPanel keyPanel=new PyramidKeyPanel(controller);
        bottomPanel.add(keyPanel,BorderLayout.NORTH);
        
        JPanel buttonPanel = new JPanel();
        bottomPanel.add(buttonPanel,BorderLayout.CENTER);
        
        JLabel title = new JLabel(_controller.getLabel("TITLE_LABEL"));
        buttonPanel.add(title);

        final JTextField textField = new JTextField();
        textField.setColumns(TITLE_TEXTFIELD_SIZE);
        textField.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                pyramidArea.setTitle(textField.getText());
            }});
        buttonPanel.add(textField);
        
        JButton button=new JButton(_controller.getLabel("CLEAR_PYRAMIDS_BUTTON"));
        button.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				pyramidArea.clearPyramids();
			}
        });
        buttonPanel.add(button);
        button=new JButton(_controller.getLabel("PRINT_BUTTON"));
        button.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent e) {
               PrinterJob pJob=PrinterJob.getPrinterJob();
               if (pJob.printDialog()==true) {
                   try {
                       pJob.setPrintable(pyramidArea);
                       pJob.print();
                } catch (PrinterException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
               }
           }
        });
        buttonPanel.add(button);
        
    }
}