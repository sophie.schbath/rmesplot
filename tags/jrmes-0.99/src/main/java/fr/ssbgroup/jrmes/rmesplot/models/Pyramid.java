/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.models;

import java.util.HashSet;
import java.util.Hashtable;

/**
 * @author mhoebeke
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Pyramid {
    
    private Hashtable _elements;
    private int _phase;
    private String _topWord;
    
    public Pyramid(int phase) {
        _phase=phase;
        _elements=new Hashtable();
    }
    
    public int getLayers() {
        return _elements.size();
    }
    
    public PyramidElement[] getElementsAtLayer(int i) {
        Integer layerInt=new Integer(i);
        return (PyramidElement[])_elements.get(layerInt);
    }
    
    public void setElementsAtLayer(int i, PyramidElement[] elements) {
        Integer layerInt=new Integer(i);
        _elements.put(layerInt,elements);
        if (i==0)
        	_topWord=elements[0].getWord();
    }
    
    public String getTopWord() {
    	return _topWord;
    }
    
    

}
