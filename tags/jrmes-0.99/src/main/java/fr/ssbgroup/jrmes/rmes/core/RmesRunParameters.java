/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmes.core;

import java.io.File;
import java.io.Serializable;

@SuppressWarnings("serial")
public class RmesRunParameters implements Serializable {
	 
	private String mainFunction;
	
	private String model;
	
	private File sequenceFile;

	private File familyFile;

	private int markovOrder;

	private int minWordLength;
	private int maxWordLength;
	
	private File resultFile;

	private File compareSequenceFile;

	private boolean useWords;

	private boolean useFamilies;

	private boolean useMaxOrder;
	
	public RmesRunParameters()
	{
		useWords=true;
		useMaxOrder=false;
		useFamilies=false;
	}

	public File getFamilyFile() {
		return familyFile;
	}
	
	
	public String getMainFunction() {
		return mainFunction;
	}

	public int getMarkovOrder() {
		return markovOrder;
	}

	public String getModel() {
		return model;
	}

	public File getResultFile() {
		return resultFile;
	}

	public File getSequenceFile() {
		return sequenceFile;
	}

	public int getMinWordLength() {
		return minWordLength;
	}

	public int getMaxWordLength() {
		return maxWordLength;
	}

	
	public void setFamilyFile(File familyFile) {
		this.familyFile = familyFile;
		this.useFamilies = true;
		this.useWords = false;
	}

	public void setMainFunction(String mainFunction) {
		this.mainFunction = mainFunction;
	}

	public void setMarkovOrder(int markovOrder) {
		this.markovOrder = markovOrder;
	}
	
	public void setUseMaxOrder(boolean useMaxOrder) {
		this.useMaxOrder=useMaxOrder;
	}
	
	public boolean getUseMaxOrder() {
		return this.useMaxOrder;
	}
	
	public void setMethod(String model) {
		this.model = model;
	}
	
	public void setResultFile(File resultFile) {
		this.resultFile = resultFile;
	}
	
	public void setSequenceFile(File sequenceFile) {
		this.sequenceFile = sequenceFile;
		if (resultFile==null)
			resultFile=sequenceFile;
	}
	
	public void setMinWordLength(int wordLength) {
		this.minWordLength = wordLength;
		this.useWords=true;
		this.useFamilies=false;
	}

	public void setMaxWordLength(int wordLength) {
		this.maxWordLength = wordLength;
		this.useWords=true;
		this.useFamilies=false;
	}

	public void setCompareSequenceFile(File seqFile) {
		this.compareSequenceFile=seqFile;
		
	}

	public File getCompareSequenceFile() {
		return compareSequenceFile;
	}

	public void setUseWords(boolean b) {
		this.useWords=b;
		this.useFamilies=!b;
	}
	
	public void setUseFamilies(boolean b) {
		this.useWords=!b;
		this.useFamilies=b;
	}

	public boolean getUseFamilies() {
		return useFamilies;
	}
	
	public boolean getUseWords() {
		return useWords;
	}
	

}
