/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot;

import java.io.File;

import javax.swing.SwingUtilities;

import fr.ssbgroup.jrmes.rmesplot.controls.RMESPlotController;

/**
 * Main class of the RMESPlot Package. This class instantiates the GUI of
 * RMESPlot and adds each result given as a command-line argument.
 * 
 * @author $Author: mhoebeke $
 * @version $Revision: 125 $
 */
public class RMESPlot {
    /**
     * This is the \e main method of the RMESPlot application.
     * 
     * @param args
     *            a list of strings denoting the URLS to load R'MES results
     *            from.
     */
	
	public static void runEmbedded(String[] args) {
		launch(args,false);
	}
	
	public static void main(String[] args) {
		launch(args,true);
	}
	
    private static void launch(String[] args, final boolean quitOnExit) {
        final String[] fargs=args;
        Runnable runnable=new Runnable() {
            public void run() {
                RMESPlotController controller = new RMESPlotController(quitOnExit);
                File[] files = new File[fargs.length];
                for (int i = 0; i < fargs.length; i++) {
                    files[i] = new File(fargs[i]);
                }
                controller.loadFiles(files);
            }
            };
			SwingUtilities.invokeLater(runnable);
    }
}
