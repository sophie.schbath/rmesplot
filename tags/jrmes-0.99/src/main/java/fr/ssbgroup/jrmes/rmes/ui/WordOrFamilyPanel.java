/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmes.ui;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.TitledBorder;

import fr.ssbgroup.jrmes.JRmes;
import fr.ssbgroup.jrmes.common.ui.StatefulPanel;
import fr.ssbgroup.jrmes.rmes.ui.actions.SelectWordOrFamilyAction;

@SuppressWarnings("serial")
public class WordOrFamilyPanel extends StatefulPanel {
	
	RmesRunFrame runFrame=null;
	JRmes controller=null;
	ButtonGroup wordOrFamilyBtnGroup=null;
	WordLengthPanel wordLengthPanel=null;
	FamilyFilePanel familyFilePanel=null;
	
	
	public WordOrFamilyPanel(RmesRunFrame runFrame) {
		super();
		this.runFrame=runFrame;
		this.controller=runFrame.getController();
		initWordOrFamilyPanel();
	}
	
	void initWordOrFamilyPanel() {
		
		wordOrFamilyBtnGroup=new ButtonGroup();
		
		setAlignmentX(LEFT_ALIGNMENT);
		setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
		setBorder(new TitledBorder(Messages.getString("WordOrFamilyPanel.title"))); //$NON-NLS-1$
		
		JPanel outerPanel=new JPanel();
		outerPanel.setLayout(new BoxLayout(outerPanel,BoxLayout.LINE_AXIS));
		outerPanel.setAlignmentX(LEFT_ALIGNMENT);

		JRadioButton btn=new JRadioButton();
		wordOrFamilyBtnGroup.add(btn);
		btn.setAlignmentX(LEFT_ALIGNMENT);
		outerPanel.add(btn);
		btn.setAction(new SelectWordOrFamilyAction(controller,JRmes.USE_WORDS, this));
		btn.setSelected(true);
		
		wordLengthPanel=new WordLengthPanel(runFrame);
		wordLengthPanel.setAlignmentX(LEFT_ALIGNMENT);
		outerPanel.add(wordLengthPanel);
		add(outerPanel);
		
		outerPanel=new JPanel();
		outerPanel.setLayout(new BoxLayout(outerPanel,BoxLayout.LINE_AXIS));
		outerPanel.setAlignmentX(LEFT_ALIGNMENT);
		btn=new JRadioButton();
		wordOrFamilyBtnGroup.add(btn);
		btn.setAction(new SelectWordOrFamilyAction(controller,JRmes.USE_FAMILIES, this));
		outerPanel.add(btn);
		
		familyFilePanel=new FamilyFilePanel(runFrame);
		familyFilePanel.setAlignmentX(LEFT_ALIGNMENT);
		familyFilePanel.setEnabled(false);
		outerPanel.add(familyFilePanel);
		add(outerPanel);
				
	}
	
	public void setWordOrFamily(String what) {
		if (JRmes.USE_WORDS.equals(what)) {
			wordLengthPanel.setEnabled(true);
			familyFilePanel.setEnabled(false);
			runFrame.getMainFunctionPanel().setEnablePoisson(true);
		}
		if (JRmes.USE_FAMILIES.equals(what)) {
			wordLengthPanel.setEnabled(false);
			familyFilePanel.setEnabled(true);
			runFrame.getMainFunctionPanel().setEnablePoisson(false);
		}
		
	}

}
