/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.main.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.ssbgroup.jrmes.JRmes;

/**
 * Defines the contents of the "About" dialog window.
 * 
 * @author mhoebeke
 */
@SuppressWarnings("serial")
public class AboutDialog extends JDialog {
	
	/**
	 * Constructor for the About Dialog.
	 * 
	 * All properties and resources are loaded in the constructor. Users of this class only
	 * have to call the {@link JDialog#setVisible(boolean)} method to make it appear/disappear.
	 * 
	 */
    public AboutDialog() {
        super((JFrame)null,Messages.getString("AboutDialog.title"),true);
        
        Container contentPane=getContentPane();
        contentPane.setLayout(new BorderLayout());
        
        JPanel innerPanel=new JPanel();
        contentPane.add(innerPanel,BorderLayout.CENTER);
        innerPanel.setLayout(new BorderLayout());
        
        String string="<html><h2>JR'MES V "+Messages.getString("JRmes.version")+"</h2></html>";
        JLabel label=new JLabel(string);
        innerPanel.add(label,BorderLayout.NORTH);
        
        string=Messages.getString("AboutDialog.text");
        label=new JLabel(string);
        innerPanel.add(label,BorderLayout.CENTER);
        
        JPanel lowerPanel=new JPanel();
        lowerPanel.setLayout(new BorderLayout());
        innerPanel.add(lowerPanel,BorderLayout.SOUTH); 
        
		try {
			InputStream imgStream=this.getClass().getResourceAsStream("/fr/ssbgroup/jrmes/rmesplot/logo-mig.jpg");
			int size = imgStream.available();
			byte[] imagedata=new byte[size];
			int read=0;
			int offset=0;
			while (read<size) {
				read+=imgStream.read(imagedata,offset,size-offset);
				offset=read;
			}
			imgStream.close();
			label=new JLabel();
			Icon icon=new ImageIcon(imagedata);
			label.setIcon(icon);
			lowerPanel.add(label,BorderLayout.CENTER);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
        JButton button=new JButton(Messages.getString("AboutDialog.OK"));
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                AboutDialog.this.setVisible(false);
            }
        });
        contentPane.add(button,BorderLayout.SOUTH);
        pack(); 
    }

}
