/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmes.ui.listeners;

import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fr.ssbgroup.jrmes.JRmes;

public class MarkovOrderChangeListener implements ChangeListener {

	JRmes controller=null;
	
	public MarkovOrderChangeListener(JRmes controller) {
		this.controller=controller;
		
	}
	
	public void stateChanged(ChangeEvent evt) {
		JSpinner spinner=(JSpinner)evt.getSource();
		Integer markovorder=(Integer)spinner.getValue();
		if (controller.getRmesRunParameters().getUseWords()==true) {
			int wordlength=controller.getMinWordLength();
			if (markovorder <= wordlength-2) {
				controller.setMarkovOrder(markovorder);
			} else {
				JOptionPane.showMessageDialog(null,Messages.getString("MarkovOrderChangeListener.wrongorder")+(wordlength-2)+Messages.getString("MarkovOrderChangeListener.wordlength")+wordlength,Messages.getString("MarkovOrderChangeListener.dialogtitle"),JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				spinner.setValue(wordlength-2);
			}
		}
	}
}
