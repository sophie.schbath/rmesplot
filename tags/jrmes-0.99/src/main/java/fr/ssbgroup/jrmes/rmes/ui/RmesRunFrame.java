/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmes.ui;

import java.util.HashMap;

import javax.swing.BoxLayout;
import javax.swing.JFrame;

import fr.ssbgroup.jrmes.JRmes;
import fr.ssbgroup.jrmes.common.ui.StatefulPanel;

@SuppressWarnings("serial")
public class RmesRunFrame extends JFrame {

	JRmes controller;
	SequenceFilePanel sfPanel=null;
	MainFunctionPanel mfPanel=null;
	WordOrFamilyPanel wfPanel=null;
	MarkovOrderPanel moPanel=null;
	OutputFilePanel ofPanel=null;
	ExecutionButtonPanel execPanel=null; 
	
	HashMap<StatefulPanel,Boolean> panelStates=new HashMap<StatefulPanel, Boolean>();
	
	public RmesRunFrame(JRmes controller) {
		super();
		this.controller=controller;
		initUi(controller);
	}
	
	protected void initUi(JRmes controller) {	
		setDefaultLookAndFeelDecorated(true);
		setTitle(Messages.getString("RmesRunFrame.title")); //$NON-NLS-1$
			
		java.awt.Container cPane=this.getContentPane();
		
		cPane.setLayout(new BoxLayout(cPane,BoxLayout.PAGE_AXIS));
		
		sfPanel=new SequenceFilePanel(this);
		sfPanel.setAlignmentX(LEFT_ALIGNMENT);
		setPanelValidState(sfPanel,sfPanel.isStateValid());
		cPane.add(sfPanel);
		
		mfPanel=new MainFunctionPanel(this);
		mfPanel.setAlignmentX(LEFT_ALIGNMENT);
		setPanelValidState(mfPanel,mfPanel.isStateValid());
		cPane.add(mfPanel);
		
		wfPanel=new WordOrFamilyPanel(this);
		wfPanel.setAlignmentX(LEFT_ALIGNMENT);
		setPanelValidState(wfPanel,wfPanel.isStateValid());
		cPane.add(wfPanel);
		
		moPanel=new MarkovOrderPanel(this);
		moPanel.setAlignmentX(LEFT_ALIGNMENT);
		setPanelValidState(moPanel,moPanel.isStateValid());
		cPane.add(moPanel);
		
		ofPanel=new OutputFilePanel(this);
		ofPanel.setAlignmentX(LEFT_ALIGNMENT);
		setPanelValidState(ofPanel,ofPanel.isStateValid());
		cPane.add(ofPanel);

		execPanel=new ExecutionButtonPanel(this);
		execPanel.setAlignmentX(LEFT_ALIGNMENT);
		cPane.add(execPanel);
		
		pack();
		
	}
	
	public JRmes getController()
	{
		return controller;
	}
	
	public SequenceFilePanel getSequenceFilePanel() {
		return sfPanel;
	}
	
	public MainFunctionPanel getMainFunctionPanel() {
		return mfPanel;
	}
	
	public WordOrFamilyPanel getWordOrFamilyPanel() {
		return wfPanel;
	}
	
	public MarkovOrderPanel getMarkovOrderPanel() {
		return moPanel;
	}
	
	public OutputFilePanel getOutputFilePanel() {
		return ofPanel;
	}
	
	public ExecutionButtonPanel getExecutionButtonPanel() {
		return execPanel;
	}
	
	public void setPanelValidState(StatefulPanel panel,boolean valid) {
		panelStates.put(panel,valid);
		if (execPanel != null) {
			if (valid==false) {
				execPanel.setEnabled(false);
			} else {
				boolean allValid=true;
				for (StatefulPanel sfpanel : panelStates.keySet()) {
					boolean panelValid=panelStates.get(sfPanel);
					allValid=allValid & panelValid;
				}
				if (allValid==true)
					execPanel.setEnabled(true);
			}
		}
	}
	
	
}
