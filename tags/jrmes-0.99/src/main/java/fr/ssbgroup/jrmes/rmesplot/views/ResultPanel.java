/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.views;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;

import javax.swing.JPanel;
import javax.swing.RepaintManager;

/**
 * Class implementing the JPanel containing the 3 components of the user
 * interface : - the tree view implemented by ResultTree, - the table view
 * implemented by ResultTablePane, - the plotting area implemented by PlotArea.
 * 
 * @author $Author: mhoebeke $
 * @version $Revision: 123 $
 */
public class ResultPanel extends JPanel implements Printable {
    ResultTree _tree;

    DataTableTabbedPane _tablePane;

    PlotArea _plotPanel;

    PyramidArea _pyramidPanel;

    GraphicsTabbedPane _tabbedPane;

    ResultPane _rPane;

    /**
     * Creates a new ResultPanel object.
     */
    public ResultPanel() {
        super();

    }

    /**
     * Method defined in the Printable interface to generate a printout of the
     * plotting area.
     * 
     * @param g
     *            the Graphics context on which to draw the plots.
     * @param pageFormat
     *            a description of the characteristics of the page on which to
     *            print the plot.
     * @param pageIndex
     *            the index of the page to be printed.
     * 
     * @return PAGE_EXISTS if the requested pageIndex == 0, and NO_SUCH_PAGE
     *         otherwise.
     */
    public int print(Graphics g, PageFormat pageFormat, int pageIndex) {
        int res = NO_SUCH_PAGE;
        if (pageIndex == 0) {
            res = PAGE_EXISTS;
            int owidth = _plotPanel.getWidth();
            int oheight = _plotPanel.getHeight();
            float ratio = (float) owidth / (float) oheight;
            int pwidth = (int) pageFormat.getImageableWidth();
            int pheight = (int) ((float) pwidth / ratio);
            if ((float) pheight > pageFormat.getImageableHeight()) {
                pheight = (int) pageFormat.getImageableHeight();
                pwidth = (int) (pheight * ratio);
            }
            Color ofg = _plotPanel.getForeground();
            Color obg = _plotPanel.getBackground();
            Graphics2D g2d = (Graphics2D) g;
            g2d.translate(pageFormat.getImageableX(), pageFormat
                    .getImageableY());
            RepaintManager currentManager = RepaintManager
                    .currentManager(_plotPanel);
            currentManager.setDoubleBufferingEnabled(false);
            _plotPanel.setForeground(Color.black);
            _plotPanel.setBackground(Color.white);
            _plotPanel.setSize(pwidth, pheight);
            _plotPanel.paint(g2d);
            currentManager.setDoubleBufferingEnabled(true);
            _plotPanel.setForeground(ofg);
            _plotPanel.setBackground(obg);
            _plotPanel.setSize(owidth, oheight);
        }
        return res;
    }
}