/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.views.filters;

import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import fr.ssbgroup.jrmes.rmesplot.controls.RMESPlotController;

/**
 * @author mhoebeke
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SelectFilterButtonPanel extends JPanel {
    
    RMESPlotController _controller = null;
    JRadioButton _selectedButton = null;
    JRadioButton _unSelectedButton = null;
    
    public SelectFilterButtonPanel(RMESPlotController controller) {
        super();
        _controller=controller;
        
        setLayout(new GridLayout(1,2));
        
        ButtonGroup group=new ButtonGroup();
        
        _selectedButton=new JRadioButton(_controller.getLabel("SELECTFILTERBUTTONGROUP_ACTIVE"),false);
        add(_selectedButton);
        group.add(_selectedButton);
        
        _unSelectedButton=new JRadioButton(_controller.getLabel("SELECTFILTERBUTTONGROUP_INACTIVE"),true);
        add(_unSelectedButton);
        group.add(_unSelectedButton);
    }

    public boolean isActive() {
        return _selectedButton.isSelected();
    }
}
