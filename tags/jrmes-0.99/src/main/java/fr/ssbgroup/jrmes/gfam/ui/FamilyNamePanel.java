/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.gfam.ui;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import fr.ssbgroup.jrmes.JRmes;
import fr.ssbgroup.jrmes.gfam.ui.listeners.FamilyNameChangeListener;

@SuppressWarnings("serial")
public class FamilyNamePanel extends JPanel {

	final int TEXTFIELD_CHARS=40;

	JRmes controller;
	
	JLabel familynamelabel=null;
	JTextField familynamefield=null;
	
	public FamilyNamePanel(JRmes controller) {
		super();
		this.controller=controller;
		initPanel(controller);
	}
	
	protected void initPanel(JRmes controller) {
		
		setLayout(new BoxLayout(this,BoxLayout.LINE_AXIS));
		setBorder(new TitledBorder(Messages.getString("FamilyNamePanel.familysetlabel"))); //$NON-NLS-1$
		setAlignmentX(LEFT_ALIGNMENT);
		
		familynamelabel=new JLabel();
		familynamelabel.setText(Messages.getString("FamilyNamePanel.label")); //$NON-NLS-1$
		add(familynamelabel);

		familynamefield=new JTextField(TEXTFIELD_CHARS);
		familynamefield.setText(controller.getGfamFamilyName());
		add(familynamefield);
		
		FamilyNameChangeListener fcl=new FamilyNameChangeListener(controller);
		familynamefield.addActionListener(fcl);
		familynamefield.addFocusListener(fcl);

	}

}
