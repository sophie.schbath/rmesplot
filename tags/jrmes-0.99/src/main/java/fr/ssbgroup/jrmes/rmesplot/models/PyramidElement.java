/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.models;

/**
 * This class creates an object which lists all data of a little box in a
 * pyramid, this element is called PyramidElement.
 * 
 * @author $Author: mhoebeke $
 * @version $Revision: 123 $
 */
public class PyramidElement {
    
    String _word;
    float _value;
    float _minValue;
    float _maxValue;

    PyramidElement(String word, float value, float minValue, float maxValue) {
        _word = word;
        _value = value;
        _maxValue = maxValue;
        _minValue = minValue;
    }

    /**
     * Method getting the word representing the PyramidElement.
     * 
     * @return String giving the word.
     */
    public String getWord() {
        return _word;
    }

    /**
     * Method getting the stat of the word.
     * 
     * @return float the stat of the word
     */
    public float getValue() {
        return _value;
    }

    /**
     * Method getting the stat max. for a giving word length.
     * 
     * @return float the stat max.
     */
    public float getMaxValue() {
        return _maxValue;
    }

    /**
     * Method getting the stat min. for a giving word length.
     * 
     * @return float the stat min.
     */
    public float getMinValue() {
        return _minValue;
    }

}