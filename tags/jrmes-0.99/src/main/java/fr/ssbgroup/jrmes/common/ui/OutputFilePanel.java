/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.common.ui;

import java.io.File;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import fr.ssbgroup.jrmes.common.ui.actions.ChooseOutputFileAction;
import fr.ssbgroup.jrmes.common.ui.listeners.OutputFileTextFieldChangeListener;

/**
 * InputFilePanel is a panel where a (file dialog opening) button and a text field allow the user to
 * specifiy an output file.
 * 
 * When the user chooses a file in the file dialog, the contents of the text field are updated. The panel is
 * considered valid if the currently defined file is writable.
 * 
 * @author mhoebeke
 *
 */
@SuppressWarnings("serial")
public class OutputFilePanel extends StatefulPanel {
	
	final int TEXTFIELD_CHARS=40;
	
	File outputFile=null;
	
	JButton fileBtn=null;
	JTextField fileField=null;
	
	/**
	 * Constructor using a label and a flag.
	 * 
	 * Builds an instance of InputFilePanel. The label is displayed on the button (and the panel border if the border flag is true).
	 * 
	 * @param label the String to be displayed on the panel button (and possibly the panel border)
	 * @param border if true, the panel will be surrounded by a (titled) border, otherwise not.
	 * @param defaultFile a File whose name will be displayed in the text field (can be null).
	 */
	public OutputFilePanel(String label, boolean border, File defaultFile) {
		super();
		initPanel(label,border,defaultFile);
	}
	
	protected void initPanel(String title,boolean border, File defaultFile) {		
		setLayout(new BoxLayout(this,BoxLayout.LINE_AXIS));
		if (border==true) 
			setBorder(new TitledBorder(title)); //$NON-NLS-1$
		fileBtn=new JButton();
		add(fileBtn);
		
		fileField=new JTextField(TEXTFIELD_CHARS);
		fileField.addActionListener(new OutputFileTextFieldChangeListener(this));
		add(fileField);
		
		if (defaultFile != null) {
				outputFile=defaultFile;
				try {
					fileField.setText(defaultFile.getCanonicalPath());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
		}
		
		fileBtn.setAction(new ChooseOutputFileAction(this,title));
	}
	
	
	/**
	 * Enables/disables the button and the text field.
	 * 
	 * @param enabled if true, enables the button and the text field, otherwise disables them.
	 */
	@Override
	public void setEnabled(boolean enabled) {
		fileBtn.setEnabled(enabled);
		fileField.setEnabled(enabled);
	}
	
	/**
	 * Returns true if the current output file is writable.
	 * 
	 * @return true if the output file is not null and is can be written.
	 */
	@Override
	public boolean isStateValid() {
		boolean valid=false;
		if (outputFile != null && outputFile.canWrite()) {
			valid=true;
		}
		return valid;
	}

	/**
	 * Defines the output file.
	 * 
	 * @param file the File representing either the file selected by the user in the file dialog, or entered directly
	 * in the text field.
	 */
	public void setOutputFile(File file) {
		outputFile=file;
		try {
			fileField.setText(file.getCanonicalPath());
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
	}

	
	
}
