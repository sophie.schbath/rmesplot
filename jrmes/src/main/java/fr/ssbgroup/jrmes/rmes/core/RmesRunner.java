/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmes.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Vector;

import org.apache.log4j.Logger;

import fr.ssbgroup.jrmes.JRmes;
import fr.ssbgroup.jrmes.common.core.ProcessRunner;
import fr.ssbgroup.jrmes.common.core.ProcessWatcher;

public class RmesRunner implements ProcessRunner {

	private static final Logger logger=Logger.getLogger("fr.ssbgroup.jrmes"); //$NON-NLS-1$

	private RmesRunParameters params;
	private String rmesPath;
	private String rmesVersion;

	Process rmesProcess;
	ProcessWatcher watcher;
	InputStream es;
	InputStream is;
	OutputStream os;

	public RmesRunner() {
		this.rmesPath=null;
		this.rmesVersion=null;
		this.params=null;
		this.rmesProcess=null;
		
	}

	public void configureRmesPath()  throws RmesNotFoundException {

		String pathString=System.getenv("PATH"); //$NON-NLS-1$
		String pathSeparator=System.getProperty("path.separator"); //$NON-NLS-1$
		String fileSeparator=System.getProperty("file.separator"); //$NON-NLS-1$
		String[] pathDirectories=pathString.split(pathSeparator);
		for (String directory : pathDirectories ){
			String rmesPathCandidate=directory+fileSeparator+"rmes"; //$NON-NLS-1$
			File rmesFile=new File(rmesPathCandidate);
			if (rmesFile.canRead()) {
				rmesPath=rmesFile.getAbsolutePath();
			} else {
				rmesPathCandidate=rmesPathCandidate+".exe"; //$NON-NLS-1$
				rmesFile=new File(rmesPathCandidate);
				if (rmesFile.canRead()) {
					rmesPath=rmesFile.getAbsolutePath();
				}
			}
		}
		if (rmesPath != null) {
			logger.info(Messages.getString("RmesRunner.rmesfound")+rmesPath); //$NON-NLS-1$
			findRmesVersion();			
		} else
			throw new RmesNotFoundException();

	}

	private void findRmesVersion()  throws RmesNotFoundException {
		String[] cmdArray={rmesPath,"--version"}; //$NON-NLS-1$
		rmesVersion=null;
		try {
			rmesProcess=Runtime.getRuntime().exec(cmdArray);
			InputStream is=rmesProcess.getInputStream();
			if (rmesProcess.waitFor()==0) {
				BufferedReader br=new BufferedReader(new InputStreamReader(is));
				while(br.ready() && rmesVersion==null) {
					String line=br.readLine();
					if (line.contains("version:")) { //$NON-NLS-1$
						String[] fields=line.split(":"); //$NON-NLS-1$
						rmesVersion=fields[fields.length-1].trim();
						logger.info(Messages.getString("RmesRunner.usingrmes")+rmesVersion); //$NON-NLS-1$
					}
				}

			} else 
				rmesPath=null;
		} catch (IOException e) {
			rmesPath=null;
		} catch (InterruptedException e) {
			rmesPath=null;
		}
		if (rmesPath==null || rmesVersion==null) {
			logger.error(Messages.getString("RmesRunner.invalidversion")); //$NON-NLS-1$
			throw new RmesNotFoundException();
		}
	}

	public String getRmesPath() {
		return rmesPath;
	}

	public String getRmesVersion() {
		return rmesVersion;
	}

	public void setRmesPath(String rmesPath)  throws RmesNotFoundException {
		this.rmesPath = rmesPath;
		findRmesVersion();
	}

	public void setRunParameters(RmesRunParameters params) {
		this.params=params;
	}

	public void setWatcher(ProcessWatcher watcher) {
		this.watcher=watcher;
	}

	public void run() {

		Vector<String> cmdarray=new Vector<String>();
		cmdarray.add(rmesPath);

		/**
		 *  On Windows platforms, «long» filenames seem to confuse the command interpreter
		 *  when launching the R'MES process. Hence they have to be quoted. Unfortunately, 
		 *  the quotes do not work on Linux platforms. Not tested on Mac OS X yet.
		 */
		String filenamequoter=""; //$NON-NLS-1$
		if (System.getProperty("os.name").startsWith("Windows")) //$NON-NLS-1$ //$NON-NLS-2$
			filenamequoter="\""; //$NON-NLS-1$
		
		try {

			if (params.getMainFunction().equals(JRmes.FUNCTION_STATS)) {
				String model=params.getModel();
				if (model.equals(JRmes.METHOD_GAUSS))
					cmdarray.add("--gauss"); //$NON-NLS-1$
				if (model.equals(JRmes.METHOD_COMPOUNDPOISSON))
					cmdarray.add("--compoundpoisson"); //$NON-NLS-1$
				if (model.equals(JRmes.METHOD_POISSON))
					cmdarray.add("--poisson"); //$NON-NLS-1$
			}

			if (params.getMainFunction().equals(JRmes.FUNCTION_COMPARE)) {
				cmdarray.add("--compare"); //$NON-NLS-1$
				cmdarray.add("--seq2"); //$NON-NLS-1$
				cmdarray.add(filenamequoter+params.getCompareSequenceFile().getCanonicalPath()+filenamequoter);
			}

			if (params.getMainFunction().equals(JRmes.FUNCTION_SKEW))
				cmdarray.add("--skew"); //$NON-NLS-1$


			cmdarray.add("-s"); //$NON-NLS-1$
			cmdarray.add(filenamequoter+params.getSequenceFile().getCanonicalPath()+filenamequoter);

			if (params.getUseWords()==true && rmesVersion.startsWith("4.0")) { //$NON-NLS-1$
				cmdarray.add("-l"); //$NON-NLS-1$
				int minwl=params.getMinWordLength();
				int maxwl=params.getMaxWordLength();

				String lengthArg=Integer.toString(minwl);
				if (maxwl>minwl)
					lengthArg=lengthArg+"-"+Integer.toString(maxwl); //$NON-NLS-1$

				cmdarray.add(lengthArg);
			}

			if (params.getUseWords()==true && rmesVersion.startsWith("3.")) { //$NON-NLS-1$
				int minwl=params.getMinWordLength();
				int maxwl=params.getMaxWordLength();

				if (maxwl>minwl) {
					cmdarray.add("-i "+minwl); //$NON-NLS-1$
					cmdarray.add("-a "+maxwl); //$NON-NLS-1$
				}
				else 
					cmdarray.add("-l "+minwl); //$NON-NLS-1$
			}
			
			if (params.getUseFamilies()==true) {
				cmdarray.add("-f"); //$NON-NLS-1$
				cmdarray.add(filenamequoter+params.getFamilyFile().getCanonicalPath()+filenamequoter);
			}

			cmdarray.add("-o"); //$NON-NLS-1$
			cmdarray.add(filenamequoter+params.getResultFile().getCanonicalPath()+filenamequoter);

			if (params.getUseMaxOrder()==false) {
				cmdarray.add("-m"); //$NON-NLS-1$
				cmdarray.add(Integer.toString(params.getMarkovOrder()));
			} else {
				cmdarray.add("--max"); //$NON-NLS-1$
			}

			if (rmesVersion.startsWith("4.0")) { //$NON-NLS-1$
				cmdarray.add("--progress"); //$NON-NLS-1$
				cmdarray.add("dots"); //$NON-NLS-1$
			}
			
			String[] cmds=new String[cmdarray.size()];
			cmds=cmdarray.toArray(cmds);

			String cmdLine=""; //$NON-NLS-1$
			for (String arg : cmdarray)
				cmdLine=cmdLine+" "+arg; //$NON-NLS-1$
			logger.info(Messages.getString("RmesRunner.cmdline")+cmdLine); //$NON-NLS-1$

			rmesProcess=Runtime.getRuntime().exec(cmds);
			is=rmesProcess.getInputStream();
			os=rmesProcess.getOutputStream();
			es=rmesProcess.getErrorStream();	
			if (watcher != null)
				watcher.processStarted(this);
			boolean completed=false;
			byte[] progressbuffer=new byte[255];
			int steps=0;
			StringBuffer errorMessage=new StringBuffer();
			StringBuffer outMessage=new StringBuffer();
			int exitValue=0;
			do {
				if (is.available()>0 ) {
					steps+=is.read(progressbuffer,0,progressbuffer.length);
					if (watcher != null)
						watcher.processPercentProgressed(this,1.0*steps);
					if (steps>=100)
						completed=true;
				}
				if (is.available()>0 && completed==true) {
					BufferedReader br=new BufferedReader(new InputStreamReader(is));
					outMessage.append(br.readLine()+"\n"); //$NON-NLS-1$
					while (br.ready()) {
						outMessage.append(br.readLine()+"\n"); //$NON-NLS-1$
					}
					logger.info(outMessage.toString());
				}
				if (es.available()>0) {
					BufferedReader br=new BufferedReader(new InputStreamReader(es));
					errorMessage.append(br.readLine()+"\n"); //$NON-NLS-1$
					while (br.ready()) {
						errorMessage.append(br.readLine()+"\n"); //$NON-NLS-1$
					}
					logger.warn(errorMessage.toString());
				}
				try {
					if (completed==false) {
						exitValue=rmesProcess.exitValue();
						completed=true;
					}

				} catch (IllegalThreadStateException ex) {
					;//ex.printStackTrace();
				}
			} while (completed==false);
			if (watcher != null) {
				if (exitValue==0)
					watcher.processFinished(this);
				else 
					watcher.processFailed(this,Messages.getString("RmesRunner.exitvalue")+exitValue); //$NON-NLS-1$
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
