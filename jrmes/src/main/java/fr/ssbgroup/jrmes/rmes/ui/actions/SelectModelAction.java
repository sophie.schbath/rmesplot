/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmes.ui.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JComboBox;

import fr.ssbgroup.jrmes.JRmes;

@SuppressWarnings("serial")
public class SelectModelAction extends AbstractAction {

	JRmes controller=null;
	
	public SelectModelAction(JRmes controller) {
		super();
		this.controller=controller;
	}
	
	public void actionPerformed(ActionEvent evt) {
		JComboBox modelBox=(JComboBox)evt.getSource();
		String modelName=(String)modelBox.getSelectedItem();
		if (JRmes.METHOD_POISSON.equals(modelName)) {
			
		}
		controller.setApproximationMethod((String)modelBox.getSelectedItem());
	}

}
