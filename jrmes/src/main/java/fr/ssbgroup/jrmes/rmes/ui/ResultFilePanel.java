/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmes.ui;

import java.io.File;

import fr.ssbgroup.jrmes.JRmes;
import fr.ssbgroup.jrmes.common.ui.OutputFilePanel;

@SuppressWarnings("serial")
public class ResultFilePanel extends OutputFilePanel {
	
	JRmes controller=null;
	
	public ResultFilePanel(JRmes controller) {
		super(Messages.getString("OutputFilePanel.ouputprefix"),true,new File(controller.getOutputFilePrefix()));
		this.controller=controller;
	}

	
	@Override
	public void setOutputFile(File file) {
		super.setOutputFile(file);
		if (isStateValid() == true) {
			controller.setOutputFilePrefix(file);
		}
	}
	
}
