/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmes.ui;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;

import fr.ssbgroup.jrmes.JRmes;
import fr.ssbgroup.jrmes.common.ui.StatefulPanel;
import fr.ssbgroup.jrmes.rmes.ui.actions.SelectNumOrMaxorderAction;
import fr.ssbgroup.jrmes.rmes.ui.listeners.MarkovOrderChangeListener;

@SuppressWarnings("serial")
public class MarkovOrderPanel extends StatefulPanel {

	final static int DEFAULT_MARKOV_ORDER=0;
	
	RmesRunFrame runFrame=null;
	JRmes controller=null;
	
	JLabel numOrderlabel=null;
	JSpinner lSpinner=null;
	
	JLabel maxOrderLabel=null;
	ButtonGroup numOrMaxGroup=null;
	
	public MarkovOrderPanel(RmesRunFrame runFrame) {
		super();
		this.runFrame=runFrame;
		this.controller=runFrame.getController();
		initPanel(controller);
	}
	
	protected void initPanel(JRmes controller) {
		
		setLayout(new BoxLayout(this,BoxLayout.LINE_AXIS));
		setBorder(new TitledBorder(Messages.getString("MarkovOrderPanel.markovorder"))); //$NON-NLS-1$
		setAlignmentX(LEFT_ALIGNMENT);
		
		numOrMaxGroup=new ButtonGroup();
		
		JRadioButton btn=new JRadioButton();
		numOrMaxGroup.add(btn);
		btn.setSelected(true);
		btn.addActionListener(new SelectNumOrMaxorderAction(controller,false,this));
		add(btn);
		
		numOrderlabel=new JLabel();
		numOrderlabel.setText(Messages.getString("MarkovOrderPanel.fixedorder")); //$NON-NLS-1$
		add(numOrderlabel);
		
		SpinnerModel lModel=new SpinnerNumberModel(0,0,100,1);
		controller.setMarkovOrder(DEFAULT_MARKOV_ORDER);
		lSpinner=new JSpinner(lModel);
		add(lSpinner);
		lSpinner.addChangeListener(new MarkovOrderChangeListener(controller));

		btn=new JRadioButton();
		numOrMaxGroup.add(btn);
		btn.addActionListener(new SelectNumOrMaxorderAction(controller,true,this));
		add(btn);
		
		maxOrderLabel=new JLabel();
		maxOrderLabel.setText(Messages.getString("MarkovOrderPanel.maxorder")); //$NON-NLS-1$
		maxOrderLabel.setEnabled(false);
		add(maxOrderLabel);
			
	}

	public void setMaxOrder(boolean maxOrder) {
		numOrderlabel.setEnabled(!maxOrder);
		lSpinner.setEnabled(!maxOrder);
		maxOrderLabel.setEnabled(maxOrder);
	}

}
