/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.util;
import java.awt.Color;
import java.util.Vector;

import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;

import org.apache.log4j.Level;
import org.apache.log4j.WriterAppender;
import org.apache.log4j.spi.LoggingEvent;

/**
 * Class handling the display of logging events in the main window of JRmes.
 * 
 * An instance of this class is automagically created by Log4j, as specified in the log4j.xml file
 * when the first event needs to be handled. 
 */
public class TextAreaAppender extends WriterAppender {
		

	class EventBufferFlusher implements Runnable {

		public void appendEvent(LoggingEvent event) {
			Document doc=jTextPane.getDocument();
			Style style=jTextPane.getStyle("normal");

			if (event.getLevel() == Level.ERROR)
				style=jTextPane.getStyle("error");
			else 
				if (event.getLevel() == Level.WARN)
					style=jTextPane.getStyle("warn");
			try {
				doc.insertString(doc.getLength(),event.getRenderedMessage()+"\n", style);
			} catch (BadLocationException e) {
				e.printStackTrace();
			}

		}

		public void run() {
				for (LoggingEvent evt : eventBuffer) {
					appendEvent(evt);
				}
				eventBuffer.clear();
		}	
	}

	
	JTextPane jTextPane = null;
	
	final Vector<LoggingEvent> eventBuffer=new Vector<LoggingEvent>();	
	final EventBufferFlusher flusher=new EventBufferFlusher();
		
	/**
	 * Associates à {@link JTextPane} to this appender. Upon association, the current event buffer
	 * will be flushed.
	 * 
	 * @param jTextPane the {@link JTextPane} to which logging events will be appended.
	 */
	public void setTextPane(JTextPane jTextPane) {
		this.jTextPane = jTextPane;	
		Style style=jTextPane.addStyle("normal",null);
		StyleConstants.setForeground(style,Color.black);
		style=jTextPane.addStyle("error",null);
		StyleConstants.setForeground(style,Color.red);
		style=jTextPane.addStyle("warn",null);
		StyleConstants.setForeground(style,Color.orange);		
		SwingUtilities.invokeLater(flusher);
		
	}
	
	
	/**
	 * Adds a logging event to the buffer of events to be displayed in the text pane.
	 * If a {@link JTextPane} is active for the current instance, the buffer is flushed.
	 * 
	 * @param loggingEvent the event whose message has to be added to the text pane.
	 */
	public void append(LoggingEvent loggingEvent) {
		synchronized(eventBuffer) {
			eventBuffer.add(loggingEvent);
		}
		if (jTextPane!=null)
			SwingUtilities.invokeLater(flusher);
	}


	/**
	 * Clears the contents of the message text area. 
	 */
	public void clearMessages() {
		if (jTextPane != null) {
			jTextPane.setText("");
		}
	}

}
