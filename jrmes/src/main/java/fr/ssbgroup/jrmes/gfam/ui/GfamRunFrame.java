/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.gfam.ui;

import javax.swing.BoxLayout;
import javax.swing.JFrame;

import fr.ssbgroup.jrmes.JRmes;

@SuppressWarnings("serial")
public class GfamRunFrame extends JFrame {
		
	JRmes controller;
	
	public GfamRunFrame(JRmes controller) {
		super();
		this.controller=controller;
		initUi(controller);
	}
	
	protected void initUi(JRmes controller) {
		
		setDefaultLookAndFeelDecorated(true);
		setTitle(Messages.getString("GfamRunFrame.title")); //$NON-NLS-1$
			
		java.awt.Container cPane=this.getContentPane();
		
		cPane.setLayout(new BoxLayout(cPane,BoxLayout.PAGE_AXIS));
		
		FamilyNamePanel fnPanel=new FamilyNamePanel(controller);
		fnPanel.setAlignmentX(LEFT_ALIGNMENT);
		cPane.add(fnPanel);
		
		PatternPanel pattPanel=new PatternPanel(controller);
		pattPanel.setAlignmentX(LEFT_ALIGNMENT);
		cPane.add(pattPanel);
		
		
		ResultFilePanel ofPanel=new ResultFilePanel(controller);
		ofPanel.setAlignmentX(LEFT_ALIGNMENT);
		cPane.add(ofPanel);
		
		ExecutionButtonPanel execPanel=new ExecutionButtonPanel(controller);
		execPanel.setAlignmentX(LEFT_ALIGNMENT);
		cPane.add(execPanel);
		
		pack();
	
	}
}
