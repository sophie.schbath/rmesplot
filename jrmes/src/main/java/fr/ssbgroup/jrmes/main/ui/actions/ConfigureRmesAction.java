/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.main.ui.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;

import fr.ssbgroup.jrmes.JRmes;
import fr.ssbgroup.jrmes.rmes.ui.RmesPathDialog;

/**
 * Class allowing the user to explicitely define the directory where
 * R'MES executables (rmes, rmes.format, rmes.gfam) will be used. 
 **/
@SuppressWarnings("serial")
public class ConfigureRmesAction extends AbstractAction {

	JRmes controller=null;
	
	/**
	 * @param controller the main JRmes controller class.
	 */
	public ConfigureRmesAction(JRmes controller) {
		super(Messages.getString("ConfigureRmesAction.configuredir")); //$NON-NLS-1$
		this.controller=controller;
	}
	
	/**
	 *  to the opening of the file dialog.
	 */
	public void actionPerformed(ActionEvent event) {
		RmesPathDialog rp=new RmesPathDialog();
		if (rp.showDialog(null,Messages.getString("ConfigureRmesAction.selectrmes")) == JFileChooser.APPROVE_OPTION) { //$NON-NLS-1$
			java.io.File rmesFile=rp.getSelectedFile();
			controller.setRmesPath(rmesFile.getAbsolutePath());
		}
	}
}
