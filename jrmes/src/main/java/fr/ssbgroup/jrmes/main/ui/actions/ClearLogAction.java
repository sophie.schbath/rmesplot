package fr.ssbgroup.jrmes.main.ui.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import fr.ssbgroup.jrmes.util.TextAreaAppender;

/**
 * ClearLogAction takes care of cleaning the log text area when the user hits the "Clear Messages" button.
 * 
 * @author mhoebeke
 *
 */
@SuppressWarnings("serial")
public class ClearLogAction extends AbstractAction {
	
	TextAreaAppender appender=null;
	
	/**
	 * Class constructor taking a {@link TextAreaAppender} as argument.
	 * 
	 * @param appender the {@link TextAreaAppender} who will be cleared on every call of {@link #actionPerformed(ActionEvent)}
	 */
	public ClearLogAction(TextAreaAppender appender) {
		super(Messages.getString("MainFrame.clearlog"));
		this.appender=appender;
	}

	/**
	 * Performs the actual clear operation on the {@link TextAreaAppender}.
	 */
	public void actionPerformed(ActionEvent evt) {
		appender.clearMessages();
	}

}
