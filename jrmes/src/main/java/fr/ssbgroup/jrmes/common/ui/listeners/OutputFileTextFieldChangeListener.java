/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.common.ui.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import fr.ssbgroup.jrmes.common.ui.OutputFilePanel;
import fr.ssbgroup.jrmes.rmes.ui.listeners.Messages;

public class OutputFileTextFieldChangeListener implements ActionListener {
	
	OutputFilePanel panel;
	
	public OutputFileTextFieldChangeListener(OutputFilePanel panel) {
		this.panel=panel;
		
	}

	public void actionPerformed(ActionEvent evt) {
		JTextField tField=(JTextField)evt.getSource();
		String contents=tField.getText();
		File file=new File(contents);
		panel.setOutputFile(file);
	}

}
