/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.readers;

import java.io.IOException;
import java.net.URL;
import java.util.Hashtable;

import fr.ssbgroup.jrmes.rmesplot.controls.RMESPlotController;
import fr.ssbgroup.jrmes.rmesplot.models.Results;
import fr.ssbgroup.jrmes.rmesplot.models.UnknownAlgorithmException;

public class ResultReader {

	protected static Hashtable<String,String> _algorithms = null;

	static {
		/**
		 * R'MES 3 identifiers.
		 */
		_algorithms = new Hashtable<String,String>();
		_algorithms.put("Cond_as","Gauss");
		_algorithms.put("Mart","Gauss (opt)");
		_algorithms.put("Mart_r","Reversed Martingale");
		_algorithms.put("Cond_as_p","Poisson");
		_algorithms.put("Cond_as_pc","Compound Poisson");
		_algorithms.put("Exact","Exact");
		
		/**
		 * R'MES 4 identifiers.
		 */
		_algorithms.put("Gauss","Gauss");
		_algorithms.put("Compound Poisson","Compound Poisson");
		_algorithms.put("Poisson","Poisson");
	}


	public Results readResults(RMESPlotController controller, URL resultURL) throws IOException, UnknownAlgorithmException
	{
		return null;
	}

}
