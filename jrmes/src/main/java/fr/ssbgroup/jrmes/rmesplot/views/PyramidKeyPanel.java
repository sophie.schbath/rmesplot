/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.views;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

import fr.ssbgroup.jrmes.rmesplot.controls.RMESPlotController;

public class PyramidKeyPanel extends JPanel {
	
	private static final int FIXED_HEIGHT=20;
	private static final int BORDER_WIDTH=10;
	
	private RMESPlotController _controller;
	public PyramidKeyPanel(RMESPlotController controller) {
		super();
		_controller=controller;
	}
	
	public void paintComponent(Graphics g) {
		Graphics2D g2d=(Graphics2D)g;
		int width=getWidth();
		int height=getHeight();
		
		GradientPaint gradientgreen=new GradientPaint(0,height/2,Color.GREEN,width/2,height/2,Color.WHITE);
		g2d.setPaint(gradientgreen);
		g2d.fill(new Rectangle2D.Float(0,0,width/2,height));
		GradientPaint gradientred=new GradientPaint(width/2,height/2,Color.WHITE,width,height/2,Color.RED);
		g2d.setPaint(gradientred);
		g2d.fill(new Rectangle2D.Float(width/2,0,width/2,height));
		
		g2d.setColor(Color.BLACK);
		FontMetrics fm=g2d.getFontMetrics();
		String label=_controller.getLabel("PYRAMIDKEY_MAXSCORE");
		g2d.drawString(label,BORDER_WIDTH,height-fm.getDescent());
		
		label=_controller.getLabel("PYRAMIDKEY_NULLSCORE");
		Rectangle2D bounds=fm.getStringBounds(label,g2d);
		g2d.drawString(label,(int)(width-bounds.getWidth())/2,height-fm.getDescent());
	
		label=_controller.getLabel("PYRAMIDKEY_MINSCORE");
		bounds=fm.getStringBounds(label,g2d);
		g2d.drawString(label,(int)(width-bounds.getWidth())-BORDER_WIDTH,height-fm.getDescent());
		
	}
	
	public Dimension getPreferredSize() {
		Dimension dim=super.getPreferredSize();
		dim.height=FIXED_HEIGHT;
		return dim;
		
	}

}
