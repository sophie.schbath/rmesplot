/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.views.filters;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;

import fr.ssbgroup.jrmes.rmesplot.controls.RMESPlotController;
import fr.ssbgroup.jrmes.rmesplot.models.filters.ElementFilter;

/**
 * @author mhoebeke
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public abstract class FilterPanel extends JPanel {
    
    
    RMESPlotController _controller = null;
    SelectFilterButtonPanel _selectFilterButtonPanel = null;
    
    public FilterPanel(RMESPlotController controller,String name) {
        _controller=controller;
        setLayout(new BorderLayout());
        setBorder(new TitledBorder(new BevelBorder(BevelBorder.RAISED),name));
        _selectFilterButtonPanel=new SelectFilterButtonPanel(controller);
        add(_selectFilterButtonPanel,BorderLayout.NORTH);        
 
    }
    
    public boolean isActive() {
        return _selectFilterButtonPanel.isActive();
    }
    
    public abstract ElementFilter getFilter();

}
