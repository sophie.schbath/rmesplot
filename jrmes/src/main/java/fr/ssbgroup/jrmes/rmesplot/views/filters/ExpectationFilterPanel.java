/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.views.filters;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.ssbgroup.jrmes.rmesplot.controls.RMESPlotController;
import fr.ssbgroup.jrmes.rmesplot.models.filters.ElementFilter;
import fr.ssbgroup.jrmes.rmesplot.models.filters.ExpectationFilter;

/**
 * @author mhoebeke
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ExpectationFilterPanel extends FilterPanel {
    
    private static final int TEXTFIELD_MAXCOLUMNS = 8;
    
    private JTextField _lowerTextField;
    private JTextField _upperTextField;
    
    public ExpectationFilterPanel(RMESPlotController controller) {
        super(controller,controller.getLabel("EXPECTATIONFILTERBORDER_TITLE"));
        
        JPanel panel=new JPanel();
        add(panel,BorderLayout.CENTER);
        
        JPanel innerPanel=new JPanel();
        innerPanel.setLayout(new GridLayout(2,1));
        panel.add(innerPanel);
        
        JPanel lowerPanel=new JPanel();
        innerPanel.add(lowerPanel);
        
        JLabel label=new JLabel(_controller.getLabel("EXPECTATIONFILTER_LOWERTEXTFIELD"));
        lowerPanel.add(label);
        
        _lowerTextField=new JTextField(TEXTFIELD_MAXCOLUMNS);
        _lowerTextField.setText("0.0");
        lowerPanel.add(_lowerTextField);JPanel upperPanel=new JPanel();
        innerPanel.add(upperPanel);
        
        label=new JLabel(_controller.getLabel("EXPECTATIONFILTER_UPPERTEXTFIELD"));
        upperPanel.add(label);
        
        _upperTextField=new JTextField(TEXTFIELD_MAXCOLUMNS);
        _upperTextField.setText("0.0");
        upperPanel.add(_upperTextField);
        
        
    }

    /* (non-Javadoc)
     * @see rmes.views.filters.FilterPanel#getFilter()
     */
    public ElementFilter getFilter() {
        float upperThreshold=Float.parseFloat(_upperTextField.getText());
        float lowerThreshold=Float.parseFloat(_lowerTextField.getText());
        return new ExpectationFilter(lowerThreshold,upperThreshold);
    }

}
