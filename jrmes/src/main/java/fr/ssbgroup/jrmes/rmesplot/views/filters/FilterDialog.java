/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.views.filters;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import fr.ssbgroup.jrmes.rmesplot.controls.RMESPlotController;
import fr.ssbgroup.jrmes.rmesplot.models.filters.ElementFilter;
import fr.ssbgroup.jrmes.rmesplot.models.filters.ElementFilterChain;

/**
 * @author mhoebeke
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FilterDialog extends JDialog {
    
    RMESPlotController _controller;
    
    public FilterDialog(RMESPlotController controller) {
        super((JFrame)null,controller.getLabel("FILTERDIALOG_TITLE"),true);
        _controller=controller;
        Container contentPane=getContentPane();
        contentPane.setBackground(Color.white);
        contentPane.setForeground(Color.black);
        contentPane.setLayout(new BorderLayout());
       
       
        
        final MultiFilterPanel multiFilterPanel=new MultiFilterPanel(_controller);
        JScrollPane scrollPane=new JScrollPane(multiFilterPanel);
        contentPane.add(scrollPane,BorderLayout.CENTER);
        
        JPanel panel=new JPanel();
        contentPane.add(panel,BorderLayout.SOUTH);
        JButton button=new JButton(_controller.getLabel("FILTERDIALOG_APPLY_BUTTON"));
        
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                FilterDialog.this.setVisible(false);
                ElementFilterChain filterChain=_controller.getElementFilterChain();
                filterChain.clear();
                Collection activeFilters=multiFilterPanel.getActiveFilters();
                Iterator it=activeFilters.iterator();
                while (it.hasNext()) {
                    ElementFilter efilter=(ElementFilter)it.next();
                    filterChain.addFilter(efilter.getClass().getName(),efilter);
                }
                
                _controller.filtersChanged();
            }
        });
        panel.add(button);
        pack();
    }

}
