package fr.ssbgroup.jrmes.rmesplot.views.actions;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;

import fr.ssbgroup.jrmes.rmesplot.controls.RMESPlotController;

/**
 * LoadResultsAction takes care of showing the file dialog allowing the selection of R'MES result files.
 * 
 * @author mark
 *
 */

@SuppressWarnings("serial")
public class DeleteResultsAction extends AbstractAction {
	
	RMESPlotController controller;
	
	public DeleteResultsAction(RMESPlotController controller) {
		super(controller.getLabel("DEL_RESULTS"));
		this.controller=controller;
	}

	public void actionPerformed(ActionEvent evt) {
		controller.deleteResults();
	}

}
