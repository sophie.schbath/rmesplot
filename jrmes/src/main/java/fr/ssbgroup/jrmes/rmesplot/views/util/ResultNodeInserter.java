/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.views.util;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

public class ResultNodeInserter {
	
	public static void insertNode(JTree tree, DefaultMutableTreeNode parentNode, DefaultMutableTreeNode newNode) {
		parentNode.add(newNode);
	}
	
	public static void insertNodeAlpha(JTree tree, DefaultMutableTreeNode parentNode, DefaultMutableTreeNode newNode) {
		int index=0;
		boolean smaller=true;
		String newLabel=newNode.getUserObject().toString();
		DefaultTreeModel model=(DefaultTreeModel)tree.getModel();
		while (smaller && index < parentNode.getChildCount()) {
			DefaultMutableTreeNode curChild=(DefaultMutableTreeNode) parentNode.getChildAt(index);
			if (curChild.getLevel() == parentNode.getLevel()+1) {
				String label=curChild.getUserObject().toString();
				if (newLabel.compareTo(label)<0) {
					model.insertNodeInto(newNode, parentNode, index);
					smaller=false;
				}
			}
			index++;
		}
		if (smaller==true)
			model.insertNodeInto(newNode, parentNode, index);
		
	}
	
	public static void insertNodeNumeric(JTree tree, DefaultMutableTreeNode parentNode, DefaultMutableTreeNode newNode) {
		int index=0;
		boolean smaller=true;
		String newLabel=newNode.getUserObject().toString();
		newLabel=newLabel.replaceAll("\\D", "");
		DefaultTreeModel model=(DefaultTreeModel)tree.getModel();
		while (smaller && index < parentNode.getChildCount()) {
			DefaultMutableTreeNode curChild=(DefaultMutableTreeNode) parentNode.getChildAt(index);
			if (curChild.getLevel() == parentNode.getLevel()+1) {
				String label=curChild.getUserObject().toString();
				label=label.replaceAll("\\D","");
				if (Integer.parseInt(newLabel)<Integer.parseInt(label)) {
					model.insertNodeInto(newNode, parentNode, index);
					smaller=false;
				}
			}
			index++;
		}
		if (smaller==true)
			model.insertNodeInto(newNode, parentNode, index);
		
	}
	

}
