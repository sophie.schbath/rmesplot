package fr.ssbgroup.jrmes.rmesplot.views.actions;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;

import fr.ssbgroup.jrmes.rmesplot.controls.RMESPlotController;

/**
 * LoadResultsAction takes care of showing the file dialog allowing the selection of R'MES result files.
 * 
 * @author mark
 *
 */

@SuppressWarnings("serial")
public class ConfigureFiltersAction extends AbstractAction {
	
	RMESPlotController controller;
	
	public ConfigureFiltersAction(RMESPlotController controller) {
		super(controller.getLabel("FILTERS_ACTION"));
		this.controller=controller;
	}

	public void actionPerformed(ActionEvent evt) {
		controller.showFilterDialog();
	}

}
