/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.models.filters;

import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;


/**
 * @author mark
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ElementFilterChain {
    private Hashtable _filters;

    public ElementFilterChain() {
        _filters = new Hashtable();
    }

    public void addFilter(String name, ElementFilter filter) {
        _filters.put(name, filter);
    }

    public void removeFilter(String name) {
        _filters.remove(name);
    }

    public void clear() {
        _filters.clear();
    }
    
    public HashSet applyFilters(HashSet elements) {
        HashSet newElements=elements;
        Collection filters=_filters.values();
        Iterator it=filters.iterator();
        while (it.hasNext()) {
            ElementFilter filter=(ElementFilter)it.next();
            newElements=filter.apply(newElements);
        }
        return newElements;
        
    }
 
}