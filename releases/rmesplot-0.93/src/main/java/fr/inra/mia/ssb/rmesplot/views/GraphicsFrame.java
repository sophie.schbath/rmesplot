/*
 * Created on 14 f�vr. 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fr.inra.mia.ssb.rmesplot.views;

import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import fr.inra.mia.ssb.rmesplot.controls.RMESPlotController;

/**
 * @author mark
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class GraphicsFrame extends JFrame {

    public GraphicsFrame(final RMESPlotController controller) {

        setTitle(controller.getLabel("GRAPHICSFRAME_TITLE"));
 
        GraphicsTabbedPane graphicsTabbedPane = new GraphicsTabbedPane(
                controller);
        getContentPane().add(graphicsTabbedPane);

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                controller.quit();
            }
        });
        pack();
        setVisible(true);
    }
}