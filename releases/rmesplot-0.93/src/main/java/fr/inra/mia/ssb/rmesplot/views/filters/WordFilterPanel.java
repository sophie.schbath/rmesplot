/*
 * Created on 24 f�vr. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package fr.inra.mia.ssb.rmesplot.views.filters;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import fr.inra.mia.ssb.rmesplot.controls.RMESPlotController;
import fr.inra.mia.ssb.rmesplot.models.Alphabet;
import fr.inra.mia.ssb.rmesplot.models.filters.ElementFilter;
import fr.inra.mia.ssb.rmesplot.models.filters.ScoreFilter;
import fr.inra.mia.ssb.rmesplot.models.filters.WordFilter;

/**
 * @author mhoebeke
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class WordFilterPanel extends FilterPanel {
    
    private static final int TEXTAREA_ROWS = 8;
    private static final int TEXTAREA_COLUMNS=20;
    
    private JTextArea _textArea;
    private JFileChooser _fileChooser;
    
    public WordFilterPanel(RMESPlotController controller) {
        super(controller,controller.getLabel("WORDFILTERBORDER_TITLE"));
    
        _fileChooser=new JFileChooser();
        _fileChooser.setMultiSelectionEnabled(false);
        
        JPanel panel=new JPanel();
        add(panel,BorderLayout.CENTER);
        
        JLabel label=new JLabel(_controller.getLabel("WORDFILTER_AREA"));
        panel.add(label);
        
        JScrollPane scrollPane=new JScrollPane();
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        panel.add(scrollPane);
        
        _textArea=new JTextArea();
        _textArea.setColumns(TEXTAREA_COLUMNS);
        _textArea.setRows(TEXTAREA_ROWS);
        _textArea.setLineWrap(true);
        scrollPane.setViewportView(_textArea);
        
        JPanel buttonPanel=new JPanel();
        add(buttonPanel,BorderLayout.SOUTH);
        JButton button=new JButton(controller.getLabel("WORDLIST_LOAD_BUTTON"));
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (_fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                    File file=_fileChooser.getSelectedFile();
                    String buffer=new String();
                    try {
                        BufferedReader reader=new BufferedReader(new FileReader(file)) ;
                        while (reader.ready()) {
                            if (buffer.length()>0)
                                buffer=buffer+"\n";
                            buffer=buffer+reader.readLine();
                        }
                        _textArea.setText(buffer);
                    } catch (FileNotFoundException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                    
                    
                }
                
            }
        });
        
        
        buttonPanel.add(button);
        button=new JButton(controller.getLabel("WORDLIST_SAVE_BUTTON"));
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (_fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
                    File file=_fileChooser.getSelectedFile();
                    String buffer=new String();
                    try {
                        PrintWriter writer=new PrintWriter(new FileWriter(file)) ;
                        writer.println(_textArea.getText());
                        writer.close();
                    } catch (FileNotFoundException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                    
                    
                }
                
            }
        });
        
        buttonPanel.add(button);
        }

    /* (non-Javadoc)
     * @see rmes.views.filters.FilterPanel#getFilter()
     */
    public ElementFilter getFilter() {
        HashSet wordSet=new HashSet();
        String wordString=_textArea.getText();
        if (wordString != null) {
            String[] wordTab=wordString.split("\\s");
            for (int i=0;i<wordTab.length;i++) {
                if (wordTab[i].length()>0) {
                    String[] expansions=Alphabet.getCurrentAlphabet().expand(wordTab[i]);
                    for (int j=0;j<expansions.length;j++)
                        wordSet.add(expansions[j]);
                }
            }
        }
        return new WordFilter(wordSet);
    }

}
