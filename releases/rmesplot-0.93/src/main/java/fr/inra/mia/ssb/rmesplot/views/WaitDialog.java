/*
 * Created on 24 f�vr. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package fr.inra.mia.ssb.rmesplot.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JDialog;
import javax.swing.JLabel;

import fr.inra.mia.ssb.rmesplot.controls.RMESPlotController;

/**
 * @author mhoebeke
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class WaitDialog extends JDialog {
    
    JLabel _label = null;
    public WaitDialog(RMESPlotController controller) {
        super(controller.getDataFrame(),"RMESPlot : "+controller.getLabel("WAITDIALOG_TITLE"),false);
        _label=new JLabel(controller.getLabel("WAITDIALOG_DEFAULT_MESSAGE"));
        Container contentPane=getContentPane();
        contentPane.setLayout(new BorderLayout());
        contentPane.setBackground(Color.white);
        contentPane.setForeground(Color.white);
        contentPane.add(_label,BorderLayout.CENTER);
        pack();
    }
    
    
    public void setMessage(String message) {
        _label.setText(message);
        pack();
    }

}
