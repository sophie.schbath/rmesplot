package fr.inra.mia.ssb.rmesplot.models;

import java.util.*;

/**
 * This factory class has the capability of building instances of one of the
 * subclasses of ElementStats depending on the name of the algorithm contained
 * in the R'MES result file. The algorithms known are : Cond_as, Mart, Mart_r,
 * Cond_as_pc, Cond_as_p.
 * 
 * @author $Author: mhoebeke $
 * @version $Revision: 123 $
 */
public class ElementStatsFactory {
    private Hashtable _elementTypes;

    /**
     * Creates a new ElementStatsFactory object.
     */
    public ElementStatsFactory() {
        _elementTypes = new Hashtable();
        _elementTypes.put("Cond_as", "GaussStats");
        _elementTypes.put("Mart", "GaussStats");
        _elementTypes.put("Mart_r", "GaussStats");
        _elementTypes.put("Cond_as_pc", "PoissonCompStats");
        _elementTypes.put("Cond_as_p", "PoissonStats");
        _elementTypes.put("Exact","ExactStats");
    }

    /**
     * Returns an instance on one of the subclasses of ElementStats.
     * 
     * @param algorithm
     *            a String denoting the type of algorithm used by R'MES.
     * 
     * @return an instance of one of the subclasses of ElementStats.
     * 
     * @throws UnknownAlgorithmException
     *             raised of the String given as argument does not match any of
     *             the known algorithms.
     */
    public ElementStats getInstance(String algorithm)
            throws UnknownAlgorithmException {
        ElementStats res = null;
        String packageName = getClass().getPackage().getName();
        String className = packageName + "."
                + (String) _elementTypes.get(algorithm);

        if (className != null) {
            try {
                Class iClass = Class.forName(className);
                res = (ElementStats) iClass.newInstance();
            } catch (ClassNotFoundException ce) {
                throw new UnknownAlgorithmException(algorithm + " " + className);
            } catch (InstantiationException ie) {
                throw new UnknownAlgorithmException(algorithm);
            } catch (IllegalAccessException ie) {
                throw new UnknownAlgorithmException(algorithm);
            }
        }

        return res;
    }
}