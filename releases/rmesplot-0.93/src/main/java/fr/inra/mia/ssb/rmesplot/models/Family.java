package fr.inra.mia.ssb.rmesplot.models;

/**
 * Class holding information about word families defined by patterns. Every
 * instance if this class represents a family of words and is characterized by : -
 * a name, - the length of the words, - the set of words in the family.
 * 
 * @author $Author: mhoebeke $
 * @version $Revision: 123 $
 */
public class Family {
    private String _name;

    private int _wordLength;

    private String[] _words;

    /**
     * Creates a new Family object. The attributes are initialized with bogus
     * values.
     */
    public Family() {
        _name = "Unknown";
        _wordLength = -1;
        _words = null;
    }

    /**
     * Initializes the name of this Family.
     * 
     * @param name
     *            the name for this Family.
     */
    public void setName(String name) {
        _name = name;
    }

    /**
     * Returns the name of this Family.
     * 
     * @return the name of this Family.
     */
    public String getName() {
        return _name;
    }

    /**
     * Initializes the word length of this Family.
     * 
     * @param wordLength
     *            the number of letters of this Family.
     */
    public void setWordLength(int wordLength) {
        _wordLength = wordLength;
    }

    /**
     * Returns the words length of this Family.
     * 
     * @return The number of letters of this Family.
     */
    public int getWordLength() {
        return _wordLength;
    }

    /**
     * Initializes the set of words of this Family.
     * 
     * @param words
     *            the array of words of this Family.
     */
    public void setWords(String[] words) {
        _words = words;
    }

    /**
     * Returns the set of words of this Famimy.
     * 
     * @return an array containing the words of this Family.
     */
    public String[] getWords() {
        return _words;
    }
}