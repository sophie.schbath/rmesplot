/*
 * Created on 14 f�vr. 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fr.inra.mia.ssb.rmesplot.models.filters;

import java.util.HashSet;
import java.util.Iterator;

import fr.inra.mia.ssb.rmesplot.models.ElementStats;

/**
 * @author mark
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class WordFilter implements ElementFilter {

    private HashSet _words;

    public WordFilter(HashSet words) {
        _words = words;
    }

    /* (non-Javadoc)
     * @see rmes.models.ElementFilter#apply(java.util.HashSet)
     */
    public HashSet apply(HashSet elements) {
        HashSet newElements=new HashSet();
        Iterator it=elements.iterator();
        while (it.hasNext()) {
            ElementStats es=(ElementStats)it.next();
            String word=es.getName();
            if (_words.contains(word))
                newElements.add(es);
        }
        return newElements;
    }

 

}