package fr.inra.mia.ssb.rmesplot.views;

import java.awt.*;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import fr.inra.mia.ssb.rmesplot.controls.RMESPlotController;
import fr.inra.mia.ssb.rmesplot.models.Model;

import java.awt.event.*;
import java.io.File;
import java.util.HashSet;
import java.util.Hashtable;

/**
 * Class implementing the layout of ResultTablePane.
 * 
 * @author $Author: mhoebeke $
 * @version $Revision: 123 $
 */
public class DataTablePanel extends JPanel implements ListSelectionListener {

    private RMESPlotController _controller;

    private Model _model;

    private int _index;
    
    private DataTable _table;
    
    private JScrollPane _scrollPane;
    

    private static int WORD_TEXTFIELD_SIZE = 8;

    private static int ENTRIES_TEXTFIELD_SIZE = 5;

    public DataTablePanel(RMESPlotController controller, Model model, int index) {

        _controller = controller;
        _model = model;
        _index = index;

        setLayout(new BorderLayout());
        JPanel tablePanel = new JPanel(new BorderLayout());
        tablePanel.setBorder(BorderFactory.createTitledBorder(_controller
                .getLabel("TAB_VALUES")));
        add(tablePanel, BorderLayout.CENTER);

        _table = new DataTable(controller, model);
        _scrollPane = new JScrollPane(_table);
        _scrollPane
                .setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        _scrollPane
                .setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        tablePanel.add(_scrollPane, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setBorder(BorderFactory.createTitledBorder(_controller
                .getLabel("BORDER_OPTIONS")));
        add(buttonPanel, BorderLayout.SOUTH);

        buttonPanel.setLayout(new GridLayout(2,1));
        
        JPanel linePanel=new JPanel();
        buttonPanel.add(linePanel);
        JButton button = new JButton(_controller.getLabel("BUTTON_PLOT_ON_X"));
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                _controller.plotOnX(_model, _index);
            }
        });
        linePanel.add(button);

        button = new JButton(_controller.getLabel("BUTTON_PLOT_ON_Y"));
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                _controller.plotOnY(_model, _index);
            }
        });
        linePanel.add(button);

        JLabel label = new JLabel(_controller.getLabel("FIND_WORD_LABEL"));
        linePanel.add(label);

        final JTextField wordTextField = new JTextField();
        wordTextField.setColumns(WORD_TEXTFIELD_SIZE);
        wordTextField.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                String word=wordTextField.getText();
                int row=_table.findWord(word);
                if (row>=0) {
                    Rectangle cellRect=_table.getCellRect(row,0,true);
                    JViewport viewport=_scrollPane.getViewport();
                    viewport.setViewPosition(cellRect.getLocation());
                } else {
                    JOptionPane.showMessageDialog(_controller.getDataFrame(),_controller.getLabel("WORD_NOT_FOUND_LABEL"),_controller.getLabel("WORD_NOT_FOUND_TITLE"),JOptionPane.ERROR_MESSAGE);
                }
            }
            });
        
        linePanel.add(wordTextField);
     
        label = new JLabel(_controller.getLabel("MAX_ENTRIES_LABEL"));
        linePanel.add(label);
        
        final JTextField maxEntriesTextField = new JTextField();
        maxEntriesTextField.setColumns(ENTRIES_TEXTFIELD_SIZE);
        maxEntriesTextField.setText((String)_controller.getProperty("datatable.maxentries"));
        maxEntriesTextField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int maxEntries = Integer
                        .parseInt(maxEntriesTextField.getText());
                _table.setMaxEntries(maxEntries);
            }
        });

        linePanel.add(maxEntriesTextField);

        
        linePanel=new JPanel();
        buttonPanel.add(linePanel);
        
        JCheckBox checkbox=new JCheckBox(_controller.getLabel("AUTO_ADD_PYRAMIDS_CHECKBOX"));
        checkbox.setSelected(_controller.getAutoAddPyramids());
        checkbox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED)
                    _controller.setAutoAddPyramids(true);
                else
                    _controller.setAutoAddPyramids(false);
            }
        });
        linePanel.add(checkbox);
        
        button = new JButton(_controller.getLabel("BUTTON_EXPORT"));
        button.addActionListener(new ActionListener() {
        	@Override
			public void actionPerformed(ActionEvent arg0) {
        		final JFileChooser fileChooser=new JFileChooser(_controller.getCurrentDir());
            	int retval = fileChooser.showSaveDialog(null);
                if (retval == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                    _controller.setCurrentDir(fileChooser.getCurrentDirectory());
            		_controller.exportModel(_table,file);
            	}
			}
        });
        linePanel.add(button);
        
        button = new JButton(_controller.getLabel("BUTTON_REMOVE"));
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                _controller.removeModel(_model, _index);
                Container parent = DataTablePanel.this.getParent();
                parent.remove(DataTablePanel.this);
                //_controller.removeElements(name);
            }
        });
        linePanel.add(button);
        
    }
    
    public void applyFilters() {
        _table.applyFilters();
    }

    public void valueChanged(ListSelectionEvent e) {
        // TODO Auto-generated method stub

    }
}