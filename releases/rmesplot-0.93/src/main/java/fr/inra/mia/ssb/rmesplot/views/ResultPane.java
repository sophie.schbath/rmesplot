package fr.inra.mia.ssb.rmesplot.views;

import javax.swing.*;
import java.awt.*;

/**
 * This class joins the JTabbedPane containing all the selected tables and the
 * button which can remove the last selected table.
 * 
 * @author $Author : jguerin $
 * @version $Revision : 1.1 $
 */
public class ResultPane extends JPanel {
    DataTableTabbedPane _rtPane;

    JButton _button;

    /**
     * Creates a ResultPane Object
     * 
     * @param rtPane
     *            the ResultTablePane displaying result tables.
     */
    ResultPane(DataTableTabbedPane rtPane) {
        _rtPane = rtPane;
        _button = new JButton("Reset a table");
        setLayout(new BorderLayout());
        add(_rtPane, BorderLayout.CENTER);
    }
}