package fr.inra.mia.ssb.rmesplot.views;

import fr.inra.mia.ssb.rmesplot.*;
import fr.inra.mia.ssb.rmesplot.controls.RMESPlotController;
import fr.inra.mia.ssb.rmesplot.models.Model;

import java.io.*;
import java.lang.reflect.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import java.beans.*;

/**
 * Class defining the tabbed pane for the two table views. This class defines
 * the behavior of the tabbed pane where each of the two data sets are
 * displayed.
 * 
 * @author $Author: mhoebeke $
 * @version $Revision: 123 $
 */
public class DataTableTabbedPane extends JTabbedPane {

    RMESPlotController _controller;

    DataTablePanel _layeredTabbedPane;

    int _tabIndex;

    JPopupMenu _popup;

    JMenuItem _item;

    /**
     * Creates a new ResultTablePane object.
     * 
     * @param tree
     *            the ResultTree containing the currently available results.
     * @param plotPanel
     *            the PlotArea where the graphics are drawn.
     * @param pyramidPanel
     *            the PyramidArea where pyramids are drawn.
     */
    public DataTableTabbedPane(RMESPlotController controller) {
        super(JTabbedPane.TOP);

        _controller = controller;
        _tabIndex = 0;

        _controller.setDataTableTabbedPane(this);

    }

    public void addModel(Model model,String description) {
    	++_tabIndex;
    	String tabName=""+_tabIndex+"-"+_controller.buildTitleString(model);
        DataTablePanel dataTablePanel = new DataTablePanel(_controller, model,
                _tabIndex);
        addTab(tabName, null, dataTablePanel, description);
        setSelectedComponent(dataTablePanel);
    }
    
    public void applyFilters() {
        Component[] components=getComponents();
        for (int i=0;i<components.length;i++) {
            if (components[i] instanceof DataTablePanel) {
                ((DataTablePanel)components[i]).applyFilters();
            }
        }
    }

    /**
     * Method reacting to user selections on the tabular data. This method is
     * called when the user selects/deselects a value in the table, and causes
     * the corresponding value to be highlighted/unhighlighted in the plot area.
     * 
     * @param le
     *            the actual ListSelectionEvent.
     */
    public void valueChanged(ListSelectionEvent le) {

    }

    /**
     * Method displaying a window that gives a warning message. No pyramid can
     * be displaying with word family or degenerated words.
     */
    public void dialogWindow() {
        JDialog dialog;
        JFrame cadre = new JFrame("Warning");
        JPanel pan = new JPanel();
        JLabel lab = new JLabel(
                "you can't display pyramid of a word family or a degenerated word");
        pan.add(lab);
        Object[] options = { "OK" };
        JOptionPane pane = new JOptionPane(pan, JOptionPane.WARNING_MESSAGE,
                JOptionPane.DEFAULT_OPTION, null, options);
        dialog = pane.createDialog(cadre, "Warning");
        dialog.pack();
        dialog.setVisible(true);
    }

}