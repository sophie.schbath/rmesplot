/*
 * Created on 24 f�vr. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package fr.inra.mia.ssb.rmesplot.views.filters;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import fr.inra.mia.ssb.rmesplot.controls.RMESPlotController;
import fr.inra.mia.ssb.rmesplot.models.filters.ElementFilter;
import fr.inra.mia.ssb.rmesplot.models.filters.ObservedFilter;
import fr.inra.mia.ssb.rmesplot.models.filters.ScoreFilter;

/**
 * @author mhoebeke
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ObservedFilterPanel extends FilterPanel {
    
    private static final int TEXTFIELD_MAXCOLUMNS = 8;
    
    private JTextField _textField;
    private JRadioButton _aboveButton;
    private JRadioButton _belowButton;
    
    
    public ObservedFilterPanel(RMESPlotController controller) {
        super(controller,controller.getLabel("OBSERVEDFILTERBORDER_TITLE"));
        
        JPanel panel=new JPanel();
        add(panel,BorderLayout.CENTER);
        
        panel.setLayout(new GridLayout(2,1));
        
        JPanel threshPanel=new JPanel();
        panel.add(threshPanel);
        
        JLabel label=new JLabel(controller.getLabel("OBSERVEDFILTER_TEXTFIELD"));
        threshPanel.add(label);
        
        _textField=new JTextField(TEXTFIELD_MAXCOLUMNS);
        _textField.setText("0.0");
        threshPanel.add(_textField);
        
        JPanel buttonPanel=new JPanel();
        panel.add(buttonPanel);
        
        label=new JLabel(controller.getLabel("OBSERVEDFILTER_BUTTONLABEL"));
        buttonPanel.add(label);
        
        _aboveButton=new JRadioButton(controller.getLabel("OBSERVEDFILTER_ABOVE"));
        _aboveButton.setSelected(true);
        buttonPanel.add(_aboveButton);
        _belowButton=new JRadioButton(controller.getLabel("OBSERVEDFILTER_BELOW"));
        buttonPanel.add(_belowButton);
        
        ButtonGroup threshGroup=new ButtonGroup();
        threshGroup.add(_aboveButton);
        threshGroup.add(_belowButton);
        }

    /* (non-Javadoc)
     * @see rmes.views.filters.FilterPanel#getFilter()
     */
    public ElementFilter getFilter() {
        int threshold=Integer.parseInt(_textField.getText());
        boolean above=_aboveButton.isSelected();
        return new ObservedFilter(threshold,above);
    }

}
