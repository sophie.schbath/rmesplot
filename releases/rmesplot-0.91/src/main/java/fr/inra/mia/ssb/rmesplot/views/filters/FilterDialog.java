/*
 * Created on 23 f�vr. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package fr.inra.mia.ssb.rmesplot.views.filters;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import fr.inra.mia.ssb.rmesplot.controls.RMESPlotController;
import fr.inra.mia.ssb.rmesplot.models.filters.ElementFilter;
import fr.inra.mia.ssb.rmesplot.models.filters.ElementFilterChain;

/**
 * @author mhoebeke
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FilterDialog extends JDialog {
    
    RMESPlotController _controller;
    
    public FilterDialog(RMESPlotController controller) {
        super((JFrame)null,controller.getLabel("FILTERDIALOG_TITLE"),true);
        _controller=controller;
        Container contentPane=getContentPane();
        contentPane.setBackground(Color.white);
        contentPane.setForeground(Color.black);
        contentPane.setLayout(new BorderLayout());
       
       
        
        final MultiFilterPanel multiFilterPanel=new MultiFilterPanel(_controller);
        JScrollPane scrollPane=new JScrollPane(multiFilterPanel);
        contentPane.add(scrollPane,BorderLayout.CENTER);
        
        JPanel panel=new JPanel();
        contentPane.add(panel,BorderLayout.SOUTH);
        JButton button=new JButton(_controller.getLabel("FILTERDIALOG_APPLY_BUTTON"));
        
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                FilterDialog.this.setVisible(false);
                ElementFilterChain filterChain=_controller.getElementFilterChain();
                filterChain.clear();
                Collection activeFilters=multiFilterPanel.getActiveFilters();
                Iterator it=activeFilters.iterator();
                while (it.hasNext()) {
                    ElementFilter efilter=(ElementFilter)it.next();
                    filterChain.addFilter(efilter.getClass().getName(),efilter);
                }
                
                _controller.filtersChanged();
            }
        });
        panel.add(button);
        pack();
    }

}
