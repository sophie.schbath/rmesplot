package fr.inra.mia.ssb.rmesplot.models;

/**
 * Exception raised when trying to build results for an unknown algorithm.
 * 
 * @author $Author: mhoebeke $
 * @version $Revision: 123 $
 */
public class UnknownAlgorithmException extends Exception {
    /**
     * Creates a new UnknownAlgorithmException object.
     * 
     * @param s
     *            a String giving the name of the unknown algorithm.
     */
    public UnknownAlgorithmException(String s) {
        super(s);
    }
}