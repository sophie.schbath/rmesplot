/*
 * Created on 25 f�vr. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package fr.inra.mia.ssb.rmesplot.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.inra.mia.ssb.rmesplot.controls.RMESPlotController;

/**
 * @author mhoebeke
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class AboutDialog extends JDialog {
    
    public AboutDialog(RMESPlotController controller) {
        super(controller.getDataFrame(),controller.getLabel("ABOUTDIALOG_TITLE"),true);
        setBackground(Color.white);
        setForeground(Color.white);
        
        Container contentPane=getContentPane();
        contentPane.setLayout(new BorderLayout());
        
        JPanel innerPanel=new JPanel();
        contentPane.add(innerPanel,BorderLayout.CENTER);
        innerPanel.setLayout(new BorderLayout());
        
        String string="<html><h2>RMESPlot V "+controller.getProperty("rmesplot.version")+"</h2></html>";
        JLabel label=new JLabel(string);
        innerPanel.add(label,BorderLayout.NORTH);
        
        string=controller.getLabel("ABOUTDIALOG_TEXT");
        label=new JLabel(string);
        innerPanel.add(label,BorderLayout.CENTER);
        
        JPanel lowerPanel=new JPanel();
        lowerPanel.setLayout(new BorderLayout());
        innerPanel.add(lowerPanel,BorderLayout.SOUTH); 
        
		try {
			InputStream imgStream=controller.getClass().getResourceAsStream("/logo-mig.jpg");
			int size = imgStream.available();
			byte[] imagedata=new byte[size];
			int read=0;
			int offset=0;
			while (read<size) {
				read+=imgStream.read(imagedata,offset,size-offset);
				offset=read;
			}
			imgStream.close();
			label=new JLabel();
			Icon icon=new ImageIcon(imagedata);
			label.setIcon(icon);
			lowerPanel.add(label,BorderLayout.CENTER);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
        JButton button=new JButton(controller.getLabel("OK"));
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                AboutDialog.this.setVisible(false);
            }
        });
        contentPane.add(button,BorderLayout.SOUTH);
        pack();
        
        
    }

}
