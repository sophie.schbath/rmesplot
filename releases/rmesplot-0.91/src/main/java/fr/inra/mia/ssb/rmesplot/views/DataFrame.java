/*
 * Created on 14 f�vr. 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fr.inra.mia.ssb.rmesplot.views;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import fr.inra.mia.ssb.rmesplot.controls.RMESPlotController;

/**
 * @author mark
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class DataFrame extends JFrame {

    RMESPlotController _controller;

    ResultTree _tree;

    DataTableTabbedPane _dataTableTabbedPane;

    public DataFrame(final RMESPlotController controller) {

        _controller = controller;

        setTitle(_controller.getLabel("DATAFRAME_TITLE"));
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                controller.quit();
            }
        });

        Container contentPane = getContentPane();

        RMESPlotMenuBar menubar = new RMESPlotMenuBar(_controller);
        setJMenuBar(menubar);

        contentPane.setLayout(new BorderLayout());

        /**
         * Split left part vertically : - on top : result tree, - at the bottom :
         * tables.
         */
        JSplitPane vsplitpane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        contentPane.add(vsplitpane);
        _tree = new ResultTree(_controller);
        JScrollPane treePane = new JScrollPane(_tree);
        treePane
                .setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        treePane
                .setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        treePane.setPreferredSize(new Dimension(480, 200));
        vsplitpane.setTopComponent(treePane);

        _dataTableTabbedPane = new DataTableTabbedPane(_controller);
        vsplitpane.setBottomComponent(_dataTableTabbedPane);

        
        vsplitpane.setPreferredSize(new Dimension(800, 600));
        pack();
        setVisible(true);

    }

    public ResultTree getResultTree() {
        return _tree;
    }

    public DataTableTabbedPane getTabbedPane() {
        return _dataTableTabbedPane;
    }
}