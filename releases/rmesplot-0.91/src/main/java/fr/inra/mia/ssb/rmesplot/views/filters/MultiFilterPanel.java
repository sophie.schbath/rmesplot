/*
 * Created on 24 f�vr. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package fr.inra.mia.ssb.rmesplot.views.filters;

import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import fr.inra.mia.ssb.rmesplot.controls.RMESPlotController;

/**
 * @author mhoebeke
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MultiFilterPanel extends JPanel {
    
    private static final int N_FILTERS=4;
    
    private RMESPlotController _controller = null;
    
    private HashSet _filterPanels = null;
    
    public MultiFilterPanel(RMESPlotController controller) {
        super();
        _controller=controller;
        _filterPanels=new HashSet();
        
        setLayout(new GridLayout(N_FILTERS,1));
        
        FilterPanel filterPanel=new ScoreFilterPanel(_controller);
        add(filterPanel);
        _filterPanels.add(filterPanel);
        
        filterPanel=new WordFilterPanel(_controller);
        add(filterPanel);
        _filterPanels.add(filterPanel);
        
        filterPanel=new ObservedFilterPanel(_controller);
        add(filterPanel);
        _filterPanels.add(filterPanel);

        filterPanel=new ExpectationFilterPanel(_controller);
        add(filterPanel);
        _filterPanels.add(filterPanel);
    }
    
    public Collection getActiveFilters() {
        HashSet filters=new HashSet();
        Iterator it=_filterPanels.iterator();
        while (it.hasNext()) {
            FilterPanel filterPanel=(FilterPanel)it.next();
            if (filterPanel.isActive()) {
                filters.add(filterPanel.getFilter());
            }
        }
        return filters;
    }

}
