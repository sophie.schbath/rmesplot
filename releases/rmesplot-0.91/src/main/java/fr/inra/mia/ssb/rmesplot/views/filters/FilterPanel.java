/*
 * Created on 24 f�vr. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package fr.inra.mia.ssb.rmesplot.views.filters;

import java.awt.BorderLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import fr.inra.mia.ssb.rmesplot.controls.RMESPlotController;
import fr.inra.mia.ssb.rmesplot.models.filters.ElementFilter;

/**
 * @author mhoebeke
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public abstract class FilterPanel extends JPanel {
    
    
    RMESPlotController _controller = null;
    SelectFilterButtonPanel _selectFilterButtonPanel = null;
    
    public FilterPanel(RMESPlotController controller,String name) {
        _controller=controller;
        setLayout(new BorderLayout());
        setBorder(new TitledBorder(new BevelBorder(BevelBorder.RAISED),name));
        _selectFilterButtonPanel=new SelectFilterButtonPanel(controller);
        add(_selectFilterButtonPanel,BorderLayout.NORTH);        
 
    }
    
    public boolean isActive() {
        return _selectFilterButtonPanel.isActive();
    }
    
    public abstract ElementFilter getFilter();

}
