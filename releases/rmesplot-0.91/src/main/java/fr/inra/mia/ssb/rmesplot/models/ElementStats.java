package fr.inra.mia.ssb.rmesplot.models;

/**
 * Base class for the elementary data items about which R'MES generates results.
 * These data items are either words of word patterns (families), and each of
 * these items has the following descriptors : - its name, - the number of its
 * observations in the data. - a stastistical indicator of the deviation of the
 * observed count wrt. a model. Derived classes are allowed to add other
 * descriptors, as long as they correctly redefine the setAttributes() and
 * getNAttributes() methods.
 * 
 * @author $Author: mhoebeke $
 * @version $Revision: 123 $
 */
public abstract class ElementStats {
    Integer _rank;

    String _name;

    Float _count;

    Float _score;

    /**
     * Creates a new ElementStats object. Instantiates a new ElementStats object
     * with bogus attributes.
     */
    protected ElementStats() {
        _rank = new Integer(-1);
        _name = "Unnamed";
        _count = new Float(-1);
        _score = new Float(-1);
    }

    /**
     * Returns the number of numerical descriptors avaible for this data item.
     * 
     * @return the number of descriptors associated with this data item.
     */
    public int getNAttributes() {
        return 2;
    }

    /**
     * Initializes the numerical descriptors of this data item. Intializes the
     * observed count and the expected count from the vector given as argument.
     * 
     * @param attributes
     *            a 2 element array where the first element contains this item's
     *            observed count and the last element contains the statistical
     *            deviation of this count.
     */
    public void setAttributes(Float[] attributes) {
        _count = attributes[0];
        _score = attributes[attributes.length - 1];
    }

    /**
     * Initializes the name of this data item.
     * 
     * @param name
     *            a String defining the name of this data item.
     */
    public void setName(String name) {
        _name = name;
    }

    /**
     * Returns the name of this data item.
     * 
     * @return a String holding the name of this data item.
     */
    public String getName() {
        return _name;
    }

    /**
     * Initializes the observed count of this data item.
     * 
     * @param count
     *            the number of actual observations of this item in the data
     *            set.
     */
    public void setCount(Float count) {
        _count = count;
    }

    /**
     * Returns the observed count of this data item.
     * 
     * @return the number of actual observations of this item in the data set.
     */
    public Float getCount() {
        return _count;
    }

    /**
     * Initialzes the statistical deviation indicator.
     * 
     * @param stat
     *            the value of the statistical deviation of this element's
     *            observer count wrt. the model.
     */
    public void setScore(Float score) {
        _score = score;
    }

    /**
     * Returns the statistical deviation indicator.
     * 
     * @return the value of the statistical deviation of this element's observed
     *         count wrt. the model.
     */
    public Float getScore() {
        return _score;
    }

    public void setRank(Integer rank) {
        _rank = rank;
    }

    public Integer getRank() {
        return _rank;
    }
}