package fr.inra.mia.ssb.rmesplot.views;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import fr.inra.mia.ssb.rmesplot.controls.RMESPlotController;
import fr.inra.mia.ssb.rmesplot.models.ElementStats;
import fr.inra.mia.ssb.rmesplot.models.GaussStats;
import fr.inra.mia.ssb.rmesplot.models.Model;

/**
 * Class managing mouse clics on column headers to set sorting criterion and/or
 * direction. Table data can be sorted according to any of the descriptors
 * contained in the columns. A first click on a column header makes the
 * corresponding data the sort cirterion. Subsequent clics toggle the sorting
 * direction.
 * 
 * @author $Author: mhoebeke $
 * @version $Revision: 123 $
 */
class DataTableColumnHeaderListener extends MouseAdapter {
    private DataTable _table;

    /**
     * Creates a new ResultColumnHeaderListener object.
     * 
     * @param rTable
     *            the ResultTable whose columns are to be managed by this
     *            listener.
     */
    DataTableColumnHeaderListener(DataTable table) {
        _table = table;
    }

    /**
     * Callback activated when a mouse click occurs on one of the column
     * headers.
     * 
     * @param e
     *            actual MouseEvent that occured.
     */
    public void mouseClicked(MouseEvent e) {
        TableColumnModel cModel = _table.getColumnModel();
        int columnClicked = cModel.getColumnIndexAtX(e.getX());
        int column = _table.convertColumnIndexToModel(columnClicked);
        DataTableTableModel tModel = (DataTableTableModel) _table.getModel();
        tModel.setSortColumn(column);
    }
}
/**
 * This class manages the display of the tabular data. This class extends the
 * AbstractTableModel to provide the methods allowing Swing components to acces
 * the its tabular data.
 * 
 * @author $Author: mhoebeke $
 * @version $Revision: 123 $
 */

class DataTableTableModel extends AbstractTableModel {

    private static final int SORT_ASC = 1;

    private static final int SORT_DESC = -1;

    private static final Integer COMPARATOR_FLOAT_ASC = new Integer(1);

    private static final Integer COMPARATOR_FLOAT_DESC = new Integer(2);

    private static final Integer COMPARATOR_INTEGER_ASC = new Integer(3);

    private static final Integer COMPARATOR_INTEGER_DESC = new Integer(4);

    private static final Integer COMPARATOR_STRING_ASC = new Integer(5);

    private static final Integer COMPARATOR_STRING_DESC = new Integer(6);

    private RMESPlotController _controller;
    
    private Model _model;

    private String[] _headers;

    private Method[] _getters;

    private ElementStats[] _data;
    
    private Hashtable _comparators;

    private int _sortColumn;

    private int _sortDirection;

    private int _maxEntries;

    private String _focussedWord;

    private int _offset;

    /**
     * Creates a new ResultTableModel object.
     */
    DataTableTableModel(RMESPlotController controller, Model model) {

        _controller = controller;
        _model = model;
        
        _sortColumn = 0;
        _sortDirection = SORT_ASC;
        _focussedWord = null;

        _maxEntries = Integer.parseInt((String)_controller.getProperty("datatable.maxentries"));
        buildComparators();
        buildMetaData();
        buildData();

    }

    private void buildComparators() {

        _comparators = new Hashtable();

        Comparator comparator = new Comparator() {
            public int compare(Object o1, Object o2) {
                int retval = 0;
                try {
                    Float float1 = (Float) _getters[_sortColumn].invoke(o1,
                            null);
                    Float float2 = (Float) _getters[_sortColumn].invoke(o2,
                            null);
                    retval = float1.compareTo(float2);
                } catch (IllegalArgumentException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return retval;
            }
        };
        _comparators.put(COMPARATOR_FLOAT_ASC, comparator);

        comparator = new Comparator() {
            public int compare(Object o1, Object o2) {
                int retval = 0;
                try {
                    Float float1 = (Float) _getters[_sortColumn].invoke(o1,
                            null);
                    Float float2 = (Float) _getters[_sortColumn].invoke(o2,
                            null);
                    retval = float2.compareTo(float1);
                } catch (IllegalArgumentException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return retval;
            }
        };
        _comparators.put(COMPARATOR_FLOAT_DESC, comparator);

        comparator = new Comparator() {
            public int compare(Object o1, Object o2) {
                int retval = 0;
                try {
                    Integer integer1 = (Integer) _getters[_sortColumn].invoke(
                            o1, null);
                    Integer integer2 = (Integer) _getters[_sortColumn].invoke(
                            o2, null);
                    retval = integer1.compareTo(integer2);
                } catch (IllegalArgumentException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return retval;
            }
        };
        _comparators.put(COMPARATOR_INTEGER_ASC, comparator);

        comparator = new Comparator() {
            public int compare(Object o1, Object o2) {
                int retval = 0;
                try {
                    Integer integer1 = (Integer) _getters[_sortColumn].invoke(
                            o1, null);
                    Integer integer2 = (Integer) _getters[_sortColumn].invoke(
                            o2, null);
                    retval = integer2.compareTo(integer1);
                } catch (IllegalArgumentException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return retval;
            }
        };
        _comparators.put(COMPARATOR_INTEGER_DESC, comparator);

        comparator = new Comparator() {
            public int compare(Object o1, Object o2) {
                int retval = 0;
                try {
                    String string1 = (String) _getters[_sortColumn].invoke(o1,
                            null);
                    String string2 = (String) _getters[_sortColumn].invoke(o2,
                            null);
                    retval = string1.compareTo(string2);
                } catch (IllegalArgumentException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return retval;
            }
        };
        _comparators.put(COMPARATOR_STRING_ASC, comparator);

        comparator = new Comparator() {
            public int compare(Object o1, Object o2) {
                int retval = 0;
                try {
                    String string1 = (String) _getters[_sortColumn].invoke(o1,
                            null);
                    String string2 = (String) _getters[_sortColumn].invoke(o2,
                            null);
                    retval = string2.compareTo(string1);
                } catch (IllegalArgumentException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return retval;
            }
        };
        _comparators.put(COMPARATOR_STRING_DESC, comparator);
    }

    private void buildMetaData() {

        Vector headers = new Vector();
        // Initialize common headers.
        headers.add("Rank");
        headers.add("Name");
        headers.add("Count");
        headers.add("Score");

        // Initialize specific headers by introspection :
        // get all methods returning Floats and whose names
        // start with "get".
        HashSet elements=_model.getElements();
        Iterator it = elements.iterator();
        ElementStats es = (ElementStats) it.next();
        Class esClass = es.getClass();
        Method[] esMethods = esClass.getDeclaredMethods();
        for (int i = 0; i < esMethods.length; i++) {
            String name = esMethods[i].getName();
            Class returnType = esMethods[i].getReturnType();
            if (name.startsWith("get") && returnType == Float.class) {
                String attribute = name.substring(3);
                headers.add(attribute);
            }
        }

        _headers = new String[headers.size()];
        _headers = (String[]) headers.toArray(_headers);

        _getters = new Method[headers.size()];
        Class[] params = new Class[0];
        for (int i = 0; i < _headers.length; i++) {
            String methodName = "get" + _headers[i];
            try {
                _getters[i] = esClass.getMethod(methodName, params);
            } catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    void buildData() {
        HashSet filteredElements=_controller.applyFilters(_model.getElements());
        _data = new ElementStats[filteredElements.size()];
        _data = (ElementStats[]) filteredElements.toArray(_data);
        sortData();
        computeOffset();
    }

    /**
     * Method returning the name of a given column.
     * 
     * @param col
     *            the column index for which the name is requested.
     * 
     * @return a String givin the column name.
     */
    public String getColumnName(int col) {
        return _controller.getLabel("HEADERS."+_headers[col]);
    }

    /**
     * Method returning the number of columns in this table.
     * 
     * @return the number of columns in this table.
     */
    public int getColumnCount() {
        return _headers.length;
    }

    /**
     * Method returning the number of rows in this table.
     * 
     * @return the number of rows in this table.
     */
    public int getRowCount() {
        return Math.min(_data.length - _offset, _maxEntries);
    }

    /**
     * Method returning the content of a given table cell.
     * 
     * @param row
     *            integer giving the row index of the requested cell.
     * @param col
     *            integer giving the column index of the requested cell.
     * 
     * @return the Object located in the table at row \e row and column \e col.
     */
    public Object getValueAt(int row, int col) {
        try {
            return _getters[col].invoke(_data[row + _offset], null);
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public Object getObjectAtRow(int index) {
        return _data[index+_offset];
    }
    /**
     * Method initializing the value of a given table cell.
     * 
     * @param value
     *            Object to initialize the cell with.
     * @param row
     *            row index of the cell to initialize.
     * @param col
     *            column index of the cell to initialize.
     */
    public void SetValueAt(Object value, int row, int col) {
    }

    /**
     * Method indicating whether de value of a cell can be edited.
     * 
     * @param row
     *            row index of the cell to be checked,
     * @param col
     *            column index of the cell to be checked.
     * 
     * @return true if the requested cell can be edited, false otherwise.
     */
    public boolean isCellEditable(int row, int col) {
        return false;
    }

    /**
     * Method fixing the column defining the sort criterion.
     * 
     * @param col
     *            index of the row to be used for sorting.
     */
    void setSortColumn(int col) {
        if (col >= 0) {
            if (col != _sortColumn) {
                _sortColumn = col;
            } else {
                _sortDirection = -_sortDirection;
            }
            sortData();
            computeOffset();
        }
    }

    /**
     * Method performing a bubble sort of the tabular data according to the
     * active sort column and order. This is the currently used method.
     */
    private void sortData() {
        Comparator comparator = null;

        Class sortColumnClass = _getters[_sortColumn].getReturnType();
        if (sortColumnClass == Float.class) {
            if (_sortDirection == SORT_ASC) {
                comparator = (Comparator) _comparators
                        .get(COMPARATOR_FLOAT_ASC);
            } else {
                comparator = (Comparator) _comparators
                        .get(COMPARATOR_FLOAT_DESC);
            }
        } else if (sortColumnClass == Integer.class) {
            if (_sortDirection == SORT_ASC) {
                comparator = (Comparator) _comparators
                        .get(COMPARATOR_INTEGER_ASC);
            } else {
                comparator = (Comparator) _comparators
                        .get(COMPARATOR_INTEGER_DESC);
            }
        } else if (sortColumnClass == String.class) {
            if (_sortDirection == SORT_ASC) {
                comparator = (Comparator) _comparators
                        .get(COMPARATOR_STRING_ASC);
            } else {
                comparator = (Comparator) _comparators
                        .get(COMPARATOR_STRING_DESC);
            }
        }
        Arrays.sort(_data, comparator);
    }

    public void setMaxEntries(int maxEntries) {
        _maxEntries = maxEntries;
    }

    public int findWord(String word) {
        int row=-1;
        boolean found=false;
        int index=0;
        while (found == false && index < _data.length) {
            ElementStats es=(ElementStats)_data[index];
            if (es.getName().equals(word)) {
                found=true;
                row=index;
            }
            index++;
        }
        return row;
    }

    private void computeOffset() {
        _offset = 0;
        if (_focussedWord != null) {
            ElementStats target = new GaussStats();
            target.setName(_focussedWord);
            Comparator nameComparator = new Comparator() {
                public int compare(Object arg0, Object arg1) {
                    ElementStats es0 = (ElementStats) arg0;
                    ElementStats es1 = (ElementStats) arg1;
                    return es0.getName().compareToIgnoreCase(es1.getName());
                }

            };
            int index = Arrays.binarySearch(_data, target, nameComparator);
            if (index >= 0)
                _offset = index;
        }
    }

}
/**
 * This class yields a tabular view of the data. One instance of ResultTable is
 * needed for each of the X and Y datasets.
 * 
 * @author $Author: mhoebeke $
 * @version $Revision: 123 $
 */

public class DataTable extends JTable {
 
    RMESPlotController _controller = null;
    Model _model = null;
    DataTableTableModel _dataTableTableModel = null;
    
    DataTable(RMESPlotController controller, Model model) {
        super();
        _controller=controller;
        _model=model;
        _dataTableTableModel=new DataTableTableModel(_controller,model);
        setModel(_dataTableTableModel);
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        ListSelectionModel lsm=getSelectionModel();
        lsm.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting() == false) {
                    ListSelectionModel lsm=(ListSelectionModel)e.getSource();
                    if (lsm.isSelectionEmpty() == false) {
                        int index=lsm.getMinSelectionIndex();
                        ElementStats es=(ElementStats)_dataTableTableModel.getObjectAtRow(index);
                        _controller.ElementSelected(es,_model);
                    }
                }
            }});
        
        getTableHeader().addMouseListener(
                new DataTableColumnHeaderListener(this));
        
    }

    public void setMaxEntries(int maxEntries) {
        ((DataTableTableModel) getModel()).setMaxEntries(maxEntries);
        revalidate();
    }

    public int findWord(String word) {
        return ((DataTableTableModel) getModel()).findWord(word);
    }

    public void applyFilters() {
        DataTableTableModel model=(DataTableTableModel)getModel();
        model.buildData();
        model.fireTableDataChanged();
        revalidate();
    }
    
    /**
     * Method to save the table data to a file.
     * 
     * @param file
     *            a File to save the data in.
     */
    public void saveToFile(File file) {
        try {
            PrintStream oStream = new PrintStream(new FileOutputStream(file));
            TableModel model = getModel();
            int colCount = model.getColumnCount();
            int rowCount = model.getRowCount();
            StringBuffer line = new StringBuffer();
            for (int i = 0; i < colCount; i++) {
                line.append(model.getColumnName(i));
                if (i < (colCount - 1)) {
                    line.append("\t");
                }
            }
            oStream.println(line);
            for (int r = 0; r < rowCount; r++) {
                line.delete(0, line.length());
                for (int c = 0; c < colCount; c++) {
                    line.append(model.getValueAt(r, c));
                    if (c < (colCount - 1)) {
                        line.append("\t");
                    }
                }
                oStream.println(line);
            }
            oStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}