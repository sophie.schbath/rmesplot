/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.models;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

/**
 * @author mhoebeke
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Alphabet {
    
    static Alphabet _currentAlphabet;
    
    HashSet _tokens;
    char _joker;
    
    protected Alphabet(String tokenString, char joker) {
        _tokens=new HashSet();
        _joker=joker;
        
        for (int c=0;c<tokenString.length();c++) {
            _tokens.add(""+tokenString.charAt(c));
        }
            
    }
    
    public String[] expand(String word) {
        Vector resultVector=new Vector();
        resultVector.add(word);
        boolean  moreJokers=false;
        do {
            moreJokers=false;
            int jokerIndex=-1;
            int i=0;
            for (i=0;i<resultVector.size() && jokerIndex<0;i++)
                jokerIndex=((String)resultVector.get(i)).indexOf(_joker);                    
            
            if (jokerIndex>=0) {
                moreJokers=true;
                String tmpWord=((String)resultVector.get(i-1));
                resultVector.remove(i-1);
                Iterator it=_tokens.iterator();
                while (it.hasNext()) {
                    String token=(String)it.next();
                    String replaceWord=tmpWord.replaceFirst(""+_joker,token);
                   resultVector.add(replaceWord);
                }
               
            }
            
        } while (moreJokers==true);
        int jokerIndex=word.indexOf(_joker);
        
        String[] resultArray=new String[resultVector.size()];
        resultArray=(String[])resultVector.toArray(resultArray);
        return resultArray;
    }
    
    public static void setCurrentAlphabet(Alphabet alphabet) {
        _currentAlphabet=alphabet;
    }
    
    public static Alphabet getCurrentAlphabet() {
        return _currentAlphabet;
    }

}
