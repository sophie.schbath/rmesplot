/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmes.ui;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import fr.ssbgroup.jrmes.JRmes;
import fr.ssbgroup.jrmes.common.ui.StatefulPanel;
import fr.ssbgroup.jrmes.rmes.ui.actions.ChooseOutputFileAction;

@SuppressWarnings("serial")
public class OutputFilePanel extends StatefulPanel {
	
	final int TEXTFIELD_CHARS=40;
	
	RmesRunFrame runFrame=null;
	JRmes controller=null;
	JTextField filefield=null;
	
	public OutputFilePanel(RmesRunFrame runFrame) {
		super();
		this.runFrame=runFrame;
		this.controller=runFrame.getController();
		initPanel(controller);
	}
	
	protected void initPanel(JRmes controller) {		
		setLayout(new BoxLayout(this,BoxLayout.LINE_AXIS));
		setBorder(new TitledBorder(Messages.getString("OutputFilePanel.ouputprefix"))); //$NON-NLS-1$
		JButton seqbtn=new JButton();
		add(seqbtn);
		
		filefield=new JTextField(TEXTFIELD_CHARS);
		filefield.setMaximumSize(filefield.getPreferredSize());
		add(filefield);
		filefield.setText(controller.getOutputFilePrefix());
		
		seqbtn.setAction(new ChooseOutputFileAction(controller,filefield));
	}
	
}
