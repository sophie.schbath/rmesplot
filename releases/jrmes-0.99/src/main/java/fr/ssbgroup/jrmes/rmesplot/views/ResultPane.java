/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.views;

import javax.swing.*;
import java.awt.*;

/**
 * This class joins the JTabbedPane containing all the selected tables and the
 * button which can remove the last selected table.
 * 
 * @author $Author : jguerin $
 * @version $Revision : 1.1 $
 */
public class ResultPane extends JPanel {
    DataTableTabbedPane _rtPane;

    JButton _button;

    /**
     * Creates a ResultPane Object
     * 
     * @param rtPane
     *            the ResultTablePane displaying result tables.
     */
    ResultPane(DataTableTabbedPane rtPane) {
        _rtPane = rtPane;
        _button = new JButton("Reset a table");
        setLayout(new BorderLayout());
        add(_rtPane, BorderLayout.CENTER);
    }
}