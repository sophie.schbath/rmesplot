/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.views;

import javax.swing.JTabbedPane;

import fr.ssbgroup.jrmes.rmesplot.controls.RMESPlotController;

/**
 * This class implementing a TabbedPane provides the switch between PyramidElement
 * display or Plot display.
 * 
 * @author $Author: mhoebeke $
 * @version $Revision: 123 $
 */
public class GraphicsTabbedPane extends JTabbedPane {

    GraphicsTabbedPane(RMESPlotController controller) {
        super(JTabbedPane.TOP);

        PlotPanel plotPanel = new PlotPanel(controller);
        addTab(controller.getLabel("PLOTTAB_TITLE"), plotPanel);

        PyramidPanel pyramidPanel = new PyramidPanel(controller);
        addTab(controller.getLabel("PYRAMIDTAB_TITLE"), pyramidPanel);

    }
}