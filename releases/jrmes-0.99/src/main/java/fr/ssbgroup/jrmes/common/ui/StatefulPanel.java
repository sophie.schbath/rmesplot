/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.common.ui;

import javax.swing.JPanel;

/**
 * StatefulPanel adds a method to JPanel allowing to check for the validity of its contents.
 * 
 * A StatefulPanel is designed to wrap a small number of related widgets, who, taken together define if the state
 * of the panel is valid, and can return this information to interested parties.
 * 
 * A trivial example of StatefulPanel is an InputPanel containing a button (opening a file selection dialog) and a textfield.
 * This InputPanel is in a valid state if the currently defined input file is readable.
 * 
 * @author mhoebeke
 *
 */
@SuppressWarnings("serial")
public class StatefulPanel extends JPanel {
	/**
	 * Returns the validity of the panel.
	 * @return true if the state of the panel, as defined by the widget it contains can be considered valid.
	 */
	public boolean isStateValid() {
		return true;
	}

}
