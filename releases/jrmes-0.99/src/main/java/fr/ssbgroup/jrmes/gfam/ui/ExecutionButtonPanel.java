/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.gfam.ui;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import org.apache.log4j.Logger;

import fr.ssbgroup.jrmes.JRmes;
import fr.ssbgroup.jrmes.common.core.ProcessRunner;
import fr.ssbgroup.jrmes.common.core.ProcessWatcher;
import fr.ssbgroup.jrmes.gfam.ui.actions.GfamRunAction;

@SuppressWarnings("serial")
public class ExecutionButtonPanel extends JPanel implements ProcessWatcher  {

	private static final Logger logger=Logger.getLogger("fr.ssbgroup.jrmes"); //$NON-NLS-1$
	
	JRmes controller;
	JProgressBar progressBar;
	
	public ExecutionButtonPanel(JRmes controller) {
		super();
		this.controller=controller;
		initPanel();		
		controller.setGfamWatcher(this);
	}
	
	protected void initPanel() {
		
		setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
		setBorder(BorderFactory.createTitledBorder(Messages.getString("ExecutionButtonPanel.execution"))); //$NON-NLS-1$
		
		JPanel btnPanel=new JPanel();
		btnPanel.setLayout(new BoxLayout(btnPanel,BoxLayout.LINE_AXIS));
		add(btnPanel);
		JButton btn=new JButton();
		btnPanel.add(btn);
		btn.setAction(new GfamRunAction(controller));
		btn.setAlignmentX(LEFT_ALIGNMENT);
	
		JPanel progressPanel=new JPanel();
		progressPanel.setLayout(new BoxLayout(progressPanel,BoxLayout.LINE_AXIS));
		add(progressPanel);
		
		progressBar=new JProgressBar();
		progressBar.setStringPainted(true);
		progressPanel.add(progressBar);
		progressBar.setEnabled(false);
		progressBar.setString("");
		
	}

	public void processFailed(ProcessRunner launcher, String message) {
		progressBar.setIndeterminate(false);
		progressBar.setEnabled(false);
		progressBar.setString(""); //$NON-NLS-1$
		logger.error(message);
		JOptionPane.showMessageDialog(null,Messages.getString("ExecutionButtonPanel.error"),Messages.getString("ExecutionButtonPanel.status"),JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
	}

	public void processFinished(ProcessRunner launcher) {
		progressBar.setIndeterminate(false);
		progressBar.setEnabled(false);
		progressBar.setString(""); //$NON-NLS-1$
		logger.info(Messages.getString("ExecutionButtonPanel.success")); //$NON-NLS-1$
		JOptionPane.showMessageDialog(null,Messages.getString("ExecutionButtonPanel.success"),Messages.getString("ExecutionButtonPanel.status"),JOptionPane.INFORMATION_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
	
	}

	public void processStarted(ProcessRunner launcher) {
		progressBar.setString(Messages.getString("ExecutionButtonPanel.running")); //$NON-NLS-1$
		progressBar.setIndeterminate(true);

	}

	public void processPercentProgressed(ProcessRunner launcher,
			double percentage) {
		/**
		 * Empty method : rmes.gfam is not capable of reporting its progress.
		 */
		
	}

	}
