/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmes.ui;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.TitledBorder;

import fr.ssbgroup.jrmes.JRmes;
import fr.ssbgroup.jrmes.common.ui.StatefulPanel;
import fr.ssbgroup.jrmes.rmes.ui.actions.SelectMainFunctionAction;
import fr.ssbgroup.jrmes.rmes.ui.actions.SelectModelAction;

@SuppressWarnings("serial")
public class MainFunctionPanel extends StatefulPanel {
	
	final int TEXTFIELD_CHARS=40;
		
	RmesRunFrame runFrame=null;
	JRmes controller=null;
	
	ButtonGroup mainFunctionBtnGroup=null;
	
	JComboBox modelBox=null;
	int poissonIndex=-1;
	
	CompareSequenceFilePanel compSeqPanel=null;
	String currentMainFunction=null;
	
	public MainFunctionPanel(RmesRunFrame runFrame) {
		super();
		this.runFrame=runFrame;
		this.controller=runFrame.getController();
		initMainFunctionPanel();
		
	}

	protected void initMainFunctionPanel() {		
		mainFunctionBtnGroup=new ButtonGroup();
		
		setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
		
		TitledBorder pBorder=new TitledBorder(Messages.getString("MainFunctionPanel.maintask")); //$NON-NLS-1$
		setBorder(pBorder);
		for (String functionName : controller.getMainFunctions()) {
			JRadioButton btn=new JRadioButton();
			btn.setAction(new SelectMainFunctionAction(controller, functionName,this));
			mainFunctionBtnGroup.add(btn);
			if (functionName.equals(JRmes.FUNCTION_STATS)) {
				btn.setSelected(true);
				controller.setMainFunction(functionName);
				JPanel modelPanel=new JPanel();
				modelPanel.setLayout(new BoxLayout(modelPanel,BoxLayout.LINE_AXIS));
				modelPanel.setAlignmentX(LEFT_ALIGNMENT);
				modelPanel.add(btn);
				modelBox=new JComboBox(controller.getApproximationMethods());
				modelBox.setAlignmentX(RIGHT_ALIGNMENT);
				modelBox.addActionListener(new SelectModelAction(controller));
				modelPanel.add(modelBox);
				controller.setApproximationMethod(controller.getApproximationMethods()[0]);
				add(modelPanel);
			} else if (functionName.equals(JRmes.FUNCTION_COMPARE)){
				if (controller.isCompareEnabled()==true) {
					JPanel seqPanel=new JPanel();
					seqPanel.setLayout(new BoxLayout(seqPanel,BoxLayout.LINE_AXIS));
					seqPanel.setAlignmentX(LEFT_ALIGNMENT);
					seqPanel.add(btn);
					compSeqPanel=new CompareSequenceFilePanel(this.runFrame);
					compSeqPanel.setEnabled(false);
					seqPanel.add(compSeqPanel);
					add(seqPanel);
				}
			} else {
				add(btn);
			}
		}
	}
	
	public void setMainFunction(String mainFunction) {
		currentMainFunction=mainFunction;
		if (mainFunction.equals(JRmes.FUNCTION_STATS)) {
			modelBox.setEnabled(true);
			compSeqPanel.setEnabled(false);
		}
		if (mainFunction.equals(JRmes.FUNCTION_SKEW)) {
			modelBox.setEnabled(false);
			compSeqPanel.setEnabled(false);
		}
		if (mainFunction.equals(JRmes.FUNCTION_COMPARE)) {
			modelBox.setEnabled(false);
			compSeqPanel.setEnabled(true);
		}
		
		runFrame.setPanelValidState(this,isStateValid());
	}
	
	public void setEnablePoisson(boolean enabled) {
		if (enabled==false) {
			modelBox.removeItem(JRmes.METHOD_POISSON);
		} else {
			modelBox.addItem(JRmes.METHOD_POISSON);
		}
	}
	
	@Override
	public boolean isStateValid() {
		boolean valid=true;
		if (currentMainFunction==JRmes.FUNCTION_COMPARE && (compSeqPanel.isStateValid() == false))
			valid=false;
		return valid;
	}
}
