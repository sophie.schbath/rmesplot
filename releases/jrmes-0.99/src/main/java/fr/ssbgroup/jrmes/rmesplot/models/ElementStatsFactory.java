/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.models;

import java.util.*;

/**
 * This factory class has the capability of building instances of one of the
 * subclasses of ElementStats depending on the name of the algorithm contained
 * in the R'MES result file. The algorithms known are : Cond_as, Mart, Mart_r,
 * Cond_as_pc, Cond_as_p.
 * 
 * @author $Author: mhoebeke $
 * @version $Revision: 123 $
 */
public class ElementStatsFactory {
    private static Hashtable<String,Class> _elementTypes;

    static {
    	/* 
    	 * R'MES 3 identifiers.
    	 */
    	 _elementTypes = new Hashtable<String,Class>();
         _elementTypes.put("Cond_as",GaussStats.class);
         _elementTypes.put("Mart", GaussStats.class);
         _elementTypes.put("Mart_r", GaussStats.class);
         _elementTypes.put("Cond_as_pc", PoissonCompStats.class);
         _elementTypes.put("Cond_as_p", PoissonStats.class);
         _elementTypes.put("Exat", ExactStats.class);
         
         /*
          * R'MES 4 identifiers.
          */
         _elementTypes.put("Gauss",GaussStats.class);
         _elementTypes.put("Compound Poisson", PoissonCompStats.class);
         _elementTypes.put("Poisson", PoissonStats.class);
    }
    
    /**
     * Creates a new ElementStatsFactory object.
     */
    public ElementStatsFactory() {
       
    }

    /**
     * Returns an instance on one of the subclasses of ElementStats.
     * 
     * @param algorithm
     *            a String denoting the type of algorithm used by R'MES.
     * 
     * @return an instance of one of the subclasses of ElementStats.
     * 
     * @throws UnknownAlgorithmException
     *             raised of the String given as argument does not match any of
     *             the known algorithms.
     */
    public ElementStats getInstance(String algorithm)
            throws UnknownAlgorithmException {
        ElementStats res = null;
        Class<ElementStats> statsClass= (Class<ElementStats>)_elementTypes.get(algorithm);
        if (statsClass != null) {
            try {
                res = (ElementStats) statsClass.newInstance();
            } catch (InstantiationException ie) {
                throw new UnknownAlgorithmException(algorithm);
            } catch (IllegalAccessException ie) {
                throw new UnknownAlgorithmException(algorithm);
            }
        }

        return res;
    }
}