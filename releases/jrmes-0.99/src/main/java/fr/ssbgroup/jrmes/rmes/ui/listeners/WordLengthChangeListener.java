/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmes.ui.listeners;

import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fr.ssbgroup.jrmes.JRmes;

public class WordLengthChangeListener implements ChangeListener {

	JRmes controller=null;
	JSpinner minSpinner=null;
	JSpinner maxSpinner=null;
	
	public WordLengthChangeListener(JRmes controller, JSpinner minSpinner, JSpinner maxSpinner) {
		this.controller=controller;
		this.minSpinner=minSpinner;
		this.maxSpinner=maxSpinner;
		
	}
	
	public void stateChanged(ChangeEvent evt) {
		JSpinner spinner=(JSpinner)evt.getSource();
		Integer wordLength=(Integer)spinner.getValue();
		int markovorder=controller.getMarkovOrder();
		if (markovorder<=wordLength-2) { 
			if (spinner==minSpinner)
				controller.setMinWordLength(wordLength);
			if (spinner==maxSpinner)
				controller.setMaxWordLength(wordLength);
		} else {
			JOptionPane.showMessageDialog(null,Messages.getString("WordLengthChangeListener.wronglength")+(markovorder+2)+Messages.getString("WordLengthChangeListener.markovorder")+markovorder+".",Messages.getString("WordLengthChangeListener.title"),JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			spinner.setValue(markovorder+2);
			
		}
		if (spinner==minSpinner) {
			Integer maxWordLength=(Integer)maxSpinner.getValue();
			if (maxWordLength<wordLength) {
				maxWordLength=wordLength;
				maxSpinner.setValue(maxWordLength);
				controller.setMaxWordLength(wordLength);
			}
		}
		if (spinner==maxSpinner) {
			Integer minWordLength=(Integer)minSpinner.getValue();
			if (minWordLength>wordLength) {
				minWordLength=wordLength;
				minSpinner.setValue(minWordLength);
				controller.setMinWordLength(wordLength);
			}
		}	
	}

}
