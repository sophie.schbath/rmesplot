/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.views;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import fr.ssbgroup.jrmes.main.ui.AboutDialog;
import fr.ssbgroup.jrmes.rmesplot.controls.RMESPlotController;

/**
 * @author mark
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class RMESPlotMenuBar extends JMenuBar {

    RMESPlotController _controller;

    AboutDialog _aboutDialog;

    public RMESPlotMenuBar(RMESPlotController controller) {
        super();

        _controller = controller;
        JMenu menu = new JMenu(_controller.getLabel("FILE_MENU"));
        add(menu);

        AbstractAction action = new AbstractAction("Load Files") {
            public void actionPerformed(ActionEvent evt) {	
            	JFileChooser fileChooser=new JFileChooser();
            	fileChooser.setMultiSelectionEnabled(true);
            	fileChooser.setCurrentDirectory(_controller.getCurrentDir());
                int retval = fileChooser.showOpenDialog(null);
                if (retval == JFileChooser.APPROVE_OPTION) {
                    File[] files = fileChooser.getSelectedFiles();
                    _controller.setCurrentDir(fileChooser.getCurrentDirectory());
                    _controller.loadFiles(files);
                }
            }
        };

        JMenuItem item = new JMenuItem(action);
        item.setText(_controller.getLabel("LOAD_MENUITEM"));
        menu.add(item);


        action=new AbstractAction("Delete Result") {
        	public void actionPerformed(ActionEvent evt) {
        		_controller.deleteResults();
        	}
        };
        
        item=new JMenuItem(action);
        item.setText(_controller.getLabel("DEL_MENUITEM"));
        menu.add(item);
        
        menu.addSeparator();

        action = new AbstractAction("Quit") {
            public void actionPerformed(ActionEvent evt) {
                _controller.quit();
            }
        };

        item = new JMenuItem(action);
        item.setText(_controller.getLabel("QUIT_MENUITEM"));
        menu.add(item);

        menu=new JMenu(_controller.getLabel("OPTIONS_MENU"));
        add(menu);
        
        action = new AbstractAction("Filtering") {
          public void actionPerformed(ActionEvent evt) {
              _controller.showFilterDialog();
          }
        };
        item = new JMenuItem(action);
        item.setText(_controller.getLabel("FILTERS_MENUITEM"));
        menu.add(item);
  
    }
}