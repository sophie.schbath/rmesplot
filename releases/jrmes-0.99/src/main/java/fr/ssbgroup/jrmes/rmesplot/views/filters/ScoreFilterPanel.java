/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.views.filters;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.ssbgroup.jrmes.rmesplot.controls.RMESPlotController;
import fr.ssbgroup.jrmes.rmesplot.models.filters.ElementFilter;
import fr.ssbgroup.jrmes.rmesplot.models.filters.ScoreFilter;

/**
 * @author mhoebeke
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ScoreFilterPanel extends FilterPanel {
    
    private static final int TEXTFIELD_MAXCOLUMNS = 8;
    
    private JTextField _textField;
    
    public ScoreFilterPanel(RMESPlotController controller) {
        super(controller,controller.getLabel("STATFILTERBORDER_TITLE"));
        
        JPanel panel=new JPanel();
        add(panel,BorderLayout.CENTER);
        
        JLabel label=new JLabel(_controller.getLabel("STATFILTER_TEXTFIELD"));
        panel.add(label);
        
        _textField=new JTextField(TEXTFIELD_MAXCOLUMNS);
        _textField.setText("0.0");
        panel.add(_textField);
    }

    /* (non-Javadoc)
     * @see rmes.views.filters.FilterPanel#getFilter()
     */
    public ElementFilter getFilter() {
        float threshold=Float.parseFloat(_textField.getText());
        return new ScoreFilter(threshold);
    }

}
