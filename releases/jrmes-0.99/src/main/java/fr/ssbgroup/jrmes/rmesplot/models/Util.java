/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.models;

/**
 * Class with utility routines used throughout the package.
 * 
 * @author $Author: mhoebeke $
 * @version $Revision: 123 $
 */
public class Util {
	
    private static final String[] DNAAlphabet = { "a", "g", "c", "t" };
    
    private static String[] _alphabet = DNAAlphabet;

    private static int _alphabetLength = DNAAlphabet.length;

    /**
     * Method converting a word referenced by an integer into its stringified
     * equivalent. This method takes an integer as input and produces a string
     * resulting from its conversion into the DNA alphabet. The length of the
     * resulting string is given as second argument. It is used when
     * sequentially reading through an R'MES result file in which each line
     * contains results for a given word. The 4 letter code being orderd as
     * follows : a, g, c, t.
     * 
     * @param word
     *            an integer to be converted into a String.
     * @param wordlength
     *            the total length of the String to be generated.
     * 
     * @return a String resulting from the conversion of the integer given as
     *         argument.
     */
    public static String intToWord(int word, int wordlength) {
        StringBuffer wordString = new StringBuffer();

        for (int i = 0; i < wordlength; i++) {
            wordString.append(_alphabet[word % _alphabetLength]);
            word /= _alphabetLength;
        }

        return wordString.reverse().toString();
    }
    
    public static void setAlphabet(String[] alphabet) {
    	_alphabet=alphabet;
    	_alphabetLength=_alphabet.length;
    }
    
    public static int nWords(int l) {
    		return (new Double(Math.pow(_alphabetLength,l)).intValue());
    }
}