/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.readers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Hashtable;

import fr.ssbgroup.jrmes.rmesplot.models.UnknownResultFormatException;

public class ResultReaderFactory {
	
	private Hashtable<String,Class> readers;
	
	public ResultReaderFactory() {
		readers=new Hashtable<String, Class>();
		readers.put("# rmes-3",Rmes3ResultReader.class);
		readers.put("Analysis performed with: rmes-4.",Rmes4ResultReader.class);
	}
	
	public ResultReader buildResultReader(URL resultURL) throws UnknownResultFormatException {
		ResultReader reader=null;
		
		try {
			BufferedReader br=new BufferedReader(new InputStreamReader(resultURL.openStream()));
			String headerline=br.readLine();
			for (String resultId : readers.keySet()) {
				if (headerline.startsWith(resultId)) {
					Class readerClass=readers.get(resultId);
					reader=(ResultReader)readerClass.newInstance();
				}
			}
			if (reader==null) {
				throw new UnknownResultFormatException(headerline);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return reader; 
	}

}
