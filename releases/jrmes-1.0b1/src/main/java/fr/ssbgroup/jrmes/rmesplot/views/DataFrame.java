/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.views;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import fr.ssbgroup.jrmes.rmesplot.controls.RMESPlotController;

/**
 * @author mark
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class DataFrame extends JFrame {

    RMESPlotController _controller;

    ResultTree _tree;

    DataTableTabbedPane _dataTableTabbedPane;

    public DataFrame(final RMESPlotController controller) {

        _controller = controller;

        setTitle(_controller.getLabel("DATAFRAME_TITLE"));
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                controller.quit();
            }
        });

        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        RMESPlotToolBar toolbar = new RMESPlotToolBar(_controller);
        contentPane.add(toolbar,BorderLayout.NORTH);


        /**
         * Split left part vertically : - on top : result tree, - at the bottom :
         * tables.
         */
        JSplitPane vsplitpane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        contentPane.add(vsplitpane,BorderLayout.CENTER);
        _tree = new ResultTree(_controller);
        JScrollPane treePane = new JScrollPane(_tree);
        treePane
                .setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        treePane
                .setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        treePane.setPreferredSize(new Dimension(480, 200));
        vsplitpane.setTopComponent(treePane);

        _dataTableTabbedPane = new DataTableTabbedPane(_controller);
        vsplitpane.setBottomComponent(_dataTableTabbedPane);

        
        vsplitpane.setPreferredSize(new Dimension(800, 600));
        pack();
        setVisible(true);

    }

    public ResultTree getResultTree() {
        return _tree;
    }

    public DataTableTabbedPane getTabbedPane() {
        return _dataTableTabbedPane;
    }
}