/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.main.ui;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import fr.ssbgroup.jrmes.main.ui.actions.GfamShowRunFrameAction;
import fr.ssbgroup.jrmes.main.ui.actions.RmesShowRmesPlotFrameAction;
import fr.ssbgroup.jrmes.main.ui.actions.RmesShowRunFrameAction;



/**
 * {@link ToolBarPanel} builds the tool bar at the top of the main window, below the menu bar.
 * 
 * The tool bar shows the three buttons giving access to the three main parts of JR'MES : 
 * <ul>
 * <li>Running R'MES.
 * <li>Exploring results generated with R'MES.
 * <li>Generating family files.
 * </ul> 
 * 
 */
@SuppressWarnings("serial")
public class ToolBarPanel extends JPanel {
	
	private MainFrame mainFrame=null;
	private JButton rmesButton=null;
	private JButton gfamButton=null; 
	
	public ToolBarPanel(MainFrame mainFrame) {
		this.mainFrame=mainFrame;
		initUI();
	}
	
	private void  initUI() {
		
		setLayout(new BorderLayout());
		
		JToolBar tBar=new JToolBar();
		add(tBar,BorderLayout.PAGE_START);
		
		rmesButton=new JButton(new RmesShowRunFrameAction(mainFrame));
		rmesButton.setEnabled(false); /* Disable the run button by default. It will be enabled when an R'MES executable is found. */
		tBar.add(rmesButton);
		
		gfamButton=new JButton(new GfamShowRunFrameAction(mainFrame));
		gfamButton.setEnabled(false);
		tBar.add(gfamButton);
		
		JButton btn=new JButton(new RmesShowRmesPlotFrameAction(mainFrame));
		tBar.add(btn);
		
		
	}

	/**
	 * Defines whether R'MES can be run.
	 * 
	 * Typically called when the user configures the location of the R'MES executables.
	 * 
	 * @param enabled if true, the user can access the button allowing her to define how to run R'MES and to perform the actual runs.
	 * @see MainFrame
	 */
	public void setRmesButtonEnabled(boolean enabled) {
		rmesButton.setEnabled(enabled);
		
	}
	
	/**
	 * Defines whether family generation can be run.
	 * 
	 * Typically called when the user configures the location of the R'MES executables.
	 * 
	 * @param enabled if true, the user can access the button allowing her to define the characteristics of the families to generate, and to perform the actual generation with "rmes.fgam"
	 * @see MainFrame
	 */
	public void setGfamButtonEnabled(boolean enabled) {
		gfamButton.setEnabled(enabled);
		
	}
}
