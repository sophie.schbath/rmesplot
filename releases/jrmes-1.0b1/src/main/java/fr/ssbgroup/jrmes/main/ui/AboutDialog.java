/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.main.ui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

/**
 * Defines the contents of the "About" dialog window.
 * 
 * @author mhoebeke
 */
@SuppressWarnings("serial")
public class AboutDialog extends JDialog {
	
	/**
	 * Constructor for the About Dialog.
	 * 
	 * All properties and resources are loaded in the constructor. Users of this class only
	 * have to call the {@link JDialog#setVisible(boolean)} method to make it appear/disappear.
	 * 
	 */
    public AboutDialog() {
        super((JFrame)null,Messages.getString("AboutDialog.title"),true);
        setPreferredSize(new Dimension(600,300));
        Container contentPane=getContentPane();
        contentPane.setLayout(new BorderLayout());
        
        String localizedAbout=Messages.getString("AboutDialog.htmlfile");
        URL aboutURL=this.getClass().getResource("/fr/ssbgroup/jrmes/rmesplot/"+localizedAbout);
    
        JEditorPane textPane=new JEditorPane();
        textPane.setEditable(false);
        try {
			textPane.setPage(aboutURL);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
        
		JScrollPane scrollPane=new JScrollPane(textPane);
        contentPane.add(scrollPane,BorderLayout.CENTER);
 
        JButton button=new JButton(Messages.getString("AboutDialog.OK"));
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                AboutDialog.this.setVisible(false);
            }
        });
        contentPane.add(button,BorderLayout.SOUTH);
        pack(); 
    }

}
