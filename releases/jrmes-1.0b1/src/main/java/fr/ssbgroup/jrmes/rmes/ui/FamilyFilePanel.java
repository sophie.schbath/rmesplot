/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmes.ui;

import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.ssbgroup.jrmes.JRmes;
import fr.ssbgroup.jrmes.rmes.ui.actions.ChooseFamilyFileAction;
import fr.ssbgroup.jrmes.rmes.ui.inputverifiers.FamilyFileInputVerifier;

@SuppressWarnings("serial")
public class FamilyFilePanel extends JPanel {
	
	final int TEXTFIELD_CHARS=40;
	
	RmesRunFrame runFrame=null;
	JRmes controller=null;
	File sequencefile=null;
	
	JButton seqbtn=null;
	JTextField filefield=null;
	
	public FamilyFilePanel(RmesRunFrame runFrame) {
		super();
		this.runFrame=runFrame;
		this.controller=runFrame.getController();
		initPanel(controller);
	}
	
	protected void initPanel(JRmes controller) {
		
		setLayout(new BoxLayout(this,BoxLayout.LINE_AXIS));
		seqbtn=new JButton();
		filefield=new JTextField(TEXTFIELD_CHARS);
		
		
		seqbtn.setAction(new ChooseFamilyFileAction(controller,filefield));
		add(seqbtn);
		
		add(filefield);
//		filefield.addActionListener(new FamilyTextFieldChangeListener(controller));
		filefield.setInputVerifier(new FamilyFileInputVerifier(controller));
		
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		seqbtn.setEnabled(enabled);
		filefield.setEnabled(enabled);
	}
	
	

}
