/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.views.filters;

import java.awt.GridLayout;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import javax.swing.JPanel;

import fr.ssbgroup.jrmes.rmesplot.controls.RMESPlotController;

/**
 * @author mhoebeke
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MultiFilterPanel extends JPanel {
    
    private static final int N_FILTERS=4;
    
    private RMESPlotController _controller = null;
    
    private HashSet _filterPanels = null;
    
    public MultiFilterPanel(RMESPlotController controller) {
        super();
        _controller=controller;
        _filterPanels=new HashSet();
        
        setLayout(new GridLayout(N_FILTERS,1));
        
        FilterPanel filterPanel=new ScoreFilterPanel(_controller);
        add(filterPanel);
        _filterPanels.add(filterPanel);
        
        filterPanel=new WordFilterPanel(_controller);
        add(filterPanel);
        _filterPanels.add(filterPanel);
        
        filterPanel=new ObservedFilterPanel(_controller);
        add(filterPanel);
        _filterPanels.add(filterPanel);

        filterPanel=new ExpectationFilterPanel(_controller);
        add(filterPanel);
        _filterPanels.add(filterPanel);
    }
    
    public Collection getActiveFilters() {
        HashSet filters=new HashSet();
        Iterator it=_filterPanels.iterator();
        while (it.hasNext()) {
            FilterPanel filterPanel=(FilterPanel)it.next();
            if (filterPanel.isActive()) {
                filters.add(filterPanel.getFilter());
            }
        }
        return filters;
    }

}
