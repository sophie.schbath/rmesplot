/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmes.ui;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

import fr.ssbgroup.jrmes.JRmes;
import fr.ssbgroup.jrmes.rmes.ui.listeners.WordLengthChangeListener;

@SuppressWarnings("serial")
public class WordLengthPanel extends JPanel {

	final static int DEFAULT_WORD_LENGTH=6;
	
	RmesRunFrame runFrame=null;
	JRmes controller=null;
	
	JLabel minlabel=null;
	JSpinner minSpinner=null;
	
	JLabel maxlabel=null;
	JSpinner maxSpinner=null;
	
	public WordLengthPanel(RmesRunFrame runFrame) {
		super();
		this.runFrame=runFrame;
		this.controller=runFrame.getController();
		initPanel(controller);
	}
	
	protected void initPanel(JRmes controller) {	
		setLayout(new BoxLayout(this,BoxLayout.LINE_AXIS));
		setAlignmentX(LEFT_ALIGNMENT);
		
		minlabel=new JLabel();
		minlabel.setText(Messages.getString("WordLengthPanel.minlength")); //$NON-NLS-1$
		add(minlabel);

		SpinnerModel lModel=new SpinnerNumberModel(DEFAULT_WORD_LENGTH,1,100,1);
		controller.setMinWordLength(DEFAULT_WORD_LENGTH);
		minSpinner=new JSpinner(lModel);
		add(minSpinner);
		

		maxlabel=new JLabel();
		maxlabel.setText(Messages.getString("WordLengthPanel.maxlength")); //$NON-NLS-1$
		add(maxlabel);

		lModel=new SpinnerNumberModel(DEFAULT_WORD_LENGTH,1,100,1);
		controller.setMaxWordLength(DEFAULT_WORD_LENGTH);
		maxSpinner=new JSpinner(lModel);
		add(maxSpinner);

		WordLengthChangeListener wlListener=new WordLengthChangeListener(controller, minSpinner, maxSpinner);
		minSpinner.addChangeListener(wlListener);
		maxSpinner.addChangeListener(wlListener);

	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		minlabel.setEnabled(enabled);
		minSpinner.setEnabled(enabled);
		maxlabel.setEnabled(enabled);
		maxSpinner.setEnabled(enabled);
	}
	

}
