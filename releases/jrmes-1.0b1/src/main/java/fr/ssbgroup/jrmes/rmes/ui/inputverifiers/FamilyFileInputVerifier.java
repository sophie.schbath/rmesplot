/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmes.ui.inputverifiers;

import java.io.File;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import fr.ssbgroup.jrmes.JRmes;

public class FamilyFileInputVerifier extends InputVerifier {

	JRmes controller=null;

	public FamilyFileInputVerifier(JRmes controller) {
		this.controller=controller;
	}

	@Override
	public boolean verify(JComponent input) {
		JTextField tField=(JTextField)input;
		String contents=tField.getText();
		if (contents.trim().length()>0) {
			File familyFile=new File(contents);
			if (familyFile.canRead()) {
				controller.setFamilyFile(familyFile);
			} else {
				JOptionPane.showMessageDialog(null,Messages.getString("FamilyFileInputVerifier.cantread")+familyFile.getPath(),Messages.getString("FamilyFileInputVerifier.invalid"),JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
				tField.setText(""); //$NON-NLS-1$
			}
		}
		return true;
	}
}
