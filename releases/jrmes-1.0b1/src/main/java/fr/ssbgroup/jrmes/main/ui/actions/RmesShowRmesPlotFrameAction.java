/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.main.ui.actions;
/**
 * This file is part of JRmes.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) : Mark Hoebeke
 *
 * JRmes is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * JRmes is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JRmes; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 **/
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import fr.ssbgroup.jrmes.main.ui.MainFrame;
import fr.ssbgroup.jrmes.util.EmbeddedRmesPlotController;

@SuppressWarnings("serial")
public class RmesShowRmesPlotFrameAction extends AbstractAction  {

	MainFrame mainFrame=null;
	
	public RmesShowRmesPlotFrameAction(MainFrame mainFrame) {
		super(Messages.getString("RmesShowRmesPlotFrameAction.examineresults")); //$NON-NLS-1$
		this.mainFrame=mainFrame;
		
	}
	
	public void actionPerformed(ActionEvent evt) {
		
		EmbeddedRmesPlotController erpc=(EmbeddedRmesPlotController)mainFrame.getRmesPlotController();
		if (erpc==null) {
			erpc=new EmbeddedRmesPlotController();
			mainFrame.setRmesPlotController(erpc);
		}
		erpc.setVisible(true);
	}
}