/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.main.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.border.TitledBorder;

import org.apache.log4j.Logger;

import fr.ssbgroup.jrmes.JRmes;
import fr.ssbgroup.jrmes.gfam.ui.GfamRunFrame;
import fr.ssbgroup.jrmes.main.ui.actions.ClearLogAction;
import fr.ssbgroup.jrmes.rmes.ui.RmesRunFrame;
import fr.ssbgroup.jrmes.rmesplot.controls.RMESPlotController;
import fr.ssbgroup.jrmes.util.TextAreaAppender;

@SuppressWarnings("serial")
public class MainFrame extends JFrame {
	
	final static Logger logger=Logger.getLogger("fr.ssbgroup.jrmes");

	JRmes controller;
	RmesRunFrame runFrame=null;
	RMESPlotController rmesPlotController=null;
	GfamRunFrame gfamRunFrame=null;
	ToolBarPanel tbPanel=null;

	public GfamRunFrame getGfamRunFrame() {
		return gfamRunFrame;
	}

	public void setGfamRunFrame(GfamRunFrame gfamRunFrame) {
		this.gfamRunFrame = gfamRunFrame;
	}

	public RMESPlotController getRmesPlotController() {
		return rmesPlotController;
	}

	public void setRmesPlotController(RMESPlotController rmesPlotController) {
		this.rmesPlotController = rmesPlotController;
	}

	public JRmes getJRmesController() {
		return this.controller;
	}
	
	public MainFrame(JRmes controller) {
		super();
		this.controller=controller;
		initUi(controller);
		
	}
	
	public RmesRunFrame getRmesRunFrame() {
		return this.runFrame;
		
	}
	
	public void setRmesRunFrame(RmesRunFrame runFrame) {
		this.runFrame=runFrame;
	}
	
	
	
	public JRmes getController() {
		return controller;
	}
	
	protected void initUi(JRmes controller) {
		
		setDefaultLookAndFeelDecorated(true);
		setTitle(Messages.getString("MainFrame.frametitle")); //$NON-NLS-1$
	
		MenuBar menuBar=new MenuBar(controller);
		setJMenuBar(menuBar);
		
		java.awt.Container cPane=this.getContentPane();
		
		cPane.setLayout(new BorderLayout());
		
		tbPanel=new ToolBarPanel(this);
		cPane.add(tbPanel,BorderLayout.PAGE_START);
			
		JPanel logPanel=new JPanel();
		logPanel.setLayout(new BorderLayout());
		cPane.add(logPanel,BorderLayout.CENTER);
		
		TitledBorder tBorder=new TitledBorder(Messages.getString("MainFrame.logtitle")); //$NON-NLS-1$
		logPanel.setBorder(tBorder);
		
		JTextPane logPane=new JTextPane();
		TextAreaAppender appender=(TextAreaAppender)logger.getAppender("jrmesappender");
		appender.setTextPane(logPane);
		JScrollPane logScrollPane=new JScrollPane(logPane);
		logPanel.add(logScrollPane,BorderLayout.CENTER);
		
		JButton clearBtn=new JButton(new ClearLogAction(appender));
		logPanel.add(clearBtn,BorderLayout.SOUTH);
	
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setPreferredSize(new Dimension(640,480));
		
		pack();
		setVisible(true);
		
	}

	public void setRmesConfigured(boolean rmesConfigured) {
		tbPanel.setRmesButtonEnabled(rmesConfigured);
		if (rmesConfigured==false && runFrame!=null)
			runFrame.setVisible(false);
	}

	public void setGfamConfigured(boolean gfamConfigured) {
		tbPanel.setGfamButtonEnabled(gfamConfigured);
		if (gfamConfigured==false && gfamRunFrame!=null)
			gfamRunFrame.setVisible(false);
		
	}
}
