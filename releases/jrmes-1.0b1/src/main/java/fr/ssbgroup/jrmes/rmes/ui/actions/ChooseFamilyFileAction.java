/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmes.ui.actions;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JTextField;

import fr.ssbgroup.jrmes.JRmes;

@SuppressWarnings("serial")
public class ChooseFamilyFileAction extends AbstractAction {

	JRmes controller=null;
	JFileChooser chooser=null;
	JTextField filenamefield=null;
	
	File curdir=null;
	
	public ChooseFamilyFileAction(JRmes controller, JTextField filenameField) {
		super(Messages.getString("ChooseFamilyFileAction.selectfamilyfile")); //$NON-NLS-1$
		this.controller=controller;
		this.curdir=new File(System.getProperty("user.dir")); //$NON-NLS-1$
		this.chooser=new JFileChooser();
		this.filenamefield=filenameField;
	}
	
	public void actionPerformed(ActionEvent arg0) {	
		chooser.setCurrentDirectory(curdir);
		int retval=chooser.showOpenDialog(null);
		if (retval == JFileChooser.APPROVE_OPTION) {
			File seqFile=chooser.getSelectedFile();
			curdir=chooser.getCurrentDirectory();
			controller.setFamilyFile(seqFile);
			try {
				filenamefield.setText(seqFile.getCanonicalPath());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
