/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;

import javax.swing.JDialog;
import javax.swing.JLabel;

import fr.ssbgroup.jrmes.rmesplot.controls.RMESPlotController;

/**
 * @author mhoebeke
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class WaitDialog extends JDialog {
    
    JLabel _label = null;
    public WaitDialog(RMESPlotController controller) {
        super(controller.getDataFrame(),"RMESPlot : "+controller.getLabel("WAITDIALOG_TITLE"),false);
        _label=new JLabel(controller.getLabel("WAITDIALOG_DEFAULT_MESSAGE"));
        Container contentPane=getContentPane();
        contentPane.setLayout(new BorderLayout());
        contentPane.setBackground(Color.white);
        contentPane.setForeground(Color.white);
        contentPane.add(_label,BorderLayout.CENTER);
        pack();
    }
    
    
    public void setMessage(String message) {
        _label.setText(message);
        pack();
    }

}
