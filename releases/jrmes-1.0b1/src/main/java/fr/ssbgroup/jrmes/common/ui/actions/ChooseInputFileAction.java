/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.common.ui.actions;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;

import fr.ssbgroup.jrmes.common.ui.InputFilePanel;

/**
 * ChooseInputFileAction implements an AbstractAction called when the user activates a file selection button in an {@link InputFilePanel}.
 * 
 * The sole purpose of this class is to open a file dialog allowing the selection of an (already existing) input file and to
 * give feedback to the InputFilePanel about the file that was actually selected.
 * 
 * @author mhoebeke
 *
 */
@SuppressWarnings("serial")
public class ChooseInputFileAction extends AbstractAction {

	InputFilePanel panel=null;
	JFileChooser chooser=null;

	File curdir=null;

	/**
	 * Class constructor using an InputFilePanel and a String.
	 * 
	 * @param panel an InputFilePanel that will be informed when the user selects a file in the file dialog.
	 * @param title the title of the file dialog.
	 */
	public ChooseInputFileAction(InputFilePanel panel, String title) {
		super(title);
		this.panel=panel;
		this.curdir=new File(System.getProperty("user.dir")); //$NON-NLS-1$
		this.chooser=new JFileChooser();
	}

	/**
	 * Opens the file dialog in file open mode. If the user actually selects a file {@link InputFilePanel#setInputFile(File)} is called with
	 * the selected file as argument.
	 * 
	 * @param evt ignored.
	 */
	public void actionPerformed(ActionEvent evt) {

		chooser.setCurrentDirectory(curdir);

		int retval=chooser.showOpenDialog(null);
		if (retval == JFileChooser.APPROVE_OPTION) {
			File file=chooser.getSelectedFile();
			curdir=chooser.getCurrentDirectory();
			panel.setInputFile(file);
		}

	}

}
