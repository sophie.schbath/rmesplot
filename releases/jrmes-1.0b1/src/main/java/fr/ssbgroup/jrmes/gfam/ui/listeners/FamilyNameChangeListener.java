/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.gfam.ui.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JTextField;

import fr.ssbgroup.jrmes.JRmes;

public class FamilyNameChangeListener implements ActionListener, FocusListener {

	JRmes controller=null;
	
	public FamilyNameChangeListener(JRmes controller) {
		this.controller=controller;
	}
	
	private void updateControllerValue(JTextField familyNameField) {
		String fName=familyNameField.getText();
		controller.setGfamFamilyName(fName);
	}
	
	public void actionPerformed(ActionEvent evt) {
		updateControllerValue((JTextField)evt.getSource());
		
	}

	public void focusGained(FocusEvent arg0) {
		// Nothing special to do here...
	}

	public void focusLost(FocusEvent evt) {
		updateControllerValue((JTextField)evt.getSource());
	}

}
