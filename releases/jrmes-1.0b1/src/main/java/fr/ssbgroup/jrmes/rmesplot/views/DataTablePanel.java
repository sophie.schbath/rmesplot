/**
 * This file is part of JR'MES 0.0.1-SNAPSHOT.
 *
 * Copyright (C) 2009, 2010 INRA - MIA - Unité MIG
 *
 * Author(s) :
 *
 * 	Mark Hoebeke (mark.hoebeke@jouy.inra.fr)
 *
 * JR'MES 0.0.1-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JR'MES 0.0.1-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JR'MES 0.0.1-SNAPSHOT.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.ssbgroup.jrmes.rmesplot.views;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import fr.ssbgroup.jrmes.rmesplot.controls.RMESPlotController;
import fr.ssbgroup.jrmes.rmesplot.models.Model;

/**
 * Class implementing the layout of ResultTablePane.
 * 
 * @author $Author: mhoebeke $
 * @version $Revision: 123 $
 */
public class DataTablePanel extends JPanel implements ListSelectionListener {

    private RMESPlotController _controller;

    private Model _model;

    private int _index;
    
    private DataTable _table;
    
    private JScrollPane _scrollPane;
    

    private static int WORD_TEXTFIELD_SIZE = 8;

    private static int ENTRIES_TEXTFIELD_SIZE = 5;

    public DataTablePanel(RMESPlotController controller, Model model, int index) {

        _controller = controller;
        _model = model;
        _index = index;

        setLayout(new BorderLayout());
        JPanel tablePanel = new JPanel(new BorderLayout());
        tablePanel.setBorder(BorderFactory.createTitledBorder(_controller
                .getLabel("TAB_VALUES")));
        add(tablePanel, BorderLayout.CENTER);

        _table = new DataTable(controller, model);
        _scrollPane = new JScrollPane(_table);
        _scrollPane
                .setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        _scrollPane
                .setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        tablePanel.add(_scrollPane, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setBorder(BorderFactory.createTitledBorder(_controller
                .getLabel("BORDER_OPTIONS")));
        add(buttonPanel, BorderLayout.SOUTH);

        buttonPanel.setLayout(new GridLayout(2,1));
        
        JPanel linePanel=new JPanel();
        buttonPanel.add(linePanel);
        JButton button = new JButton(_controller.getLabel("BUTTON_PLOT_ON_X"));
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                _controller.plotOnX(_model, _index);
            }
        });
        linePanel.add(button);

        button = new JButton(_controller.getLabel("BUTTON_PLOT_ON_Y"));
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                _controller.plotOnY(_model, _index);
            }
        });
        linePanel.add(button);

        JLabel label = new JLabel(_controller.getLabel("FIND_WORD_LABEL"));
        linePanel.add(label);

        final JTextField wordTextField = new JTextField();
        wordTextField.setColumns(WORD_TEXTFIELD_SIZE);
        wordTextField.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                String word=wordTextField.getText();
                int row=_table.findWord(word);
                if (row>=0) {
                    Rectangle cellRect=_table.getCellRect(row,0,true);
                    JViewport viewport=_scrollPane.getViewport();
                    viewport.setViewPosition(cellRect.getLocation());
                } else {
                    JOptionPane.showMessageDialog(_controller.getDataFrame(),_controller.getLabel("WORD_NOT_FOUND_LABEL"),_controller.getLabel("WORD_NOT_FOUND_TITLE"),JOptionPane.ERROR_MESSAGE);
                }
            }
            });
        
        linePanel.add(wordTextField);
     
        label = new JLabel(_controller.getLabel("MAX_ENTRIES_LABEL"));
        linePanel.add(label);
        
        final JTextField maxEntriesTextField = new JTextField();
        maxEntriesTextField.setColumns(ENTRIES_TEXTFIELD_SIZE);
        maxEntriesTextField.setText((String)_controller.getProperty("datatable.maxentries"));
        maxEntriesTextField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int maxEntries = Integer
                        .parseInt(maxEntriesTextField.getText());
                _table.setMaxEntries(maxEntries);
            }
        });

        linePanel.add(maxEntriesTextField);

        
        linePanel=new JPanel();
        buttonPanel.add(linePanel);
        
        JCheckBox checkbox=new JCheckBox(_controller.getLabel("AUTO_ADD_PYRAMIDS_CHECKBOX"));
        checkbox.setSelected(_controller.getAutoAddPyramids());
        checkbox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED)
                    _controller.setAutoAddPyramids(true);
                else
                    _controller.setAutoAddPyramids(false);
            }
        });
        linePanel.add(checkbox);
        
        button = new JButton(_controller.getLabel("BUTTON_EXPORT"));
        button.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		final JFileChooser fileChooser=new JFileChooser(_controller.getCurrentDir());
            	int retval = fileChooser.showSaveDialog(null);
                if (retval == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                    _controller.setCurrentDir(fileChooser.getCurrentDirectory());
            		_controller.exportModel(_table,file);
            	}
			}
        });
        linePanel.add(button);
        
        button = new JButton(_controller.getLabel("BUTTON_REMOVE"));
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                _controller.removeModel(_model, _index);
                Container parent = DataTablePanel.this.getParent();
                parent.remove(DataTablePanel.this);
                //_controller.removeElements(name);
            }
        });
        linePanel.add(button);
        
    }
    
    public void applyFilters() {
        _table.applyFilters();
    }

    public void valueChanged(ListSelectionEvent e) {
        // TODO Auto-generated method stub

    }
}