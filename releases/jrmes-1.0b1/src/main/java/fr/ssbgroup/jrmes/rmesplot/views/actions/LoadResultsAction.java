package fr.ssbgroup.jrmes.rmesplot.views.actions;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;

import fr.ssbgroup.jrmes.rmesplot.controls.RMESPlotController;

/**
 * LoadResultsAction takes care of showing the file dialog allowing the selection of R'MES result files.
 * 
 * @author mark
 *
 */

@SuppressWarnings("serial")
public class LoadResultsAction extends AbstractAction {
	
	RMESPlotController controller;
	
	public LoadResultsAction(RMESPlotController controller) {
		super(controller.getLabel("LOAD_RESULTS"));
		this.controller=controller;
	}

	public void actionPerformed(ActionEvent evt) {
      	JFileChooser fileChooser=new JFileChooser();
    	fileChooser.setMultiSelectionEnabled(true);
    	fileChooser.setCurrentDirectory(controller.getCurrentDir());
        int retval = fileChooser.showOpenDialog(null);
        if (retval == JFileChooser.APPROVE_OPTION) {
            File[] files = fileChooser.getSelectedFiles();
            controller.setCurrentDir(fileChooser.getCurrentDirectory());
            controller.loadFiles(files);
        }

	}

}
