<?xml version='1.0'?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.0/docbookx.dtd">

<article>
<articleinfo>
<title>RMESPlot User Guide</title>
<author>
	<firstname>Mark</firstname><surname>Hoebeke</surname>
	<email>Mark.Hoebeke_at_jouy.inra.fr</email>
</author>
</articleinfo>
<sect1>
	<title>What is RMESPlot?</title>
	<simpara>
	RMESPlot is a companion tool to the R'MES software package. It allows visualizing and exploring result files generated
	by the R'MES program. 
	</simpara>
</sect1>
<sect1>
	<title>Installing RMESPlot</title>

	<sect2>
		<title>Requirements</title>
	  	<simpara>RMESPlot is written in Java, and thus needs a Java Runtime Environment or (JRE) to run. 
	  Moreover it uses features found only in Java 1.5 (Java 5 or Tiger), and will not work on earlier JREs.
	  JREs for many platforms can be downloaded from: <ulink url="http://java.com/download/index.jsp"></ulink></simpara>
	</sect2>

	<sect2>
		<title>Getting RMESPlot</title>
		<simpara>RMESPlot is packaged as a single Java archive (or JAR file), which can be downloaded from:</simpara>
		<ulink url="http://mulcyber.toulouse.inra.fr/projects/rmesplot"/>.
	</sect2>

	<sect2>
		<title>Running RMESPlot</title>
		<para>
		<itemizedlist>
			<listitem><para>On Windows and MacOS X systems: a double click on the downloaded archive launches RMESPlot</para></listitem>
			<listitem><para>On Unix like systems: go to the directory where the JAR file was downloaded and type
			<screen>java -jar rmesplot-{version}.jar</screen>
			where {version} denotes the version of RMESPlot.</para></listitem>
		</itemizedlist>
		</para>
	</sect2>
</sect1>
<sect1>
	<title>Using RMESPlot</title>
	<sect2><title>The User Interface</title>
		<para>RMESPlot is composed of two windows: the dataset window and the plot window.</para>
		
		<sect3>
			<title>The dataset window</title>
			<para>The dataset window (<xref linkend="datasetwindow"/>) is made of two parts. The upper part contains a tree
			representing the datasets that have been loaded. The outermost components of the tree are sequence names as they
			have been submitted to R'MES. These components each contain a sublevel with the approximation method used (Gaussian, 
			Compound Poisson, Poisson) on the words or word families. For each approximation method, another level details
			the phases for which the results have been computed (phase 0 if no phase information has been used). Each of the phases
			then contains the wordlengths that have been examined by R'MES. Finally, for each word length, the list of Markov
			orders that have been processed constitute the leaves of the result tree.</para>
	    	<figure id="datasetwindow">
      			<title>The dataset window</title>
      			<mediaobject>
      				<imageobject>
      					<imagedata fileref="datasets.png" format="PNG" scale="35" align="center"/>
      				</imageobject>
      			</mediaobject>
    		</figure>
    		<para>The lower part of the dataset window contains a series of tabs corresponding to leaves selected in the result tree.
    		Each tab displays the actual numerical results in tabular format, and a series of buttons and fields detailed in the following
    		sections.</para>
 	 </sect3>
 	 <sect3>
 	 	<title>The plot window</title>
 	 	<para>The plot window (<xref linkend="plotwindow"/>) gives access to two graphical representations through tabs located at the top.
 	 	The XY representation where the scores of words taken from two different result sets can be drawn along the X and Y axes.
 	 	</para>
	    	<figure id="plotwindow">
      			<title>The plot window</title>
      			<mediaobject>
      				<imageobject>
      					<imagedata fileref="plots.png" format="PNG" scale="35" align="center"/>
      				</imageobject>
      			</mediaobject>
    		</figure>
    	<para>A pyramidal representation where the exceptionality of selected words can be compared with the exceptionality of its
    	subwords.
    	</para>
 	 </sect3>
	</sect2>
</sect1>
<sect1>
 	 	<title>How do I?</title>
 	 	<sect2>
 	 		<title>Load results into RMESPlot?</title>
 	 		<para>
 	 			Use the <guimenuitem>Load Results</guimenuitem> item in the <guimenu>File</guimenu> menu. Then select the file (or the set of files)
 	 			to be loaded in the result tree.
 	 		</para>
 	 	</sect2>
 	 	<sect2>
 	 		<title>Display the numerical data associated to results?</title>
 	 		<para>
 	 			Unfold the result tree to reach the leaf of interest then click on this leaf. A new tab will be created in the lower part
 	 			of the dataset window with the numerical quantities. For word results, the title of the tab is formatted as follows:
 	 			<screen>&lt;i&gt;-l&lt;j&gt;-m&lt;k&gt;-p&lt;n&gt;</screen>
 	 			where:<itemizedlist>
 	 			<listitem><simpara>i is the tab index,</simpara></listitem>
 	 			<listitem><simpara>j is the word length,</simpara></listitem>
 	 			<listitem><simpara>k is the Markov order,</simpara></listitem>
 	 			<listitem><simpara>n is the phase number.</simpara></listitem>
 	 			</itemizedlist>
 	 			For word family results, as words are all of the same length, the length information is replaced with "fam".
 	 		</para>
 	 		<para>The results can be sorted by clicking on the column headers: a first click orders according to the values of the column
 	 		and a second click reverses the sort order.</para>
 	 	</sect2>
 	 	<sect2>
 	 		<title>Plot results in the plot window?</title>
 	 		<para>If no dataset has been selected, select a dataset of interest in the result tree. Click on the <guibutton>Plot on X</guibutton> or <guibutton>Plot on Y</guibutton> 
 	 		button below the result table.</para>
 	 		<para>If only one dataset is currently plotted, all datapoints will appear in red and will be positioned on the selected axis. 
 	 		If two datasets are plot, each on its axis, all their common words will be blue, and a diagonal line will be drawn to ease
 	 		dataset comparison.</para>
 	 	</sect2>
 	 	<sect2>
 	 		<title>Know the word underlying a point of the plot window?</title>
 	 		<para>Click on the cross in the plot window, and the label of the corresponding word will be drawn.</para>
 	 	</sect2>
 	 	<sect2>
 	 		<title>Locate a given word in the plot window?</title>
 	 		<para>Click on the word in the result table and its label will be drawn next to the data point representing it in the plot window.</para>
 	 	</sect2>
 	 	<sect2>
 	 		<title>Find a word in the result table?</title>
 	 		<para>Enter the word in the text field below the result table, and it will scroll to bring the word into view.</para>
 	 	</sect2>
 	 	<sect2>
 	 		<title>Filter out unwanted data?</title>
 	 		<para>The <guimenuitem>Filters...</guimenuitem> item of the <guimenu>Options</guimenu> menu opens the filter window <xref linkend="filterwindow"/>.
 	 		</para>
 	 		<warning>When applying filters, both data on the plot window and the result tables are affected.</warning>
	    	<figure id="filterwindow">
      			<title>The filter window</title>
      			<mediaobject>
      				<imageobject>
      					<imagedata fileref="filters.png" format="PNG" scale="35" align="center"/>
      				</imageobject>
      			</mediaobject>
    		</figure>
    		<para>The current version of RMESPlot offers four data filters:
    		<itemizedlist>
    			<listitem><para>the Stat value filter: this filter only keeps data points whose score has an absolute value above the user-defined value.
    			The higher this absolute value, the more data points will be filtered-out, and only the more exceptionally frequent or rare words will be kept.</para></listitem>
				<listitem><para>the Word filter: this filter only keeps explicitely listed words. The word list can be enterd by hand in the text area (one word per line), or loaded from a previously saved file
				with the appropriate button.</para></listitem> 
				<listitem><para>the Observed count filter: this filter only keeps words whose observed count is either below or above the given threshold.</para></listitem>
				<listitem><para>the Expectation filter: this filter only keeps words whose expectation is between the given lower and upper bounds.</para></listitem>
    		</itemizedlist>
    		
    		<emphasis>After filling the filter parameters, the <guibutton>Active</guibutton> button has to be checked to activate a filter.</emphasis></para>
 	 	</sect2>
 	 	<sect2>
 	 		<title>Check the exceptionality of the subwords of a given word?</title>
 	 		<para>Activate the <guibutton>Pyramids</guibutton> tab of the plot window and select the word of interest in the result table.
 	 		Provided the results for its subwords are also loaded in RMESPlot, this will draw a pyramid with the square representing the word
 	 		at the top. Each level of the pyramid contains squares representing the subwords of the words of the level immediately above it. The color
 	 		of the squares denote the exceptionality of these subwords, ranging from green (exceptionally frequent) to red (exceptionally rare).
 	 		An example is given in <xref linkend="pyramids"/>.</para>
	    	<figure id="pyramids">
      			<title>Word pyramids</title>
      			<mediaobject>
      				<imageobject>
      					<imagedata fileref="pyramids.png" format="PNG" scale="35" align="center"/>
      				</imageobject>
      			</mediaobject>
    		</figure>
 	 	</sect2>
 	 	<sect2>
 	 		<title>Report bugs, complaints, feedback ?</title>
 	 		<para>Send a post to the RMESPlot mailing lists available at <ulink url="http://mulcyber.toulouse.inra.fr/forum/?group_id=38"/>.</para>
 	 	</sect2>
 	 </sect1>
</article>
