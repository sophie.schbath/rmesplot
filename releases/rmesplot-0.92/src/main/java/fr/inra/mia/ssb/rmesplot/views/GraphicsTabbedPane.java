package fr.inra.mia.ssb.rmesplot.views;

import javax.swing.*;
import javax.swing.event.*;

import fr.inra.mia.ssb.rmesplot.controls.RMESPlotController;

/**
 * This class implementing a TabbedPane provides the switch between PyramidElement
 * display or Plot display.
 * 
 * @author $Author: mhoebeke $
 * @version $Revision: 123 $
 */
public class GraphicsTabbedPane extends JTabbedPane {

    GraphicsTabbedPane(RMESPlotController controller) {
        super(JTabbedPane.TOP);

        PlotPanel plotPanel = new PlotPanel(controller);
        addTab(controller.getLabel("PLOTTAB_TITLE"), plotPanel);

        PyramidPanel pyramidPanel = new PyramidPanel(controller);
        addTab(controller.getLabel("PYRAMIDTAB_TITLE"), pyramidPanel);

    }
}