/*
 * Created on 14 f�vr. 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fr.inra.mia.ssb.rmesplot.models.filters;

import java.util.HashSet;
import java.util.Iterator;

import fr.inra.mia.ssb.rmesplot.models.ElementStats;
import fr.inra.mia.ssb.rmesplot.models.ElementStatsWithExpectation;

/**
 * @author mark
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ExpectationFilter implements ElementFilter {

    private float _lowerThreshold;
    private float _upperThreshold;

    public ExpectationFilter(float lowerThreshold, float upperThreshold) {
        _lowerThreshold = lowerThreshold;
        _upperThreshold = upperThreshold;
        }

    /* (non-Javadoc)
     * @see rmes.models.filters.ElementFilter#apply(java.util.HashSet)
     */
    public HashSet apply(HashSet elements) {
    	HashSet newElements=new HashSet();
        Iterator it=elements.iterator();
        while (it.hasNext()) {
            ElementStats es=(ElementStats)it.next();
            if (es instanceof ElementStatsWithExpectation) {
            	float expect=((ElementStatsWithExpectation)es).getExpect().floatValue();
            	if (expect >= _lowerThreshold && expect <= _upperThreshold) {
            		newElements.add(es);
            	}
            } else 
            	newElements.add(es);
        }
        return newElements;
    }

}