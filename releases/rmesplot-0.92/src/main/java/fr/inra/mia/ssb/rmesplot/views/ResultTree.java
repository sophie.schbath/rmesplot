package fr.inra.mia.ssb.rmesplot.views;

import fr.inra.mia.ssb.rmesplot.*;
import fr.inra.mia.ssb.rmesplot.controls.RMESPlotController;
import fr.inra.mia.ssb.rmesplot.models.Model;
import fr.inra.mia.ssb.rmesplot.models.Results;
import fr.inra.mia.ssb.rmesplot.views.util.ResultNodeInserter;

import java.util.*;
import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.*;

/**
 * Class implementing the tree of available R'MES results. This class represents
 * the result tree figuring in the upper left corner of RMESPlot.
 * 
 * @author $Author: mhoebeke $
 * @version $Revision: 123 $
 */
public class ResultTree extends JTree implements TreeSelectionListener {

    RMESPlotController _controller;

    /**
     * Creates a new ResultTree object.
     */
    public ResultTree(RMESPlotController controller) {
        _controller = controller;

        DefaultMutableTreeNode root = new DefaultMutableTreeNode(controller
                .getLabel("RESULTTREE_TITLE"));
        DefaultTreeModel model = new DefaultTreeModel(root);
        setModel(model);
        setRootVisible(false);
        setShowsRootHandles(true);
        getSelectionModel().setSelectionMode(
                TreeSelectionModel.SINGLE_TREE_SELECTION);
        addTreeSelectionListener(this);

        _controller.setResultTree(this);
    }

    /**
     * Method adding a Result instance to the tree of available results.
     * 
     * @param results
     *            a Result instance built from an R'MES data file.
     */
    public void addResults(Results results) {
        //Look for node having the same title.
        DefaultTreeModel model = (DefaultTreeModel) getModel();
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();
        String title = results.getTitle();
        DefaultMutableTreeNode titlenode = getNodeWithLabel(title, root);
        if (titlenode == null) {
            titlenode = new DefaultMutableTreeNode(title);
            ResultNodeInserter.insertNodeAlpha(this,root,titlenode);
        }
        String algorithm = results.getAlgorithm();
        DefaultMutableTreeNode algonode = getNodeWithLabel(algorithm, titlenode);
        if (algonode == null) {
            algonode = new DefaultMutableTreeNode(algorithm);
            ResultNodeInserter.insertNodeAlpha(this,titlenode,algonode);
        }
        String phase = new String(_controller.getLabel("PHASE") + " ");
        phase = phase + Integer.toString(results.getPhase());
        DefaultMutableTreeNode phasenode = getNodeWithLabel(phase, algonode);
        if (phasenode == null) {
            phasenode = new DefaultMutableTreeNode(phase);
            ResultNodeInserter.insertNodeNumeric(this,algonode, phasenode);
        }
        HashSet models = results.getModels();
        Iterator modelIt = models.iterator();
        while (modelIt.hasNext()) {
            Model curmodel = (Model) modelIt.next();
            int wordlen=curmodel.getWordSize();
            String wordlenString = new String(_controller.getLabel("WORDLENGTH")+ " ");
            wordlenString += Integer.toString(wordlen);
            DefaultMutableTreeNode wlnode = getNodeWithLabel(wordlenString,phasenode);
            if (wlnode == null) {
                wlnode = new DefaultMutableTreeNode(wordlenString);
                ResultNodeInserter.insertNodeNumeric(this,phasenode, wlnode);
            }
            DefaultMutableTreeNode ordernode = getNodeWithLabel(curmodel,
                    wlnode);
            if (ordernode == null) {
                ordernode = new DefaultMutableTreeNode(curmodel);
                ResultNodeInserter.insertNodeNumeric(this,wlnode, ordernode);
                model.reload();
                TreePath path = new TreePath(ordernode.getPath());
                scrollPathToVisible(path);
            }
        }
    }

    /**
     * Method returning a particular tree node given a parent node and a label.
     * 
     * @param o
     *            an Object, which when transformed with toString() will be used
     *            to look up the node that is requested.
     * @param parent
     *            a TreeNode whose direct descedants will be searched for the
     *            requested node.
     * 
     * @return a DefaultMutableTreeNode whose label is identical to the
     *         stringification of the Object given as argument.
     */
    DefaultMutableTreeNode getNodeWithLabel(Object o, TreeNode parent) {
        DefaultMutableTreeNode node = null;
        String label = o.toString();
        for (Enumeration e = parent.children(); e.hasMoreElements();) {
            DefaultMutableTreeNode child = (DefaultMutableTreeNode) e
                    .nextElement();
            if (label.equals(child.toString())) {
                node = child;
                break;
            }
        }
        return node;
    }

    /**
     * Method reacting to user selections in the result tree. This method is
     * called when the user selects an item in the result tree and updates the
     * table views accordingly, either for the X axis dataset or for the Y axis
     * dataset.
     * 
     * @param te
     *            the actual TreeSelectionEvent
     */
    public void valueChanged(TreeSelectionEvent te) {
    	TreePath path=te.getPath();
    	if (isPathSelected(path)) {
    		DefaultMutableTreeNode node = (DefaultMutableTreeNode) path
    		.getLastPathComponent();
    		if ((node != null) && node.isLeaf()) {
    			Object userObject=node.getUserObject();
    			if (userObject instanceof Model) {
    				Model model = (Model) userObject;
    				Object[] components=path.getPath();
    				String pathString="";
    				for (int i=1;i<components.length;i++)
    					pathString=pathString+", "+components[i];
    				pathString=pathString.substring(2);
    				_controller.modelSelected(model,pathString);
    			}
    		}
    	}
    }
    
    public void deleteResults() {
       	TreePath path=getSelectionPath();
    	if (path != null) {
    		DefaultMutableTreeNode node=(DefaultMutableTreeNode) path.getLastPathComponent();
    		((DefaultTreeModel) getModel()).removeNodeFromParent(node);
    	} else {
    		JOptionPane.showMessageDialog(null,_controller.getLabel("NO_SELECTION_MESSAGE"));
    	}
    }
}