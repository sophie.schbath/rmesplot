/*
 * Created on 24 f�vr. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package fr.inra.mia.ssb.rmesplot.views.filters;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import fr.inra.mia.ssb.rmesplot.controls.RMESPlotController;
import fr.inra.mia.ssb.rmesplot.models.filters.ElementFilter;
import fr.inra.mia.ssb.rmesplot.models.filters.ScoreFilter;

/**
 * @author mhoebeke
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ScoreFilterPanel extends FilterPanel {
    
    private static final int TEXTFIELD_MAXCOLUMNS = 8;
    
    private JTextField _textField;
    
    public ScoreFilterPanel(RMESPlotController controller) {
        super(controller,controller.getLabel("STATFILTERBORDER_TITLE"));
        
        JPanel panel=new JPanel();
        add(panel,BorderLayout.CENTER);
        
        JLabel label=new JLabel(_controller.getLabel("STATFILTER_TEXTFIELD"));
        panel.add(label);
        
        _textField=new JTextField(TEXTFIELD_MAXCOLUMNS);
        _textField.setText("0.0");
        panel.add(_textField);
    }

    /* (non-Javadoc)
     * @see rmes.views.filters.FilterPanel#getFilter()
     */
    public ElementFilter getFilter() {
        float threshold=Float.parseFloat(_textField.getText());
        return new ScoreFilter(threshold);
    }

}
