/*
 * Created on 14 f�vr. 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fr.inra.mia.ssb.rmesplot.controls;

import java.awt.Dialog;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;
import javax.swing.tree.TreePath;

import fr.inra.mia.ssb.rmesplot.models.Alphabet;
import fr.inra.mia.ssb.rmesplot.models.DNAAlphabet;
import fr.inra.mia.ssb.rmesplot.models.ElementStats;
import fr.inra.mia.ssb.rmesplot.models.Model;
import fr.inra.mia.ssb.rmesplot.models.Pyramid;
import fr.inra.mia.ssb.rmesplot.models.Results;
import fr.inra.mia.ssb.rmesplot.models.WordResults;
import fr.inra.mia.ssb.rmesplot.models.filters.ElementFilterChain;
import fr.inra.mia.ssb.rmesplot.views.DataFrame;
import fr.inra.mia.ssb.rmesplot.views.DataTableTabbedPane;
import fr.inra.mia.ssb.rmesplot.views.GraphicsFrame;
import fr.inra.mia.ssb.rmesplot.views.PlotArea;
import fr.inra.mia.ssb.rmesplot.views.PyramidArea;
import fr.inra.mia.ssb.rmesplot.views.ResultTree;
import fr.inra.mia.ssb.rmesplot.views.WaitDialog;
import fr.inra.mia.ssb.rmesplot.views.filters.FilterDialog;

/**
 * @author mark
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class RMESPlotController {

	private boolean _exitOnQuit=true;
	
	private Properties _properties=null;
	
    private ResourceBundle _bundle = null;

    private ResultTree _tree = null;

    private DataTableTabbedPane _dataTableTabbedPane = null;

    private PlotArea _plotArea = null;
    
    private PyramidArea _pyramidArea = null;
    
    private ElementFilterChain _filterChain = null;

    private Model _modelOnX = null;

    private int _indexOnX = 0;

    private Model _modelOnY = null;

    private int _indexOnY = 0;

    private FilterDialog _filterDialog = null;
    private WaitDialog _waitDialog=null;

    private GraphicsFrame _graphicsFrame;

    private DataFrame _dataFrame;

    boolean _autoAddPyramids;
    
    public RMESPlotController(boolean exitOnQuit) {

    	_exitOnQuit=exitOnQuit;
    	
        _bundle = ResourceBundle.getBundle("RMESPlotL10N");
        _properties = new Properties();
        try {
			_properties.load(getClass().getResourceAsStream("/RMESPlot.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		_filterChain=new ElementFilterChain();
		Boolean tmpBoolean=Boolean.valueOf((String)_properties.getProperty("datatablepanel.autoaddpyramids"));
		_autoAddPyramids=tmpBoolean.booleanValue();
		
		Alphabet.setCurrentAlphabet(new DNAAlphabet());
		
		buildGUI();
    }

    private void buildGUI() {
        _graphicsFrame = new GraphicsFrame(this);
        _dataFrame = new DataFrame(this);
        _filterDialog=new FilterDialog(this);
        _waitDialog=new WaitDialog(this);
    }

    public String getLabel(String key) {
        String label=null;
        try {
            label = _bundle.getString(key);
        } catch (MissingResourceException mre) {
            label = "???";
        }
        return label;
    }

    public Object getProperty(Object key) {
    	return _properties.get(key);
    }
    
    public void loadFiles(File[] files) {
    	_tree.clearSelection();
        if (files.length>0) {
            ResultLoader resultLoader=new ResultLoader(this,files);
            Thread loaderThread=new Thread(resultLoader);
            loaderThread.start();
            try {
                loaderThread.join();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public void modelSelected(Model model,String description) {
        _dataTableTabbedPane.addModel(model, description);
    }

    public void deleteResults() {
    	_tree.deleteResults();
    }
    
    public String buildTitleString(Model model)
    {
    	
    	Results results=model.getResults();
    	String title="";
    	
    	if (results instanceof WordResults)
    		title="l"+model.getWordSize();
    	else
    		title="fam";
    	
    	title=title+"-m"+model.getOrder()+"-p"+results.getPhase();
    	
    	return title;
    }
    
    public void plotOnX(Model model, int index) {
        _modelOnX = model;
        _indexOnX = index;
        _plotArea.plotOnX(_modelOnX,""+index+"-"+buildTitleString(model));
    }

    public void clearPlotOnX() {
        _plotArea.clearXPlot();
        _modelOnX = null;
        _indexOnX = 0;
    }

    public void plotOnY(Model model, int index) {
        _modelOnY = model;
        _indexOnY = index;
        _plotArea.plotOnY(_modelOnY,""+index+"-"+buildTitleString(model));
    }

    public void clearPlotOnY() {
        _plotArea.clearYPlot();
        _modelOnY = null;
        _indexOnY = 0;
    }

    public void removeModel(Model model, int index) {
        if (model == _modelOnX && index == _indexOnX) {
            clearPlotOnX();

        }
        if (model == _modelOnY && index == _indexOnY) {
            clearPlotOnY();
        }
    }

    public void showFilterDialog() {
        _filterDialog.setVisible(true);
    }
    
    public void quit() {
    	_graphicsFrame.dispose();
    	_dataFrame.dispose();
    	_filterDialog.dispose();
    	_waitDialog.dispose();
    	if (_exitOnQuit)
    		System.exit(0);
    }

    public ResultTree getResultTree() {
        return _tree;
    }
    
    public void setResultTree(ResultTree tree) {
        _tree = tree;
    }

    public void setDataTableTabbedPane(DataTableTabbedPane dataTableTabbedPane) {
        _dataTableTabbedPane = dataTableTabbedPane;
    }

    public PlotArea getPlotArea() {
        return _plotArea;
    }
    
    public void setPlotArea(PlotArea plotArea) {
        _plotArea = plotArea;
    }
    
    public PyramidArea getPyramidArea() {
    	return _pyramidArea;
    }
    
    public void setPyramidArea(PyramidArea pyramidArea) {
    	_pyramidArea=pyramidArea;
    }
    
    public DataFrame getDataFrame() {
        return _dataFrame;
    }
    
    public GraphicsFrame getGraphicsFrame() {
        return _graphicsFrame;
    }

    public WaitDialog getWaitDialog() {
        return _waitDialog;
    }
    
    public ElementFilterChain getElementFilterChain() {
        return _filterChain;
    }
    /**
     * Method called by inner components to filter out elements from datasets
     * based on the filters activated in _filterChain.
     * @param elements elements before filtering.
     * @return filtered elements.
     */
    public HashSet applyFilters(HashSet elements) {
        return _filterChain.applyFilters(elements);
        
    }
    
    /**
     * Method called when the _filterDialog is closed and leading
     * to the refiltering of all models displayed in tables, plots and pyramids.
     *
     */
    public void filtersChanged() {
    	_dataTableTabbedPane.applyFilters();
        if (_modelOnX != null)
            _plotArea.refreshPlotOnX();
        if (_modelOnY != null)
            _plotArea.refreshPlotOnY();
    }
    
    public void ElementSelected(ElementStats es, Model model) {
        if (model==_modelOnX || model==_modelOnY)
            _plotArea.selectWord(es.getName());
 
        if (_autoAddPyramids == true) {
            Results results=model.getResults();
            if (results instanceof WordResults) {
                Pyramid pyramid=((WordResults)results).getPyramidWithTop(es,model);
                _pyramidArea.addPyramid(pyramid);
            }       
        }
    }
    
    public boolean getAutoAddPyramids() {
        return _autoAddPyramids;
    }
    
    public void setAutoAddPyramids(boolean aap) {
        _autoAddPyramids=aap;
    }
}