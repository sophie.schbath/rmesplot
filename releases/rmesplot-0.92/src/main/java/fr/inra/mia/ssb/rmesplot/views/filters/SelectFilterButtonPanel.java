/*
 * Created on 24 f�vr. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package fr.inra.mia.ssb.rmesplot.views.filters;

import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import fr.inra.mia.ssb.rmesplot.controls.RMESPlotController;

/**
 * @author mhoebeke
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SelectFilterButtonPanel extends JPanel {
    
    RMESPlotController _controller = null;
    JRadioButton _selectedButton = null;
    JRadioButton _unSelectedButton = null;
    
    public SelectFilterButtonPanel(RMESPlotController controller) {
        super();
        _controller=controller;
        
        setLayout(new GridLayout(1,2));
        
        ButtonGroup group=new ButtonGroup();
        
        _selectedButton=new JRadioButton(_controller.getLabel("SELECTFILTERBUTTONGROUP_ACTIVE"),false);
        add(_selectedButton);
        group.add(_selectedButton);
        
        _unSelectedButton=new JRadioButton(_controller.getLabel("SELECTFILTERBUTTONGROUP_INACTIVE"),true);
        add(_unSelectedButton);
        group.add(_unSelectedButton);
    }

    public boolean isActive() {
        return _selectedButton.isSelected();
    }
}
