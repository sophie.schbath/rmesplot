package fr.inra.mia.ssb.rmesplot.views.util;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;

public class ResultNodeInserter {
	
	public static void insertNode(JTree tree, DefaultMutableTreeNode parentNode, DefaultMutableTreeNode newNode) {
		parentNode.add(newNode);
	}
	
	public static void insertNodeAlpha(JTree tree, DefaultMutableTreeNode parentNode, DefaultMutableTreeNode newNode) {
		int index=0;
		boolean smaller=true;
		String newLabel=newNode.getUserObject().toString();
		DefaultTreeModel model=(DefaultTreeModel)tree.getModel();
		while (smaller && index < parentNode.getChildCount()) {
			DefaultMutableTreeNode curChild=(DefaultMutableTreeNode) parentNode.getChildAt(index);
			if (curChild.getLevel() == parentNode.getLevel()+1) {
				String label=curChild.getUserObject().toString();
				if (newLabel.compareTo(label)<0) {
					model.insertNodeInto(newNode, parentNode, index);
					smaller=false;
				}
			}
			index++;
		}
		if (smaller==true)
			model.insertNodeInto(newNode, parentNode, index);
		
	}
	
	public static void insertNodeNumeric(JTree tree, DefaultMutableTreeNode parentNode, DefaultMutableTreeNode newNode) {
		int index=0;
		boolean smaller=true;
		String newLabel=newNode.getUserObject().toString();
		newLabel=newLabel.replaceAll("\\D", "");
		DefaultTreeModel model=(DefaultTreeModel)tree.getModel();
		while (smaller && index < parentNode.getChildCount()) {
			DefaultMutableTreeNode curChild=(DefaultMutableTreeNode) parentNode.getChildAt(index);
			if (curChild.getLevel() == parentNode.getLevel()+1) {
				String label=curChild.getUserObject().toString();
				label=label.replaceAll("\\D","");
				if (Integer.parseInt(newLabel)<Integer.parseInt(label)) {
					model.insertNodeInto(newNode, parentNode, index);
					smaller=false;
				}
			}
			index++;
		}
		if (smaller==true)
			model.insertNodeInto(newNode, parentNode, index);
		
	}
	

}
